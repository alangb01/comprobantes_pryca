@extends ("layouts.personas")
@section('titulo_pagina','Buscar Boletas')
@section('content')
    <div class="container">
        @include('admin.comun.notificaciones')
        <div class="panel panel-default col-md-6 col-md-offset-3">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <a href="{{ $url_regresar }}" class="btn btn-success">Regresar</a>
                    </div>
                    <div class="col-md-6 text-center">
                        <h4>@yield('titulo_pagina')</h4>
                    </div>
                </div>

            </div>
            <div class="panel-body">
                <form role="form" action="{{ route('persona.buscar.boleta') }}" method="get" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="form-group col-md-12">
                        <label for="dni" class="obligatorio">DNI</label>
                        <input type="text" pattern=".{8,}" title="Ingrese minimo 8 numeros" class="form-control "
                               id="dni" name="dni" value="{{ old('dni') }}" placeholder="Ingrese dni de la persona">
                        @if($errors->has('dni')!=null)
                            <span class="help-inline alert-danger">{{ $errors->first('dni') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="serie" class="obligatorio">Serie de Boleta</label>
                        <input type="text" pattern=".{4,}" title="Ingrese minimo 4 caracteres" class="form-control "
                               id="serie" name="serie" value="{{ old('serie') }}" placeholder="Ingrese Serie de la boleta">
                        @if($errors->has('serie')!=null)
                            <span class="help-inline alert-danger">{{ $errors->first('serie') }}</span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="numero" class="obligatorio">Numero de Boleta</label>
                        <input type="number" pattern=".{4,}" title="Ingrese minimo 4 numeros" class="form-control "
                               id="numero" name="numero" value="{{ old('numero') }}" placeholder="Ingrese Número de la boleta">
                        @if($errors->has('numero')!=null)
                            <span class="help-inline alert-danger">{{ $errors->first('numero') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Ingresa el Código</label>
                        <div class="col-md-4">
                            {!! $captcha_img !!}
                        </div>
                        <div class="col-md-4">
                            <input id="captcha" type="text" class="form-control" name="captcha" required>
                            @if ($errors->has('captcha'))
                                <span class="help-block">
                                        <strong>Error en el Captcha</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-md-12 text-center">
                        <br>
                        <button type="submit" class="btn btn-success">Buscar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection