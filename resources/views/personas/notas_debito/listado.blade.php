@extends ("layouts.personas")
@section('titulo_pagina','Notas de debito de '.$persona)
@section('content')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
		<div class="panel-heading">
		  	<div class="row">
		  		<div class="col-md-12 text-center"><h4>@yield('titulo_pagina')</h4></div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12 text-center ">
		  			
		  		</div>
		  	</div>
		</div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr>
			   			<th>Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th>Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th>Fecha de emisión {!! $campos_orden->get('numero') !!}</th>
			   			<th>Concepto {!! $campos_orden->get('concepto') !!}</th>
			   			<th>Importe {!! $campos_orden->get('importe') !!}</th>
			   			<th>Factura {!! $campos_orden->get('boleta') !!}</th>
			   			{{-- <th>Estado</th> --}}
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_notas_debito) && count($listado_notas_debito)>0)
					@foreach($listado_notas_debito as $id=>$nota_debito)
						<tr class="">
							<td >{{ $nota_debito->serie }}</td>
							<td >{{ str_pad($nota_debito->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >{{ $nota_debito->fecha_emision->format("d/m/Y") }}</td>
							<td >{{ $nota_debito->conceptoToString() }}</td>
							<td class="text-right">{{ number_format($nota_debito->importe,2) }}</td>
							<td class="text-center">{{ $nota_debito->boleta->serie."-".$nota_debito->boleta->numero }}
								<form action="{{ $nota_debito->url_boleta_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
								   	{{ csrf_field() }}
									<button type="submit" class="btn btn-primary">
										Zip
									</button>
								</form>	
								@if($nota_debito->url_boleta_pdf!="")
								<form action="{{ $nota_debito->url_boleta_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
								   	{{ csrf_field() }}
									<button type="submit"  class="btn btn-primary">
										PDF
									</button>
								</form>	
								@endif
							</td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									<form action="{{ $nota_debito->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											ZIP
										</button>
									</form>	
									
									@if($nota_debito->url_pdf!="")
									<form action="{{ $nota_debito->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>	
									
									@endif
								</div>
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="8">
								No hay notas de debito registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="8">
			   				{{ $listado_notas_debito->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
</div>
@endsection