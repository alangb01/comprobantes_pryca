<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Modelo de tabla - Boleta</title>
    <style type="text/css">
        table{
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
            font-family: Raleway,sans-serif;
            font-size: 10px !important;
            line-height: 1.6;
        }
        table td{
            padding: 2px 5px;
            vertical-align: middle;
            overflow-wrap: break-word;
            position: relative;
        }
        .col-2{width:16.66666667%}
        .col-3{width:25%}
        .col-4{width:33.33333333%}
        .col-5{width:41.66666667%}
        .col-6{width:50%}
        .col-8{width:66.66666667%}
        .col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-8{
            -webkit-box-sizing:border-box;
            -moz-box-sizing:border-box;
            box-sizing:border-box;
        }
        .pdf{
            /*width: 1000px;*/
            background-color: #fff;
            padding: 20px 10px;
            margin: auto;
        }
        #facturaDatos{
            table-layout: fixed;
        }
        #facturaDatos tr{ width: 100%; }
        #facturaDatos .info-empresa *, #facturaDatos .dataRUC strong{
            display: block;
            color: #8C000C;
        }
        #facturaDatos .info-empresa strong{
            font-size: 16px;
        }
        .pad0{
            padding: 2px 0;
        }
        #facturaDatos .dataRUC{
            display: block;
            border: 3px solid #8C000C;
            border-radius: 10px;
            text-transform: uppercase;
            padding: 15px;
        }
        #facturaDatos .dataRUC *{
            font-size: 25px;
        }
        #facturaDatos .dataRUC strong small{
            color: #E0131A;
        }
        .mar20{
            margin: 20px 0;
        }
        #facturaDatos thead td strong{
            text-transform: uppercase;
            color: #8C000C;
            font-size: 14px;
        }
        #facturaDatos thead td span{
            color: #1a829a;
        }
        /**/
        #listaProductos{
            margin: 30px 0 0;
        }
        #listaProductos td{
            padding: 10px;
            font-size: 15px;
        }
        #listaProductos td, .totalDetalle td span{
            border: 1px solid #ddd;
        }
        .empty td, .total td:not(.totalDetalle){
            border: none !important;
        }
        .totalLetras{
            text-transform: uppercase;
            vertical-align: top;
        }
        .fRowO span, .fRowT span{
            display: block;
        }
        .fRowO small, .fRowT small{
            color: #8C000C;
            position: absolute;
            font-size: 12px;
            letter-spacing: 0.05em;
        }
        .fRowO small{
            top: 5px;
        }
        .fRowO span{
            margin: 15px 0 0;
        }
        .fRowT small{
            bottom: 5px;
            left: 0;
            right: 0;
        }
        .fRowT span{
            margin: 0 0 15px;
        }
    </style>
</head>
<body>
<div class="pdf">
    <table id="facturaDatos">
        <thead>
        <tr>
            <td class="logo col-4">
                <img src="{{public_path('image/logo.jpg')}}" alt="TCA" title="TCA">
            </td>
            <td class="info-empresa col-4" align="center">
                <i><strong>{{$data['nombre_empresa']}}</strong></i>
                <small>{{$data['establecimiento']}}</small>
                {{--<small>Ate - Lima - Lima</small>--}}
                <small>Telefax: 326-2472</small>
            </td>
            <td class="col-5 pad0" rowspan="6" align="center">
                <span class="dataRUC">
                    <strong>R.U.C. N° {{$data['ruc_empresa']}}</strong>
                    <strong class="mar20">{{$data['nombre_documento']}} ELECTRÓNICA</strong>
                    <strong>{{$data['codigo_factura']}}</strong>
                </span>
            </td>
        </tr>
        <tr>
            <td class="col-6">
                <strong>cliente: </strong>
                <span>{{$data['razon_social']}}</span>
            </td>
        </tr>
        <tr>
            <td class="col-8">
                <strong>dirección: </strong>
                {{--<span>{{$data['direccion_persona']}}</span>--}}
            </td>
            <td class="col-4">
                <strong>r.u.c. o dni: </strong>
                <span>{{$data['ruc']}}</span>
            </td>
        </tr>
        <tr>
            <td class="col-8">
                <strong>documento que modifica: </strong>
                <span></span>
            </td>
            <td class="col-4">
                <strong>f. de emisión: </strong>
                <span>{{$data['fecha_emision']}}</span>
            </td>
        </tr>
        <tr>
            <td class="col-8">
                <strong>ref n°: </strong>
                <span>{{$data['serie_boleta']}} - {{$data['numero_boleta']}}</span>
            </td>
            <td class="col-4">
                <strong>fecha: </strong>
                <span>{{$data['fecha_emision']}}</span>
            </td>
        </tr>
        <tr>
            <td class="col-6">
                <strong>motivo: </strong>
                <span>{{$data['concepto']}}</span>
            </td>
        </tr>
        </thead>
    </table>
    <table id="listaProductos">
        <thead align="center">
        <tr>
            <td class="col-2"><strong>Cantidad</strong></td>
            <td class="col-2"><strong>U.M.</strong></td>
            <td class="col-2"><strong>Descripción</strong></td>
            <td class="col-2"><strong>Precio Unitario</strong></td>
            <td class="col-2"><strong>Descuento</strong></td>
            <td class="col-2"><strong>Precio Venta</strong></td>
        </tr>
        </thead>
        <tbody>
        @for ( $i = 0 ; $i < count($temp); $i++)
            <tr >
                <td>{{$temp[$i]['cantidadUnidad']}}</td>
                <td>{{$temp[$i]['tipoCantidad']}}</td>
                <td>{{$temp[$i]['descripcion_producto']}}</td>
                <td>S/. {{ $temp[$i]['PrecioVentaUnitario']}}</td>
                <td>S/. 00.00000</td>
                <td>S/. {{ $temp[$i]['PrecioVentaUnitario']}}</td>
            </tr>
        @endfor
        <!--  -->
        {{--<tr class="fRowO">--}}
            {{--<td></td>--}}
            {{--<td>--}}
                {{--<small>V. gravadas</small>--}}
                {{--<span>S/. {{ $data['total_venta_exonerada'] }}</span>--}}
            {{--</td>--}}
            {{--<td>--}}
                {{--<small>V. inafecto</small>--}}
                {{--<span>S/. {{ $data['total_venta_inafecta'] }}</span>--}}
            {{--</td>--}}
            {{--<td>--}}
                {{--<small>IGV</small>--}}
                {{--<span>S/. {{ $data['total_venta_gravada'] }}</span>--}}
            {{--</td>--}}
            {{--<td>--}}
                {{--<small>Total</small>--}}
                {{--<span>S/. {{ $data['importe_total']}}</span>--}}
            {{--</td>--}}
            {{--<td></td>--}}
        {{--</tr>--}}
        {{--<tr class="fRowT">--}}
            {{--<td></td>--}}
            {{--<td align="center">--}}
                {{--<span></span>--}}
                {{--<small>Cancelado</small>--}}
            {{--</td>--}}
            {{--<td align="center" colspan="2">--}}
                {{--<span></span>--}}
                {{--<small>Nombres y Apellidos</small>--}}
            {{--</td>--}}
            {{--<td align="center">--}}
                {{--<span></span>--}}
                {{--<small>D.N.I.</small>--}}
            {{--</td>--}}
            {{--<td align="center">--}}
                {{--<span></span>--}}
                {{--<small>Firma</small>--}}
            {{--</td>--}}
        {{--</tr>--}}
        <!--  -->
        <tr class="empty"><td colspan="6"></td></tr>
        <!--  -->
        <tr class="total">
            <td class="totalLetras">
                <strong>SON  {{$numero_letras}} Y 00 / 100 SOLES</strong>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td class="totalDetalle" colspan="2">
                <table>
                    <tr>
                        <td class="col-6"><strong>Monto Descuentos</strong></td>
                        <td class="col-6"><span>S/. &nbsp;{{ $data['sumatoria_descuentos'] }}</span></td>
                    </tr>
                    <tr>
                        <td class="col-6"><strong>Op. Gravadas</strong></td>
                        <td class="col-6"><span>S/. {{ $data['total_venta_exonerada'] }}</span></td>
                    </tr>
                    <tr>
                        <td class="col-6"><strong>Op. Inafectas</strong></td>
                        <td class="col-6"><span>S/. {{$data['total_venta_inafecta']}}</span></td>
                    </tr>
                    <tr>
                        <td class="col-6"><strong>Op. Exoneradas</strong></td>
                        <td class="col-6"><span>S/. {{$data['total_venta_inafecta']}}</span></td>
                    </tr>
                    <tr>
                        <td class="col-6"><strong>IGV</strong></td>
                        <td class="col-6"><span>S/. {{ $data['total_venta_gravada']}}</span></td>
                    </tr>
                    <tr>
                        <td class="col-6"><strong>Importe Total</strong></td>
                        <td class="col-6"><span>S/. {{$data['importe_total']}}</span></td>
                    </tr>
                    @if(isset($data['percepciones']) && $data['percepciones']>0)
                        <tr>
                            <td class="col-6"><strong>Percepción</strong></td>
                            <td class="col-6"><span>S/. {{$data['percepciones']}}</span></td>
                        </tr>
                        <tr>
                            <td class="col-6"><strong>Total a pagar</strong></td>
                            <td class="col-6"><span>S/. {{$data['total_final']}}</span></td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>
        <!--  -->
        <tr class="empty"><td colspan="6"></td></tr>
        <!--  -->
        </tbody>
        <tfoot align="center">
        <tr>
            <td colspan="6">
                <span>Autorizado mediante la resolución Nro. 097-2012/SUNAT. Esta es una representación impresa de la factura (boleta) de venta electrónica. Puede ser consultada ingresando a <a href="http://tca.com.pe/cpe/">http://tca.com.pe/cpe/</a></span>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</body>
</html>