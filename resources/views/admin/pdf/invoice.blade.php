<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Comprobante de Pago</title>
    <style type="text/css">
        table{
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
            font-family: Raleway,sans-serif;
            font-size: 10px !important;
            line-height: 1.6;
        }
        table td{
            padding: 2px 5px;
            vertical-align: middle;
            overflow-wrap: break-word;
        }
        .col-1{width: 8.6666667%}
        .col-2{width:16.66666667%}
        .col-3{width:25%}
        .col-4{width:33.33333333%}
        .col-5{width:41.66666667%}
        .col-6{width:50%}
        .col-8{width:66.66666667%}
        .col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-8{
            -webkit-box-sizing:border-box;
            -moz-box-sizing:border-box;
            box-sizing:border-box;
        }
        #facturaDatos{
            table-layout: fixed;
        }
        #facturaDatos tr{ width: 100%; }
        #facturaDatos .info-empresa *, #facturaDatos .dataRUC strong{
            display: block;
            color: #8C000C;
        }
        #facturaDatos .info-empresa strong{
            font-size: 16px;
        }
        .pad0{
            padding: 2px 0;
        }
        #facturaDatos .dataRUC{
            display: block;
            border: 3px solid #8C000C;
            border-radius: 10px;
            text-transform: uppercase;
            padding: 15px;
        }
        #facturaDatos .dataRUC *{
            font-size: 25px;
        }
        #facturaDatos .dataRUC strong small{
            color: #E0131A;
        }
        .mar20{
            margin: 20px 0;
        }
        #facturaDatos thead td strong{
            text-transform: uppercase;
            color: #8C000C;
            font-size: 14px;
        }
        #facturaDatos thead td span{
            color: #1a829a;
        }
        /**/
        #listaProductos{
            margin: 30px 0 0;
        }
        #listaProductos td{
            padding: 10px;
            font-size: 15px;
        }
        #listaProductos td, .totalDetalle td span{
            border: 1px solid #ddd;
        }
        .empty td, .total td:not(.totalDetalle){
            border: none !important;
        }
        .totalLetras{
            text-transform: uppercase;
            vertical-align: top;
        }
        .totalDetalle tr{
            margin: 5px 0;
        }
        .totalDetalle td{
            padding: 2px 10px !important;
        }
        .totalDetalle td span{
            display: block;
            padding: 10px;
            font-size: 14px;
        }
    </style>
</head>
<body>
<table id="facturaDatos">
    <thead>
    <tr>
        <td class="logo col-4">
            <img src="{{ 'image/logo.jpg' }}" alt="TCA" title="TCA">
        </td>
        <td class="info-empresa col-4" align="center">
            <i><strong>{{$data['nombre_empresa']}}</strong></i>
            <small>{{$data['establecimiento']}}</small>
                {{--<small>Ate - Lima - Lima</small>--}}
            <small>Telefax: 326-2472</small>
        </td>
        <td class="col-5 pad0" rowspan="6" align="center">
                    <span class="dataRUC">
                        <strong>R.U.C. N° {{$data['ruc_empresa']}}</strong>
                        <strong class="mar20">{{$data['nombre_documento']}} ELECTRÓNICA</strong>
                        <strong>{{$data['codigo_factura']}}</strong>
                    </span>
        </td>
    </tr>
    <tr>
        <td class="col-6">
            <strong>punto partida: </strong>
            <span></span>
        </td>
    </tr>
    <tr>
        <td class="col-8">
            <strong>cliente: </strong>
            <span>{{$data['razon_social']}}</span>
        </td>
        <td class="col-4">
            <strong>ruc o dni: </strong>
            <span>{{$data['ruc']}}</span>
        </td>
    </tr>
    <tr>
        <td class="col-8">
            <strong>dirección: </strong>
            <span>{{$data['direccion_persona']}}</span>
        </td>
        <td class="col-4">
            <strong>código: </strong>
            <span></span>
        </td>
    </tr>
    <tr>
        <td class="col-8">
            <strong>referencia: </strong>
            <span></span>
        </td>
        <td class="col-4">
            <strong>cond. de pago: </strong>
            <span></span>
        </td>
    </tr>
    <tr>
        <td class="col-6">
            <strong>f. emisión: </strong>
            <span>{{$data['fecha_emision']}}</span>
        </td>
        <td class="col-6">
            <strong>f. vencimiento: </strong>
            <span>{{$data['fecha_vencimiento']}}</span>
        </td>
    </tr>
    </thead>
</table>
<table id="listaProductos" style="page-break-after: auto">
    <thead align="center">
    <tr>
        <td class="col-2"><strong>Cantidad</strong></td>
        <td class="col-1"><strong>Unidad Medida</strong></td>
        <td class="col-4"><strong>Descripción</strong></td>
        <td class="col-5"><strong>Valor Unitario</strong></td>
        <td class="col-5"><strong>Precio Venta</strong></td>
    </tr>
    </thead>
    <tbody>
    {{--@for ( $i = 0 ; $i < count($data); $i++)--}}
    @forelse($data['data_producto'] as $item)
        <tr >
            <td>{{ $item['cantidadUnidad'] }}</td>
            <td>{{ $item['tipoCantidad'] }}</td>
            <td>{{ $item['descripcion_producto']  }}</td>
            <td>S/. {{ $item['PrecioVentaUnitario']}}</td>
            <td>S/. {{ $item['PrecioVentaTotal']}}</td>
        </tr>
    @empty
        <tr>
            <td colspan="5"></td>
        </tr>
    @endforelse
    {{--@endfor--}}
    <!--  -->
    <tr class="empty"><td colspan="4"></td></tr>
    <!--  -->
    <tr class="total">
        <td class="totalLetras">
            <strong>SON  {{$numero_letras}} Y 00 / 100 SOLES</strong>
        </td>
        <td></td>
        <td></td>
        <td class="totalDetalle" colspan="2">
            <table>
                <tr>
                    <td class="col-6"><strong>Monto Descuentos</strong></td>
                    <td class="col-6"><span>S/. &nbsp;{{ $data['sumatoria_descuentos'] }}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>Op. Gravadas</strong></td>
                    <td class="col-6"><span>S/. {{ $data['total_venta_gravada'] }}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>Op. Inafectas</strong></td>
                    <td class="col-6"><span>S/. {{$data['total_venta_inafecta']}}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>Op. Exoneradas</strong></td>
                    <td class="col-6"><span>S/. {{$data['total_venta_exonerada']}}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>IGV</strong></td>
                    <td class="col-6"><span>S/. {{ $data['sumatoria_IGV']}}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>Importe Total</strong></td>
                    <td class="col-6"><span>S/. {{$data['importe_total']}}</span></td>
                </tr>
                @if(isset($data['percepciones']) && $data['percepciones']>0)
                <tr>
                    <td class="col-6"><strong>Percepción</strong></td>
                    <td class="col-6"><span>S/. {{$data['percepciones']}}</span></td>
                </tr>
                <tr>
                    <td class="col-6"><strong>Total a pagar</strong></td>
                    <td class="col-6"><span>S/. {{$data['total_final']}}</span></td>
                </tr>
                @endif
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="1">
            OPERACIÓN SUJETA A PERCEPCIÓN <br>
            Porcentaje 2.0 TOTAL PERCEPCION:
        </td>
        @if(isset($data['percepciones']) && $data['percepciones']>0)
        <td><span>S/. {{$data['percepciones']}}</span></td>
        @else
            <td><span>S/. 0.00</span></td>
        @endif
        <td>TOTAL COBRAR INCLUIDO PERCEPCIÓN: </td>
        <td><span>S/. {{$data['total_final']}}</span></td>
    </tr>
    <!--  -->
    <tr class="empty"><td colspan="5"></td></tr>
    <!--  -->
    </tbody>
    <tfoot align="center">
    <tr>
        <td colspan="5">
            <span>Autorizado mediante la resolución Nro. 097-2012/SUNAT. Esta es una representación impresa de la factura (boleta) de venta electrónica. Puede ser consultada ingresando a <a href="http://tca.com.pe/cpe/">http://tca.com.pe/cpe/</a></span>
            <p style="text-align: center">{{$data['codigo_hash']}}</p>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>