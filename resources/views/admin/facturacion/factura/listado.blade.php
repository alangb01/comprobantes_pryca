@extends ("layouts.admin")
@section('titulo_pagina','Listado de facturas')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar facturas">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>
	  		</div>
			<div class="col-md-4 text-right">
				<div class="btn-group ">
					<a href="{{ $url_nuevo }}" class="btn btn-success ">Nuevo</a>
				</div>
				<div class="btn-group ">
					<a href="{{ $url_subida }}" class="btn btn-warning ">Subir</a>
				</div>
			</div>
	  	</div>
	  </div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr class="">
			   			<th class="col-md-1 text-center">Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th class="col-md-1 text-center">Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th class="col-md-1 text-center">RUC {!! $campos_orden->get('ruc') !!}</th>
			   			<th class="col-md-2 text-center">Razon social {!! $campos_orden->get('razon_social') !!}</th>
			   			<th class="col-md-1 text-center">Fecha   {!! $campos_orden->get('fecha_emision') !!}</th>
			   			<th class="col-md-1 text-center">Importe  {!! $campos_orden->get('importe') !!}</th>
			   			<th class="col-md-2 text-center">Estado</th>
			   			<th class="col-md-2 text-center">Descarga</th>
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_facturas) && count($listado_facturas)>0)
					@foreach($listado_facturas as $id=>$factura)
						<tr class="">
							<td class="text-center">{{ $factura->serie }}</td>
							<td class="text-right">{{ str_pad($factura->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >{{ $factura->empresa->ruc }}</td>
							<td >{{ $factura->empresa->razon_social }}</td>
							<td >{{ $factura->fecha_emision->format("d/m/Y") }}</td>
							<td class="text-right">{{ number_format($factura->importe,2) }}</td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									@if($factura->estado()==$factura::ESTADO_EMITIDO)
									<div class="btn alert-success text-center " style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@elseif($factura->estado()==$factura::ESTADO_ANULADO)
									<div class="btn alert-danger text-center" style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@elseif($factura->estado()==$factura::ESTADO_MODIFICADO)
									<div class="btn alert-warning text-center" style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@endif
								</div>
							</td>
							<td class="text-center">
								<div class="btn-group ">
									<form action="{{ $factura->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											Zip
										</button>
									</form>	
									@if($factura->url_pdf!="")
									<form action="{{ $factura->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>	
									
									@endif
									<form action="{{ $factura->url_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
										{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											EXCEL
										</button>
									</form>
								</div>
							</td>
							<td class="col-md-2 text-center">

								<form action="{{ $factura->url_eliminar }}" method="post" class=" inline">
								   	{{ csrf_field() }}
									{!! method_field('DELETE') !!}
									<div class="btn-group btn-group-sm">

										<button class="btn btn-danger alerta-eliminar" type="submit" data-confirmar="¿Seguro que desea eliminar el factura {{ $factura->serie }} {{ $factura->numero }}?">
											<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
										</button>
									</div>
								</form>	
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="9">
								No hay facturas registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="9">
			   				{{ $listado_facturas->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
	
</div>
@endsection