@if(isset($facturas_disponibles) && count($facturas_disponibles)>0)
<option value="">Seleccione la factura</option>
<?php foreach ($facturas_disponibles as $id => $factura): ?>
<option value="{{ $factura->id }}" {{ old('id_factura',isset($nota_debito)?$nota_debito->id_factura:'')==$factura->id?'selected':'' }}>{{ $factura->serie."-".$factura->numero }}</option>
<?php endforeach ?>
@else
<option value="">No hay facturas</option>
@endif