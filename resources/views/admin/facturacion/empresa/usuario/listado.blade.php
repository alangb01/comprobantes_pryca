@extends ("layouts.admin")
@section('titulo_pagina','Listado de usuarios de '.$empresa)
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-8"><h4>@yield('titulo_pagina')</h4></div>
	  		{{-- <div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar usuarios">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div> --}}
			<div class="col-md-4">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
			</div>
	  	</div>
	  </div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr>
			   			<th>Usuario {!! $campos_orden->get('usuario') !!}</th>
			   			<th>Email {!! $campos_orden->get('email') !!}</th>
			   			<th>Clave modificada {!! $campos_orden->get('razon_social') !!}</th>
			   			<th>Estado {!! $campos_orden->get('direccion') !!}</th>
			   			<th>Creado</th>
			   			{{-- <th>Estado</th> --}}
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_usuarios) && count($listado_usuarios)>0)
					@foreach($listado_usuarios as $id=>$usuario)
						<tr class="">
							<td >{{ $usuario->usuario }}</td>
							<td >{{ $usuario->email }}</td>
							<td >{{ $usuario->forzar_cambio_clave }}</td>
							<td >{{ $usuario->estado }}</td>
							<td >{{ $usuario->created_at }}</td>
							<td class="col-md-2 text-center">
								<form action="{{ $usuario->url_eliminar }}" method="post" class=" inline">
								   	{{ csrf_field() }}
									{!! method_field('DELETE') !!}
									<div class="btn-group btn-group-sm">
										<button class="btn btn-danger alerta-eliminar" type="submit" data-confirmar="¿Seguro que desea eliminar el usuario {{ $usuario }}?">
											<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
										</button>
									</div>
								</form>	


							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">
								No hay usuarios registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="6">
			   				{{ $listado_usuarios->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
	
</div>
@endsection