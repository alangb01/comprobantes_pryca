@extends ("layouts.admin")
@section('titulo_pagina',false?'Editar usuario':'Nuevo usuario'.' de '.$empresa)
@section('content'),
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if (false)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="email" class="obligatorio">Email</label>
			    	<input type="text" class="form-control " id="email" name="email" value="{{ old('email',$usuario->email) }}" placeholder="ingrese el email del usuario"/>
			    	
			    	@if($errors->has('email')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('email') }}</span>
			    	@endif
			  	</div>
			  	<div class="col-md-12">
			  		<div class="alert alert-warning">
				  		Se creara una cuenta usando el correo ingresado y se generará clave automatica que sera enviada al correo.
				  	</div>
			  	</div>
			  	<div class="form-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>

			</form>
		</div>
	</div>
</div>
@endsection