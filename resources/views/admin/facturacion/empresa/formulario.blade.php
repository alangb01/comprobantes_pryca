@extends ("layouts.admin")
@section('titulo_pagina',false?'Editar empresa':'Nuevo empresa')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if (false)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="ruc" class="obligatorio">RUC</label>
			    	<input type="text" class="form-control " id="ruc" name="ruc" value="{{ old('ruc',$empresa->ruc) }}" placeholder="ingrese el ruc del empresa" maxlength="11">
			    	
			    	@if($errors->has('ruc')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('ruc') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="razon_social" class="obligatorio">Razon social</label>
			    	<input type="text" class="form-control " id="razon_social" name="razon_social" value="{{ old('razon_social',$empresa->razon_social) }}" placeholder="ingrese la razon social del empresa">
			    	
			    	@if($errors->has('razon_social')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('razon_social') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="direccion" class="obligatorio">Dirección</label>
			    	<input type="text" class="form-control " id="direccion" name="direccion" value="{{ old('direccion',$empresa->direccion) }}" placeholder="ingrese la dirección del empresa">
			    	
			    	@if($errors->has('direccion')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('direccion') }}</span>
			    	@endif
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection