@extends ("layouts.admin")
@section('titulo_pagina','Subir archivos de comprobante')
@section('css')
<link href="{{ asset('/css/dropzone.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		{{-- <form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			<div class="form-group col-md-12">
			    	<label for="comprimido" class="obligatorio">Comprobante ZIP (Obligatorio)</label>
			    	<input type="file" class="form-control " id="comprimido" name="comprimido" value="{{ old('comprimido') }}" placeholder="ingrese el comprimido del usuario" accept="application/zip">
			    	
			    	@if($errors->has('comprimido')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('comprimido') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="pdf" class="obligatorio">Comprobante PDF</label>
			    	<input type="file" class="form-control " id="pdf" name="pdf" value="{{ old('pdf') }}" placeholder="ingrese el pdf del usuario" accept="application/pdf">
			    	
			    	@if($errors->has('pdf')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('pdf') }}</span>
			    	@endif
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form> --}}

			{!! Form::open(['route'=> 'admin.facturacion.nota_credito.subida.procesar', 'method' => 'POST', 'files'=>'true', 'id' => 'my-dropzone' , 'class' => 'dropzone']) !!}
               		{{ csrf_field() }}
                    <div class="dz-message" style="height:200px;">
                        Sube aquí las facturas
                    </div>
                    <div class="dropzone-previews"></div>
                    <button type="submit" class="btn btn-success" id="submit">Subir</button>
                    {!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
@section('scripts')
    {!! Html::script('js/dropzone.js'); !!}
<script>
var url = "{!!route('admin.facturacion.nota_credito.listado')!!}";

Dropzone.options.myDropzone = {
             autoProcessQueue: false,
            uploadMultiple: false,
            maxFilezise: 10,
            maxFiles: 10,
            addRemoveLinks: true,
            parallelUploads: 10,
            addRemoveLinks: true,
            paramName:'comprimido',
            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;
                
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                    
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                    //$(location).attr('href',url);
                });
 
                this.on("success", 
                    myDropzone.processQueue.bind(myDropzone)
                );
            	
            }
        };


</script>
@endsection