@extends ("layouts.admin")
@section('titulo_pagina',false?'Editar nota de credito':'Nuevo nota de credito')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if (false)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="serie" class="obligatorio">Serie de la nota de credito</label>
			    	<input type="text" class="form-control " id="serie" name="serie" value="{{ old('serie',$nota_credito->serie) }}" placeholder="ingrese la serie de la nota de credito (NC001)">
			    	
			    	@if($errors->has('serie')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('serie') }}</span>
			    	@endif
			  	</div>

			  	<div class="form-group col-md-12">
			    	<label for="numero" class="obligatorio">Número de la nota de credito</label>
			    	<input type="text" class="form-control " id="numero" name="numero" value="{{ old('numero',$nota_credito->numero) }}" placeholder="ingrese el numero la nota de credito (01)">
			    	
			    	@if($errors->has('numero')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('numero') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="cliente">Concepto de nota de credito:</label>
			    	<select class="form-control" id="id_concepto" name="id_concepto">
			    		@if(isset($conceptos_disponibles) && count($conceptos_disponibles)>0)
						<option value="">Seleccione el concepto de nota de credito</option>
			    		<?php foreach ($conceptos_disponibles as $id => $concepto): ?>
			    		<option value="{{ $concepto->id }}" {{ old('id_concepto',$nota_credito->id_concepto)==$concepto->id?'selected':'' }}>{{ $concepto->nombre }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay conceptos disponibles</option>
						@endif
					</select>
					@if($errors->has('id_concepto')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('id_concepto') }}</span>
			    	@endif
			  	</div>
		  		<div class="form-group col-md-12">
			    	<label for="cliente">Cliente:</label>
			    	<select class="form-control" id="id_empresa" name="id_empresa">
			    		@if(isset($empresas_disponibles) && count($empresas_disponibles)>0)
						<option value="">Seleccione el cliente</option>
			    		<?php foreach ($empresas_disponibles as $id => $cliente): ?>
			    		<option value="{{ $cliente->id }}" {{ old('id_empresa',$nota_credito->id_empresa)==$cliente->id?'selected':'' }}>{{ "[".$cliente->ruc."]".$cliente->razon_social }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay clientes</option>
						@endif
					</select>
					@if($errors->has('id_empresa')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('id_empresa') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="cliente">Factura:</label>
			    	<select class="form-control" id="id_factura" name="id_factura">
			    		@include('admin.facturacion.nota_credito.combo-facturas')
					</select>
					@if($errors->has('id_factura')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('id_factura') }}</span>
			    	@endif
			  	</div>

			  	<div class="form-group col-md-12">
			    	<label for="fecha_emision" class="obligatorio">Fecha emisión</label>
			    	<input type="text" class="form-control datepicker" id="fecha_emision" name="fecha_emision" value="{{ old('fecha_emision',$nota_credito->fecha_emision) }}" placeholder="ingrese el fechaemision del nota_credito">
			    	
			    	@if($errors->has('fecha_emision')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('fecha_emision') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="importe" class="obligatorio">Importe</label>
			    	<input type="text" class="form-control " id="importe" name="importe" value="{{ old('importe',$nota_credito->importe) }}" placeholder="ingrese el importe del nota_credito (01)">
			    	
			    	@if($errors->has('importe')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('importe') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="zip" class="obligatorio">Archivo zip</label>
			    	<input type="file" class="form-control " id="zip" name="zip" value="{{ old('zip') }}" placeholder="ingrese el zip del nota_credito" accept="application/zip">
			    	
			    	@if($errors->has('zip')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('zip') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="pdf" class="obligatorio">Archivo pdf</label>
			    	<input type="file" class="form-control " id="pdf" name="pdf" value="{{ old('pdf') }}" placeholder="ingrese el pdf del nota_credito" accept="application/pdf">
			    	
			    	@if($errors->has('pdf')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('pdf') }}</span>
			    	@endif
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script>
	$(document).on('change','#id_empresa',function(){
		data={'id_empresa':$(this).val()}
		$.get("{{ route('admin.facturacion.nota_credito.combo.comprobantes') }}",data,function(respuesta){
			$("#id_factura").html(respuesta);
		})
	})
</script>

@endpush