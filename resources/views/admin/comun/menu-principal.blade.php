<ul class="nav navbar-nav">
    <li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      Facturacion <b class="caret"></b>
	    </a>
	    <ul class="dropdown-menu">
	      <li><a href="{{ route('admin.facturacion.factura.listado') }}">Facturas</a></li>
	      <li><a href="{{ route('admin.facturacion.nota_credito.listado') }}">Notas de credito</a></li>
	      {{--<li><a href="{{ route('admin.facturacion.nota_debito.listado') }}">Notas de debito</a></li>--}}
	      {{--<li class="divider"></li>--}}
	      {{--<li><a href="{{ route('admin.facturacion.empresa.listado') }}">Empresas</a></li>--}}
	      
	      {{-- <li class="divider"></li> --}}
	    </ul>
	</li>
	<li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	      Boleteo <b class="caret"></b>
	    </a>
	    <ul class="dropdown-menu">
	      <li><a href="{{ route('admin.boleteo.boleta.listado') }}">Boletas</a></li>
	      <li><a href="{{ route('admin.boleteo.nota_credito.listado') }}">Notas de credito</a></li>
	      {{--<li><a href="{{ route('admin.boleteo.nota_debito.listado') }}">Notas de debito</a></li>--}}
	      {{--<li class="divider"></li>--}}
	      {{--<li><a href="{{ route('admin.boleteo.persona.listado') }}">Personas</a></li>--}}
	      
	      {{-- <li class="divider"></li> --}}
	    </ul>
	</li>
</ul>
