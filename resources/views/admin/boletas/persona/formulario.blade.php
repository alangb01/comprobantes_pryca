@extends ("layouts.admin")
@section('titulo_pagina',false?'Editar persona':'Nuevo persona')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if (false)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="dni" class="obligatorio">DNI</label>
			    	<input type="text" class="form-control " id="dni" name="dni" value="{{ old('dni',$persona->dni) }}" placeholder="ingrese el dni del persona" maxlength="8">
			    	
			    	@if($errors->has('dni')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('dni') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="nombre" class="obligatorio">Nombre</label>
			    	<input type="text" class="form-control " id="nombre" name="nombre" value="{{ old('nombre',$persona->nombre) }}" placeholder="ingrese la razon social del persona">
			    	
			    	@if($errors->has('nombre')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('nombre') }}</span>
			    	@endif
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection