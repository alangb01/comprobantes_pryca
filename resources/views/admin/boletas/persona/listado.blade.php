@extends ("layouts.admin")
@section('titulo_pagina','Listado de persona')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar persona">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
			</div>
	  	</div>
	  </div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr>
			   			<th>
			   				Dni {!! $campos_orden->get('dni') !!}
			   			</th>
			   			<th>
			   				Apellido {!! $campos_orden->get('nombre') !!}
			   			</th>
			   			<th>
			   				Usuarios
			   			</th>
			   			{{-- <th>Estado</th> --}}
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_personas) && count($listado_personas)>0)
					@foreach($listado_personas as $id=>$persona)
						<tr class="">
							<td >{{ $persona->dni }}</td>
							<td >{{ $persona->nombre }}</td>
							
							<td>
								<div class="btn-group btn-group-sm">
									<a href="{{ $persona->url_usuarios }}" class="btn btn-primary"> {{ $persona->cuentas->count() }} usuarios </a>
								</div>
							</td>
							<td class="col-md-2 text-center">

								<form action="{{ $persona->url_eliminar }}" method="post" class=" inline">
								   	{{ csrf_field() }}
									{!! method_field('DELETE') !!}
									<div class="btn-group btn-group-sm">
										<button class="btn btn-danger alerta-eliminar" type="submit" data-confirmar="¿Seguro que desea eliminar la persona {{ $persona }}?">
											<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
										</button>
									</div>
								</form>	


							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="5">
								No hay persona registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="5">
			   				{{ $listado_personas->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
</div>
@endsection