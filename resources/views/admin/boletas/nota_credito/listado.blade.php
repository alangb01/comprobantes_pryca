@extends ("layouts.admin")
@section('titulo_pagina','Listado de notas de credito')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar notas de credito">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>
	  		</div>
			<div class="col-md-4 text-right">
				<div class="btn-group ">
					<a href="{{ $url_nuevo }}" class="btn btn-success ">Nuevo</a>
				</div>
				<div class="btn-group ">
					<a href="{{ $url_subida }}" class="btn btn-warning ">Subir</a>
				</div>
			</div>
	  	</div>
	  </div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr class="">
			   			<th class="col-md-1 text-center">Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th class="col-md-1 text-center">Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th class="col-md-1 text-center">DNI {!! $campos_orden->get('dni') !!}</th>
			   			<th class="col-md-2 text-center">Nombre {!! $campos_orden->get('nombre') !!}</th>
			   			<th class="col-md-1 text-center">Fecha   {!! $campos_orden->get('fecha_emision') !!}</th>

			   			<th class="col-md-1 text-center">Importe  {!! $campos_orden->get('importe') !!}</th>
			   			<th class="col-md-1 text-center">Boleta</th>
			   			<th class="col-md-1 text-center">Descarga</th>
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_notas_credito) && count($listado_notas_credito)>0)
					@foreach($listado_notas_credito as $id=>$nota_credito)
						<tr class="">
							<td class="text-center">{{ $nota_credito->serie }}</td>
							<td class="text-right">{{ str_pad($nota_credito->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >{{ $nota_credito->persona->dni }}</td>
							<td >{{ $nota_credito->persona->nombre }}</td>
							<td >{{ $nota_credito->fecha_emision->format("d/m/Y") }}</td>
							<td class="text-right">{{ number_format($nota_credito->importe,2) }}</td>
							<td class="text-center">{{ $nota_credito->boleta!=null?$nota_credito->boleta->serie."-".$nota_credito->boleta->numero:'No existe la boleta asociada' }}
							</td>
							<td class="text-center">
								<div class="btn-group ">
									<form action="{{ $nota_credito->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											Zip
										</button>
									</form>	
									@if($nota_credito->url_pdf!="")
									<form action="{{ $nota_credito->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>
									@endif
									@if($nota_credito->url_excel!="")
										<form action="{{ $nota_credito->url_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
											{{ csrf_field() }}
											<button type="submit"  class="btn btn-primary">
												Excel
											</button>
										</form>
									@endif
								</div>
							</td>
							<td class="col-md-2 text-center">
								<form action="{{ $nota_credito->url_eliminar }}" method="post" class=" inline">
								   	{{ csrf_field() }}
									{!! method_field('DELETE') !!}
									<div class="btn-group btn-group-sm">

										<button class="btn btn-danger alerta-eliminar" type="submit" data-confirmar="¿Seguro que desea eliminar el nota_credito {{ $nota_credito->serie }} {{ $nota_credito->numero }}?">
											<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
										</button>
									</div>
								</form>	
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="10">
								No hay notas_credito registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="10">
			   				{{ $listado_notas_credito->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
</div>
@endsection