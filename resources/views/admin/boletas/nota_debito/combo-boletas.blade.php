@if(isset($boletas_disponibles) && count($boletas_disponibles)>0)
<option value="">Seleccione la boleta</option>
<?php foreach ($boletas_disponibles as $id => $boleta): ?>
<option value="{{ $boleta->id }}" {{ old('id_boleta',isset($nota_debito)?$nota_debito->id_boleta:'')==$boleta->id?'selected':'' }}>{{ $boleta->serie."-".$boleta->numero }}</option>
<?php endforeach ?>
@else
<option value="">No hay boletas</option>
@endif