<form id="asociar" action="{{ $boleta->action_form }}" method="post">
	{{ csrf_field() }}
	<input type="hidden" value="{{ $boleta->id }}">
	<div class="">
		<label for="">Numero Serie</label>
		<input type="text" readonly value="{{ $boleta->serie."-".$boleta->numero }}" class="form-control">
	</div>
	<div class="">
		<label for="">Cliente</label>
		<input type="text" readonly value="{{ $boleta->nombre }}" class="form-control">
	</div>
	<div class="">
		<label for="">Importe</label>
		<input type="text" readonly value="{{ $boleta->importe }}" class="form-control">
	</div>
	<div>
		<label for="">Cliente</label>
		<select class="form-control" id="id_persona" name="id_persona">
    		@if(isset($personas_disponibles) && count($personas_disponibles)>0)
			<option value="">Seleccione el cliente</option>
    		<?php foreach ($personas_disponibles as $id => $cliente): ?>
    		<option value="{{ $cliente->id }}" {{ old('id_persona',$boleta->id_persona)==$cliente->id?'selected':'' }}>{{ "[".$cliente->dni."]".$cliente }}</option>
    		<?php endforeach ?>
    		@else
    		<option value="">No hay clientes</option>
			@endif
		</select>
	</div>
</form>