@extends ("layouts.admin")
@section('titulo_pagina','Listado de boletas')
@section('content')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar boletas">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>
	  		</div>
			<div class="col-md-4 text-right">
				<div class="btn-group ">
					<a href="{{ $url_nuevo }}" class="btn btn-success ">Nuevo</a>
				</div>
				<div class="btn-group ">
					<a href="{{ $url_subida }}" class="btn btn-warning ">Subir</a>
				</div>
			</div>
	  	</div>
	  </div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr class="">
			   			<th class="col-md-1 text-center">Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th class="col-md-1 text-center">Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th class="col-md-1 text-center">DNI {!! $campos_orden->get('dni') !!}</th>
			   			<th class="col-md-2 text-center">Nombre {!! $campos_orden->get('nombre') !!}</th>
			   			<th class="col-md-1 text-center">Fecha   {!! $campos_orden->get('fecha_emision') !!}</th>
			   			<th class="col-md-1 text-center">Importe  {!! $campos_orden->get('importe') !!}</th>
			   			<th class="col-md-2 text-center">Estado</th>
			   			<th class="col-md-2 text-center">Descarga</th>
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_boletas) && count($listado_boletas)>0)
					@foreach($listado_boletas as $id=>$boleta)
						<tr class="">
							<td class="text-center">{{ $boleta->serie }}</td>
							<td class="text-right">{{ str_pad($boleta->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >
								@if($boleta->persona!=null)
								{{ $boleta->persona->dni }}
								@else
									<a href="{{ route('admin.boleteo.boleta.asociar',$boleta->id) }}" class="btn btn-danger modal-asociado">Sin asociar</a>
								@endif

							</td>
							<td >{{ $boleta->persona?$boleta->persona->nombre:$boleta->nombre }}</td>
							<td >{{ $boleta->fecha_emision->format("d/m/Y") }}</td>
							<td class="text-right">{{ number_format($boleta->importe,2) }}</td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									@if($boleta->estado()==$boleta::ESTADO_EMITIDO)
									<div class="btn alert-success text-center " style="display:inline-block">{{ $boleta->estadoToString() }}</div>
									@elseif($boleta->estado()==$boleta::ESTADO_ANULADO)
									<div class="btn alert-danger text-center" style="display:inline-block">{{ $boleta->estadoToString() }}</div>
									@elseif($boleta->estado()==$boleta::ESTADO_MODIFICADO)
									<div class="btn alert-warning text-center" style="display:inline-block">{{ $boleta->estadoToString() }}</div>
									@endif
								</div>
							</td>
							<td class="text-center">
								<div class="btn-group ">
									<form action="{{ $boleta->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											Zip
										</button>
									</form>	
									@if($boleta->url_pdf!="")
									<form action="{{ $boleta->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>
									@endif
										<form action="{{ $boleta->url_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
											{{ csrf_field() }}
											<button type="submit"  class="btn btn-primary">
												EXCEL
											</button>
										</form>
								</div>
							</td>
							<td class="col-md-2 text-center">

								<form action="{{ $boleta->url_eliminar }}" method="post" class=" inline">
								   	{{ csrf_field() }}
									{!! method_field('DELETE') !!}
									<div class="btn-group btn-group-sm">
										{{--data-confirmar="¿Seguro que desea eliminar el boleta {{ $boleta->serie }} {{ $boleta->numero }}?"--}}
										<button class="btn btn-danger alerta-eliminar" type="submit">
											<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
										</button>
									</div>
								</form>	
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="9">
								No hay boletas registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="9">
			   				{{ $listado_boletas->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
	
</div>
@endsection