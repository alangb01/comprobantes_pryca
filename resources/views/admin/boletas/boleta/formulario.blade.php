
@extends ("layouts.admin")
@section('titulo_pagina',false?'Editar boleta':'Nuevo boleta')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if (false)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="serie" class="obligatorio">Serie del boleta</label>
			    	<input type="text" class="form-control " id="serie" name="serie" value="{{ old('serie',$boleta->serie) }}" placeholder="ingrese la serie del boleta (FE001)">
			    	
			    	@if($errors->has('serie')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('serie') }}</span>
			    	@endif
			  	</div>

			  	<div class="form-group col-md-12">
			    	<label for="numero" class="obligatorio">Número del boleta</label>
			    	<input type="text" class="form-control " id="numero" name="numero" value="{{ old('numero',$boleta->numero) }}" placeholder="ingrese el numero del boleta (01)">
			    	
			    	@if($errors->has('numero')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('numero') }}</span>
			    	@endif
			  	</div>
		  		<div class="form-group col-md-12">
			    	<label for="cliente">Cliente:</label>
			    	<select class="form-control" id="id_persona" name="id_persona">
			    		@if(isset($personas_disponibles) && count($personas_disponibles)>0)
						<option value="">Seleccione el cliente</option>
			    		<?php foreach ($personas_disponibles as $id => $cliente): ?>
			    		<option value="{{ $cliente->id }}" {{ old('id_persona',$boleta->id_persona)==$cliente->id?'selected':'' }}>{{ "[".$cliente->dni."]".$cliente }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay clientes</option>
						@endif
					</select>
					@if($errors->has('id_persona')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('id_persona') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="fecha_emision" class="obligatorio">Fecha emisión</label>
			    	<input type="text" class="form-control datepicker" id="fecha_emision" name="fecha_emision" value="{{ old('fecha_emision',$boleta->fecha_emision) }}" placeholder="ingrese el fechaemision del boleta">
			    	
			    	@if($errors->has('fecha_emision')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('fecha_emision') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="importe" class="obligatorio">Importe</label>
			    	<input type="text" class="form-control " id="importe" name="importe" value="{{ old('importe',$boleta->importe) }}" placeholder="ingrese el importe del boleta (01)">
			    	
			    	@if($errors->has('importe')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('importe') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="zip" class="obligatorio">Archivo zip</label>
			    	<input type="file" class="form-control " id="zip" name="zip" value="{{ old('zip') }}" placeholder="ingrese el zip del boleta" accept="application/zip">
			    	
			    	@if($errors->has('zip')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('zip') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	{{--<label for="pdf" class="obligatorio">Archivo pdf</label>--}}
			    	{{--<input type="file" class="form-control " id="pdf" name="pdf" value="{{ old('pdf') }}" placeholder="ingrese el pdf del boleta" accept="application/pdf">--}}
			    	{{----}}
			    	{{--@if($errors->has('pdf')!=null)--}}
			    	{{--<span class="help-inline alerta-campo">{{ $errors->first('pdf') }}</span>--}}
			    	{{--@endif--}}
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection