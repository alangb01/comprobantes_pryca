@extends ("layouts.admin")
@section('titulo_pagina','Editar mis datos')
@section('content')
<div class="container">
	@include('admin.comun.general')
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"></div>
				<div class="col-md-6 text-uppercase">
					<h5>@yield('titulo_pagina')</h5>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			{{-- @if ($usuario->id) --}}
				{!!  method_field('PUT') !!}
	   			{{-- @endif --}}
			  	
			  	<div class="form-group col-md-12">
			    	<label for="usuario">Usuario:</label>
			    	<input type="text" class="form-control" id="usuario" name="usuario" value="{{ old('usuario',$usuario->usuario) }}" placeholder="Ingrese un alias de usuario">
			    	@if($errors->has('usuario')!=null)
			    	<span class="help-inline">{{ $errors->first('usuario') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="email">Correo :</label>
			    	<input type="text" class="form-control" id="email" name="email"  value="{{ old('email',$usuario->email) }}" placeholder="Ingrese un correo de contacto" autocomplete="off">
			    	@if($errors->has('email')!=null)
			    	<span class="help-inline">{{ $errors->first('email') }}</span>
			    	@endif
			  	</div>

			  	<div class="form-group col-md-12">
  			    	<label for="password">Clave actual:</label>
  			    	<input type="password" class="form-control" id="password" name="password"  value="" placeholder="Ingrese su clave actual" autocomplete="off">
  			    	@if($errors->has('password')!=null)
  			    	<span class="help-inline">{{ $errors->first('password') }}</span>
  			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Registrar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection