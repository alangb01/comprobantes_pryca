@extends ("layouts.empresas")
@section('titulo_pagina','Facturas'.(isset($empresa)?" de ".$empresa:' encontradas'))
@section('content')
<div class="container">
	@include('admin.comun.notificaciones')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
		<div class="panel-heading">
		  	<div class="row">
				<div class="col-md-6 text-left">
					<a href="{{ $url_regresar }}" class="btn btn-success">Regresar</a>
				</div>
		  		<div class="col-md-6 text-center"><h4>@yield('titulo_pagina')</h4></div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12 text-center ">
		  			
		  		</div>
		  	</div>
		</div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr>
			   			<th>Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th>Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th>Fecha de emisión {!! $campos_orden->get('numero') !!}</th>
			   			<th>Importe {!! $campos_orden->get('numero') !!}</th>
			   			<th>Estado</th>
			   			<th class="col-md-2 text-center">Opciones</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_facturas) && count($listado_facturas)>0)
					@foreach($listado_facturas as $id=>$factura)
						<tr class="">
							<td >{{ $factura->serie }}</td>
							<td >{{ str_pad($factura->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >{{ $factura->fecha_emision->format("d/m/Y") }}</td>
							<td class="text-right">{{ number_format($factura->importe,2) }}</td>
							<td class="text-center btn-group-sm">
								<div class="btn-group btn-group-sm">
									@if($factura->estado()==$factura::ESTADO_EMITIDO)
									<div class="btn alert-success text-center " style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@elseif($factura->estado()==$factura::ESTADO_ANULADO)
									<div class="btn alert-danger text-center" style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@elseif($factura->estado()==$factura::ESTADO_MODIFICADO)
									<div class="btn alert-warning text-center" style="display:inline-block">{{ $factura->estadoToString() }}</div>
									@endif
								</div>
							</td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									<form action="{{ $factura->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											ZIP
										</button>
									</form>	
									
{{--									@if($factura->url_pdf!="")--}}
									<form action="{{ $factura->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
									   	{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>
									{{--@endif--}}
									<form action="{{ $factura->url_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
										{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											EXCEL
										</button>
									</form>
								</div>
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">
								No hay facturas registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="6">
			   				{{ $listado_facturas->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
	
</div>
@endsection