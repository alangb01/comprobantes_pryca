@extends ("layouts.empresas")
@section('titulo_pagina','Notas de credito'.(isset($empresa)?" de ".$empresa:' encontradas'))
@section('content')
@include('admin.comun.notificaciones')
<div class="container">
	<div class="panel panel-default">
	  <!-- Default panel contents -->
		<div class="panel-heading">
		  	<div class="row">
				<div class="col-md-3">
					<a href="{{ $url_regresar }}" class="btn btn-success">Regresar</a>
				</div>
		  		<div class="col-md-6 text-center"><h4>@yield('titulo_pagina')</h4></div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12 text-center ">
		  			
		  		</div>
		  	</div>
		</div>
	  	<div class="panel-body">
	  		<table class="table table-bordered table-condensed table-hover ">
			   	<thead>
			   		<tr>
			   			<th>Serie {!! $campos_orden->get('serie') !!}</th>
			   			<th>Numero {!! $campos_orden->get('numero') !!}</th>
			   			<th>Fecha de emisión {!! $campos_orden->get('numero') !!}</th>
			   			<th>Concepto {!! $campos_orden->get('concepto') !!}</th>
			   			<th>Importe {!! $campos_orden->get('numero') !!}</th>
			   			<th>Factura {!! $campos_orden->get('factura') !!}</th>
			   			{{-- <th>Estado</th> --}}
			   			<th class="col-md-2 text-center">Nota de Crédito</th>
			   		</tr>
			   	</thead>
			   	<tbody>
			   		@if(isset($listado_notas_credito) && count($listado_notas_credito)>0)
					@foreach($listado_notas_credito as $id=>$nota_credito)
						<tr class="">
							<td >{{ $nota_credito->serie }}</td>
							<td >{{ str_pad($nota_credito->numero, 2, "0", STR_PAD_LEFT)  }}</td>
							<td >{{ $nota_credito->fecha_emision->format("d/m/Y") }}</td>
							<td >{{ $nota_credito->conceptoToString() }}</td>
							<td class="text-right">{{ number_format($nota_credito->importe,2) }}</td>
							<td class="text-center">
								@if(isset($nota_credito->factura))
								<form action="{{ $nota_credito->url_boleta_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
									{{ csrf_field() }}
									<button type="submit" class="btn btn-primary">
										Zip
									</button>
								</form>


									<form action="{{ $nota_credito->url_boleta_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
										{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											PDF
										</button>
									</form>
									<form action="{{ $nota_credito->url_boleta_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
										{{ csrf_field() }}
										<button type="submit"  class="btn btn-primary">
											EXCEL
										</button>
									</form>
									@else
									No se encuentra la factura
								@endif

							</td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									<form action="{{ $nota_credito->url_zip }}" method="post" class=" inline btn-group-sm" style="display:inline-block" target="_blank">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-primary">
											ZIP
										</button>
									</form>

{{--									@if($nota_credito->url_pdf!="")--}}
										<form action="{{ $nota_credito->url_pdf }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
											{{ csrf_field() }}
											<button type="submit"  class="btn btn-primary">
												PDF
											</button>
										</form>

									{{--@endif--}}
{{--									@if($nota_credito->url_excel!="")--}}
										<form action="{{ $nota_credito->url_excel }}" method="post" class=" inline btn-group-sm"  style="display:inline-block" target="_blank">
											{{ csrf_field() }}
											<button type="submit"  class="btn btn-primary">
												EXCEL
											</button>
										</form>

									{{--@endif--}}
								</div>
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="8">
								No hay notas de credito registrados
							</td>
						</tr>
					@endif
			   	</tbody>
			   	<tfoot>
			   		<tr>
			   			<td colspan="8">
			   				{{ $listado_notas_credito->links() }}
			   			</td>
			   		</tr>
			   	</tfoot>
			  </table>
	  	</div>
	</div>
	
</div>
@endsection