@section('titulo_app','Sistema de comprobantes')
@extends('layouts.app')
@section('content')
<div class="flex-center position-ref full-height">
    {{-- @if (Route::has('login')) --}}
        <div class="top-right links ">
            {{--<a href="{{ url('admin') }}">Administracion</a>--}}
            {{--<a href="{{ url('personas') }}">Personas</a>--}}
            <div class="btn-group ">
                <div class="dropdown links col-md-5">
                    <a class="dropdown-toggle" id="dropdownMenu1"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Personas
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="{{ route('persona.ver.boleta') }}">Boletas</a></li>
                        <li><a href="{{route('persona.ver.nota.credito') }}">Notas de Crédito</a></li>
                    </ul>
                </div>
                <div class="dropdown links col-md-3">
                    <a class="dropdown-toggle" id="dropdownMenu2"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Empresas
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <li><a href="{{ route('empresa.ver.factura') }}">Facturas</a></li>
                        <li><a href="{{ route('empresa.ver.nota.credito') }}">Notas de Crédito</a></li>
                    </ul>
                </div>
            </div>


            {{--<a href="{{ url('empresas') }}">Empresas</a>--}}
            {{-- <a href="{{ url('register') }}">Registrar</a> --}}
            {{-- <a href="{{ url('admin/login') }}">Admin</a> --}}

        </div>

    {{-- @endif --}}

    <div class="content">
        <div class="title m-b-md">
            @yield('titulo_app')
        </div>

    </div>
</div>
@endsection