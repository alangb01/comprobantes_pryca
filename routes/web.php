<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('test',function(){
    $comprobante = new \App\Console\Commands\ProcesarComprobantesPendientes();
    $comprobante->procesarArchivosPendientes();
});

// Auth::routes();

Route::group(['namespace'=>'Admin'], function(){
    Route::group(['namespace'=>'Facturacion'],function(){
        Route::get('pruebaftp/facturas','FacturasController@almacenarSubidaFTP');
        Route::get('pruebaftp/facturas/notas/credito','NotasCreditoController@almacenarSubidaFTP');

    });
    Route::group(['namespace'=>'Boleteo'],function(){
        Route::get('pruebaftp/boletas','BoletasController@almacenarSubidaFTP');
        Route::get('pruebaftp/boletas/notas/credito','NotasCreditoController@almacenarSubidaFTP');
    });
});

//EMPRESA
Route::group(['middleware' => ['web'],'prefix'=>'empresas','namespace'=>'Empresas'],function(){
	Route::group(['namespace'=>'Auth'],function(){
		//Login Routes...
	    Route::get('login',['as'=>'empresa.login','uses'=>'LoginController@showLoginForm']);
	    Route::post('login',['as'=>'empresa.login.procesar','uses'=>'LoginController@login']);
	    Route::post('logout',['as'=>'empresa.logout','uses'=>'LoginController@logout']);

	    // Registration Routes...
	    Route::get('register', ['as'=>'empresa.registrar','uses'=>'LoginController@showRegistrationForm']);
	    Route::post('register', ['as'=>'empresa.registrar.procesar','uses'=>'LoginController@register']);

	    // Registration Routes...
	    Route::get('password/reset', ['as'=>'empresa.reset.clave','uses'=>'ForgotPasswordController@showLinkRequestForm']);
	    Route::post('password/email', ['as'=>'empresa.reset.clave.procesar','uses'=>'ForgotPasswordController@sendResetLinkEmail']);

	    // Registration Routes...
	    Route::get('password/reset/{token}', ['as'=>'empresa.reset.cambiar','uses'=>'ResetPasswordController@showResetForm']);
	    Route::post('password/reset', ['as'=>'empresa.reset.cambiar.procesar','uses'=>'ResetPasswordController@reset']);
	});

	Route::get('/', ['as'=>'empresa.panel','uses'=>'UsuarioController@index']);

	//USUARIO > CUENTA
	Route::get('cuenta/cambiar-clave',['as'=>'empresa.cuenta.cambiar-clave','uses'=>'UsuarioController@cambiarClave']);
	Route::put('cuenta/cambiar-clave',['as'=>'empresa.cuenta.procesar-cambio-clave','uses'=>'UsuarioController@procesarCambioClave']);
	Route::get('cuenta/cambiar-datos',['as'=>'empresa.cuenta.cambiar-datos','uses'=>'UsuarioController@editar']);
	Route::put('cuenta/cambiar-datos',['as'=>'empresa.cuenta.procesar-cambio-datos','uses'=>'UsuarioController@actualizar']);


	//USUARIO > FACTURAs DE EMPRESA ASOCIADA
	Route::get('facturas', ['as'=>'empresa.facturas','uses'=>'FacturasController@listado']);
	//USUARIO > FACTURAs DE EMPRESA ASOCIADA > DESCARGA
	Route::post('descargar/zip/factura/{id_factura}',['as'=>'empresa.descargar.factura.zip','uses'=>'FacturasController@descargarZIP']);
	Route::post('descargar/pdf/factura/{id_factura}',['as'=>'empresa.descargar.factura.pdf','uses'=>'FacturasController@descargarPDF']);
    Route::post('descargar/excel/factura/{id_factura}',['as'=>'empresa.descargar.factura.excel','uses'=>'FacturasController@descargarEXCEL']);


    //USUARIO > NOTAS CREDITO DE EMPRESA ASOCIADA
	Route::get('notas-credito', ['as'=>'empresa.notas_credito','uses'=>'NotasCreditoController@listado']);
	//ADMIN > NOTA DE CREDITO > DESCARGA
	Route::post('descargar/zip/nota-credito/{id_nota_credito}',['as'=>'empresa.descargar.nota_credito.zip','uses'=>'NotasCreditoController@descargarZIP']);
	Route::post('descargar/pdf/nota-credito/{id_nota_credito}',['as'=>'empresa.descargar.nota_credito.pdf','uses'=>'NotasCreditoController@descargarPDF']);


	//USUARIO > NOTAS DEBITO DE EMPRESA ASOCIADA
	Route::get('notas-debito', ['as'=>'empresa.notas_debito','uses'=>'NotasDebitoController@listado']);
	//ADMIN > NOTA DE DEBITO > DESCARGA
	Route::post('descargar/zip/nota-debito/{id_nota_debito}',['as'=>'empresa.descargar.nota_debito.zip','uses'=>'NotasDebitoController@descargarZIP']);
	Route::post('descargar/pdf/nota-debito/{id_nota_debito}',['as'=>'empresa.descargar.nota_debito.pdf','uses'=>'NotasDebitoController@descargarPDF']);
});

//PERSONA
Route::group(['middleware' => ['web'],'prefix'=>'personas','namespace'=>'Personas'],function(){
	Route::group(['namespace'=>'Auth'],function(){
		//Login Routes...
	    Route::get('login',['as'=>'persona.login','uses'=>'LoginController@showLoginForm']);
	    Route::post('login',['as'=>'persona.login.procesar','uses'=>'LoginController@login']);
	    Route::post('logout',['as'=>'persona.logout','uses'=>'LoginController@logout']);

	    // Registration Routes...
	    Route::get('register', ['as'=>'persona.registrar','uses'=>'LoginController@showRegistrationForm']);
	    Route::post('register', ['as'=>'persona.registrar.procesar','uses'=>'LoginController@register']);

	    // Registration Routes...
	    Route::get('password/reset', ['as'=>'persona.reset.clave','uses'=>'ForgotPasswordController@showLinkRequestForm']);
	    Route::post('password/email', ['as'=>'persona.reset.clave.procesar','uses'=>'ForgotPasswordController@sendResetLinkEmail']);

	    // Registration Routes...
	    Route::get('password/reset/{token}', ['as'=>'persona.reset.cambiar','uses'=>'ResetPasswordController@showResetForm']);
	    Route::post('password/reset', ['as'=>'persona.reset.cambiar.procesar','uses'=>'ResetPasswordController@reset']);
	});

	Route::get('/', ['as'=>'persona.panel','uses'=>'UsuarioController@index']);

	//USUARIO > CUENTA
	Route::get('cuenta/cambiar-clave',['as'=>'persona.cuenta.cambiar-clave','uses'=>'UsuarioController@cambiarClave']);
	Route::put('cuenta/cambiar-clave',['as'=>'persona.cuenta.procesar-cambio-clave','uses'=>'UsuarioController@procesarCambioClave']);
	Route::get('cuenta/cambiar-datos',['as'=>'persona.cuenta.cambiar-datos','uses'=>'UsuarioController@editar']);
	Route::put('cuenta/cambiar-datos',['as'=>'persona.cuenta.procesar-cambio-datos','uses'=>'UsuarioController@actualizar']);


	//USUARIO > BOLETAS DE PERSONA ASOCIADA
	Route::get('boletas', ['as'=>'persona.boletas','uses'=>'BoletasController@listado']);



    //USUARIO > NOTAS CREDITO DE PERSONA ASOCIADA
	Route::get('notas-credito', ['as'=>'persona.notas_credito','uses'=>'NotasCreditoController@listado']);
	//ADMIN > NOTA DE CREDITO > DESCARGA
	Route::post('descargar/zip/nota-credito/{id_nota_credito}',['as'=>'persona.descargar.nota_credito.zip','uses'=>'NotasCreditoController@descargarZIP']);
	Route::post('descargar/pdf/nota-credito/{id_nota_credito}',['as'=>'persona.descargar.nota_credito.pdf','uses'=>'NotasCreditoController@descargarPDF']);


	//USUARIO > NOTAS DEBITO DE PERSONA ASOCIADA
	Route::get('notas-debito', ['as'=>'persona.notas_debito','uses'=>'NotasDebitoController@listado']);
	//ADMIN > NOTA DE DEBITO > DESCARGA
	Route::post('descargar/zip/nota-debito/{id_nota_debito}',['as'=>'persona.descargar.nota_debito.zip','uses'=>'NotasDebitoController@descargarZIP']);
	Route::post('descargar/pdf/nota-debito/{id_nota_debito}',['as'=>'persona.descargar.nota_debito.pdf','uses'=>'NotasDebitoController@descargarPDF']);
});

//USUARIO > FACTURAs DE PERSONA ASOCIADA > DESCARGA


Route::group(['prefix' => 'personas'],function (){
    //BOLETAS
    Route::get('ver/boleta',['as'=>'persona.ver.boleta','uses'=>'ComprobantesController@verBoleta']);
    Route::get('ver/boleta/buscar',['as'=>'persona.buscar.boleta','uses'=>'ComprobantesController@buscarBoleta']);
    Route::get('ver/boleta/resultados',['as'=>'persona.buscar.boleta.resultados','uses'=>'ComprobantesController@resultadosBoletas']);
//    Route::get('ver/boleta/listar/{dni}',['as'=>'persona.listar.dni.boleta','uses'=>'ComprobantesController@listarBoletaDni']);
//    Route::get('ver/boleta/listar/{serie}/{numero}',['as'=>'persona.listar.serie.numero.boleta','uses'=>'ComprobantesController@listarBoletaSerieNumero']);

    Route::post('descargar/zip/boleta/{id_boleta}',['as'=>'persona.descargar.boleta.zip','uses'=>'ComprobantesController@descargarZIP']);
    Route::post('descargar/pdf/boleta/{id_boleta}',['as'=>'persona.descargar.boleta.pdf','uses'=>'ComprobantesController@descargarPDF']);
    Route::post('descargar/excel/boleta/{id_boleta}',['as'=>'persona.descargar.boleta.excel','uses'=>'ComprobantesController@descargarEXCEL']);

    //NOTAS DE CRÉDITO BOLETAS
    Route::group(['prefix' => 'ver/nota'],function(){
        Route::get('credito',['as'=>'persona.ver.nota.credito','uses'=>'ComprobantesController@verNotaCreditoBoleta']);
        Route::get('credito/buscar',['as'=>'persona.buscar.nota.credito','uses'=>'ComprobantesController@buscarNotaCreditoBoleta']);
        Route::get('credito/resultados',['as'=>'persona.buscar.nota.credito.resultados','uses'=>'ComprobantesController@resultadosNotasCreditoBoleta']);
//        Route::get('credito/listar/{dni}',['as'=>'persona.listar.dni.nota.credito','uses'=>'ComprobantesController@listarNotaCreditoDniBoleta']);
//        Route::get('credito/listar/{serie}/{numero}',['as'=>'persona.listar.serie.numero.nota.credito','uses'=>'ComprobantesController@listarNotaCreditoSerieNumeroBoleta']);

        Route::post('descargar/zip/credito/{id_boleta}',['as'=>'persona.descargar.boleta.nota.credito.zip','uses'=>'ComprobantesController@descargarBoletaNotaCreditoZIP']);
        Route::post('descargar/pdf/credito/{id_boleta}',['as'=>'persona.descargar.boleta.nota.credito.pdf','uses'=>'ComprobantesController@descargarBoletaNotaCreditoPDF']);
        Route::post('descargar/excel/credito/{id_boleta}',['as'=>'persona.descargar.boleta.nota.credito.excel','uses'=>'ComprobantesController@descargarBoletaNotaCreditoEXCEL']);

    });

});

Route::group(['prefix' => 'empresas'],function (){
    Route::get('ver/factura',['as'=>'empresa.ver.factura','uses'=>'ComprobantesController@verFactura']);
    Route::get('ver/factura/buscar',['as'=>'empresa.buscar.factura','uses'=>'ComprobantesController@buscarFactura']);
    Route::get('ver/factura/resultados',['as'=>'empresa.buscar.factura.resultados','uses'=>'ComprobantesController@resultadosFacturas']);
//    Route::get('ver/factura/listar/{ruc}',['as'=>'empresa.listar.ruc.factura','uses'=>'ComprobantesController@listarFacturaRuc']);
//    Route::get('ver/factura/listar/{serie}/{numero}',['as'=>'empresa.listar.serie.numero.factura','uses'=>'ComprobantesController@listarFacturaSerieNumero']);

    Route::post('descargar/zip/factura/{id_factura}',['as'=>'empresa.descargar.factura.zip','uses'=>'ComprobantesController@descargarZIPFactura']);
    Route::post('descargar/pdf/factura/{id_factura}',['as'=>'empresa.descargar.factura.pdf','uses'=>'ComprobantesController@descargarPDFFactura']);
    Route::post('descargar/excel/factura/{id_factura}',['as'=>'empresa.descargar.factura.excel','uses'=>'ComprobantesController@descargarEXCELFactura']);

    //NOTAS DE CRÉDITO FACTURAS
    Route::group(['prefix' => 'ver/nota'],function(){
        Route::get('credito',['as'=>'empresa.ver.nota.credito','uses'=>'ComprobantesController@verNotaCreditoFactura']);
        Route::get('credito/buscar',['as'=>'empresa.buscar.nota.credito','uses'=>'ComprobantesController@buscarNotaCreditoFactura']);
        Route::get('credito/resultados',['as'=>'empresa.buscar.nota.credito.resultados','uses'=>'ComprobantesController@resultadosNotasCreditoFactura']);
//        Route::get('credito/listar/{dni}',['as'=>'empresa.listar.dni.nota.credito','uses'=>'ComprobantesController@listarNotaCreditoDniFactura']);
//        Route::get('credito/listar/{serie}/{numero}',['as'=>'empresa.listar.serie.numero.nota.credito','uses'=>'ComprobantesController@listarNotaCreditoSerieNumeroFactura']);

        Route::post('descargar/zip/credito/{id_nota}',['as'=>'empresa.descargar.factura.nota.credito.zip','uses'=>'ComprobantesController@descargarFacturaNotaCreditoZIP']);
        Route::post('descargar/pdf/credito/{id_nota}',['as'=>'empresa.descargar.factura.nota.credito.pdf','uses'=>'ComprobantesController@descargarFacturaNotaCreditoPDF']);
        Route::post('descargar/excel/credito/{id_nota}',['as'=>'empresa.descargar.factura.nota.credito.excel','uses'=>'ComprobantesController@descargarFacturaNotaCreditoEXCEL']);

    });

});
// ADMINISTRADOR
Route::group(['middleware' => ['web'],'prefix'=>'admin','namespace'=>'Admin'], function () {
	Route::group(['namespace'=>'Auth'],function(){
		//Login Routes...
	    Route::get('login',['as'=>'admin.login','uses'=>'LoginController@showLoginForm']);
	    Route::post('login',['as'=>'admin.login.procesar','uses'=>'LoginController@login']);
	    Route::post('logout',['as'=>'admin.logout','uses'=>'LoginController@logout']);

	    // Registration Routes...
	    Route::get('register', ['as'=>'admin.registrar','uses'=>'LoginController@showRegistrationForm']);
	    Route::post('register', ['as'=>'admin.registrar.procesar','uses'=>'LoginController@register']);

	    // Registration Routes...
	    Route::get('password/reset', ['as'=>'admin.reset.clave','uses'=>'ForgotPasswordController@showLinkRequestForm']);
	    Route::post('password/email', ['as'=>'admin.reset.clave.procesar','uses'=>'ForgotPasswordController@sendResetLinkEmail']);

	    // Registration Routes...
	    Route::get('password/reset/{token}', ['as'=>'admin.reset.cambiar','uses'=>'ResetPasswordController@showResetForm']);
	    Route::post('password/reset', ['as'=>'admin.reset.cambiar.procesar','uses'=>'ResetPasswordController@reset']);
	});
    
    //ADMIN > INDEX
	Route::get('/', ['as'=>'admin.panel','uses'=>'AdminController@index']);

	//ADMIN > CUENTA
	Route::get('cuenta/cambiar-clave',['as'=>'admin.cuenta.cambiar-clave','uses'=>'AdminController@cambiarClave']);
	Route::put('cuenta/cambiar-clave',['as'=>'admin.cuenta.procesar-cambio-clave','uses'=>'AdminController@procesarCambioClave']);
	Route::get('cuenta/cambiar-datos',['as'=>'admin.cuenta.cambiar-datos','uses'=>'AdminController@editar']);
	Route::put('cuenta/cambiar-datos',['as'=>'admin.cuenta.procesar-cambio-datos','uses'=>'AdminController@actualizar']);

	Route::group(['namespace'=>'Facturacion','prefix'=>'facturacion'],function(){
		//ADMIN > EMPRESA
		Route::get('empresas',['as'=>'admin.facturacion.empresa.listado','uses'=>'EmpresasController@listado']);
		Route::get('empresa/crear',['as'=>'admin.facturacion.empresa.crear','uses'=>'EmpresasController@crear']);
		Route::post('empresa/crear',['as'=>'admin.facturacion.empresa.almacenar','uses'=>'EmpresasController@almacenar']);
		Route::delete('empresa/{id_empresa}/eliminar',['as'=>'admin.facturacion.empresa.eliminar','uses'=>'EmpresasController@eliminar']);
		
		//ADMIN > EMPRESA > CUENTAS
		Route::get('empresa/{id_empresa}/usuarios',['as'=>'admin.facturacion.usuario.listado','uses'=>'UsuariosController@listado']);
		Route::get('empresa/{id_empresa}/usuario/crear',['as'=>'admin.facturacion.usuario.crear','uses'=>'UsuariosController@crear']);
		Route::post('empresa/{id_empresa}/usuario/crear',['as'=>'admin.facturacion.usuario.almacenar','uses'=>'UsuariosController@almacenar']);
		Route::get('empresa/{id_empresa}/usuario/{id_usuario}/editar',['as'=>'admin.facturacion.usuario.editar','uses'=>'UsuariosController@crear']);
		Route::put('empresa/{id_empresa}/usuario/{id_usuario}/editar',['as'=>'admin.facturacion.usuario.almacenar','uses'=>'UsuariosController@almacenar']);
		Route::delete('empresa/{id_empresa}/usuario/{id_usuario}/eliminar',['as'=>'admin.facturacion.usuario.eliminar','uses'=>'UsuariosController@eliminar']);
		
		//ADMIN > FACTURA
		Route::get('facturas',['as'=>'admin.facturacion.factura.listado','uses'=>'FacturasController@listado']);
		Route::get('factura/subida',['as'=>'admin.facturacion.factura.subida','uses'=>'FacturasController@subida']);//'FacturasController@subida');
		Route::post('factura/subida',['as'=>'admin.facturacion.factura.subida.procesar','uses'=>'FacturasController@almacenarSubida']);//'FacturasController@almacenarSubida');
		Route::get('factura/crear',['as'=>'admin.facturacion.factura.crear','uses'=>'FacturasController@crear']);//'FacturasController@subida');
		Route::post('factura/crear',['as'=>'admin.facturacion.factura.almacenar','uses'=>'FacturasController@almacenar']);//'FacturasController@almacenarSubida');
		Route::delete('factura/{id_factura}/eliminar',['as'=>'admin.facturacion.factura.eliminar','uses'=>'FacturasController@eliminar']);
		//ADMIN > FACTURA > DESCARGA
		Route::post('descargar/zip/factura/{id_factura}',['as'=>'admin.facturacion.descargar.factura.zip','uses'=>'FacturasController@descargarZIP']);
		Route::post('descargar/pdf/factura/{id_factura}',['as'=>'admin.facturacion.descargar.factura.pdf','uses'=>'FacturasController@descargarPDF']);
        Route::post('descargar/excel/factura/{id_factura}',['as'=>'admin.facturacion.descargar.factura.excel','uses'=>'FacturasController@descargarEXCEL']);
		//ADMIN > NOTAS DE DEBITO
		Route::get('notas_debito',['as'=>'admin.facturacion.nota_debito.listado','uses'=>'NotasDebitoController@listado']);
		Route::get('nota_debito/subida',['as'=>'admin.facturacion.nota_debito.subida','uses'=>'NotasDebitoController@subida']);//'FacturasController@subida');
		Route::post('nota_debito/subida',['as'=>'admin.facturacion.nota_debito.subida.procesar','uses'=>'NotasDebitoController@almacenarSubida']);//'FacturasController@almacenarSubida');
		Route::get('nota_debito/crear',['as'=>'admin.facturacion.nota_debito.crear','uses'=>'NotasDebitoController@crear']);//'FacturasController@subida');
		Route::post('nota_debito/crear',['as'=>'admin.facturacion.nota_debito.almacenar','uses'=>'NotasDebitoController@almacenar']);//'FacturasController@almacenarSubida');
		Route::get('notas-debito/comprobante',['as'=>'admin.facturacion.nota_debito.combo.comprobantes','uses'=>'NotasDebitoController@comboComprobantes']);
		Route::delete('nota_debito/{id_nota_debito}/eliminar',['as'=>'admin.facturacion.nota_debito.eliminar','uses'=>'NotasDebitoController@eliminar']);
		//ADMIN > FACTURA > DESCARGA
		Route::post('descargar/zip/nota-debito/{id_nota_debito}',['as'=>'admin.facturacion.descargar.nota_debito.zip','uses'=>'NotasDebitoController@descargarZIP']);
		Route::post('descargar/pdf/nota-debito/{id_nota_debito}',['as'=>'admin.facturacion.descargar.nota_debito.pdf','uses'=>'NotasDebitoController@descargarPDF']);


		//ADMIN > NOTAS DE CREDITO
		Route::get('notas-credito',['as'=>'admin.facturacion.nota_credito.listado','uses'=>'NotasCreditoController@listado']);
		Route::get('nota-credito/subida',['as'=>'admin.facturacion.nota_credito.subida','uses'=>'NotasCreditoController@subida']);//'FacturasController@subida');
		Route::post('nota-credito/subida',['as'=>'admin.facturacion.nota_credito.subida.procesar','uses'=>'NotasCreditoController@almacenarSubida']);//'FacturasController@almacenarSubida');
		Route::get('nota-credito/crear',['as'=>'admin.facturacion.nota_credito.crear','uses'=>'NotasCreditoController@crear']);//'FacturasController@subida');
		Route::post('nota-credito/crear',['as'=>'admin.facturacion.nota_credito.almacenar','uses'=>'NotasCreditoController@almacenar']);//'FacturasController@almacenarSubida');
		Route::get('notas-credito/comprobante',['as'=>'admin.facturacion.nota_credito.combo.comprobantes','uses'=>'NotasCreditoController@comboComprobantes']);
		Route::delete('nota-credito/{id_nota_credito}/eliminar',['as'=>'admin.facturacion.nota_credito.eliminar','uses'=>'NotasCreditoController@eliminar']);
		//ADMIN > NOTA DE CREDITO > DESCARGA
		Route::post('descargar/zip/nota-credito/{id_nota_credito}',['as'=>'admin.facturacion.descargar.nota_credito.zip','uses'=>'NotasCreditoController@descargarZIP']);
		Route::post('descargar/pdf/nota-credito/{id_nota_credito}',['as'=>'admin.facturacion.descargar.nota_credito.pdf','uses'=>'NotasCreditoController@descargarPDF']);
        Route::post('descargar/excel/nota-credito/{id_nota_credito}',['as'=>'admin.facturacion.descargar.nota_credito.excel','uses'=>'NotasCreditoController@descargarEXCEL']);

    });

	Route::group(['namespace'=>'Boleteo','prefix'=>'boleteo'],function(){
		//ADMIN > EMPRESA
		Route::get('personas',['as'=>'admin.boleteo.persona.listado','uses'=>'PersonasController@listado']);
		Route::get('persona/crear',['as'=>'admin.boleteo.persona.crear','uses'=>'PersonasController@crear']);
		Route::post('persona/crear',['as'=>'admin.boleteo.persona.almacenar','uses'=>'PersonasController@almacenar']);
		Route::delete('persona/{id_persona}/eliminar',['as'=>'admin.boleteo.persona.eliminar','uses'=>'PersonasController@eliminar']);
		
		//ADMIN > EMPRESA > CUENTAS
		Route::get('persona/{id_persona}/usuarios',['as'=>'admin.boleteo.usuario.listado','uses'=>'UsuariosController@listado']);
		Route::get('persona/{id_persona}/usuario/crear',['as'=>'admin.boleteo.usuario.crear','uses'=>'UsuariosController@crear']);
		Route::post('persona/{id_persona}/usuario/crear',['as'=>'admin.boleteo.usuario.almacenar','uses'=>'UsuariosController@almacenar']);
		Route::get('persona/{id_persona}/usuario/{id_usuario}/editar',['as'=>'admin.boleteo.usuario.editar','uses'=>'UsuariosController@crear']);
		Route::put('persona/{id_persona}/usuario/{id_usuario}/editar',['as'=>'admin.boleteo.usuario.almacenar','uses'=>'UsuariosController@almacenar']);
		Route::delete('persona/{id_persona}/usuario/{id_usuario}/eliminar',['as'=>'admin.boleteo.usuario.eliminar','uses'=>'UsuariosController@eliminar']);
		
		//ADMIN > FACTURA
		Route::get('boletas',['as'=>'admin.boleteo.boleta.listado','uses'=>'BoletasController@listado']);
		Route::get('boleta/subida',['as'=>'admin.boleteo.boleta.subida','uses'=>'BoletasController@subida']);//'BoletasController@subida');
		Route::post('boleta/subida',['as'=>'admin.boleteo.boleta.subida.procesar','uses'=>'BoletasController@almacenarSubida']);//'BoletasController@almacenarSubida');
		Route::get('boleta/crear',['as'=>'admin.boleteo.boleta.crear','uses'=>'BoletasController@crear']);//'BoletasController@subida');
		Route::post('boleta/crear',['as'=>'admin.boleteo.boleta.almacenar','uses'=>'BoletasController@almacenar']);//'BoletasController@almacenarSubida');
		Route::delete('boleta/{id_boleta}/eliminar',['as'=>'admin.boleteo.boleta.eliminar','uses'=>'BoletasController@eliminar']);
		Route::get('boleta/{id_boleta}/asociar',['as'=>'admin.boleteo.boleta.asociar','uses'=>'BoletasController@modalAsociar']);
		Route::post('boleta/{id_boleta}/asociar',['as'=>'admin.boleteo.boleta.asociar.procesar','uses'=>'BoletasController@procesarModalAsociar']);
		//ADMIN > FACTURA > DESCARGA
		Route::post('descargar/zip/boleta/{id_boleta}',['as'=>'admin.boleteo.descargar.boleta.zip','uses'=>'BoletasController@descargarZIP']);
		Route::post('descargar/pdf/boleta/{id_boleta}',['as'=>'admin.boleteo.descargar.boleta.pdf','uses'=>'BoletasController@descargarPDF']);
        Route::post('descargar/excel/boleta/{id_boleta}',['as'=>'admin.boleteo.descargar.boleta.excel','uses'=>'BoletasController@descargarEXCEL']);
		//ADMIN > NOTAS DE DEBITO
		Route::get('notas_debito',['as'=>'admin.boleteo.nota_debito.listado','uses'=>'NotasDebitoController@listado']);
		Route::get('nota_debito/subida',['as'=>'admin.boleteo.nota_debito.subida','uses'=>'NotasDebitoController@subida']);//'NotasDebitoController@subida');
		Route::post('nota_debito/subida',['as'=>'admin.boleteo.nota_debito.subida.procesar','uses'=>'NotasDebitoController@almacenarSubida']);//'NotasDebitoController@almacenarSubida');
		Route::get('nota_debito/crear',['as'=>'admin.boleteo.nota_debito.crear','uses'=>'NotasDebitoController@crear']);//'NotasDebitoController@subida');
		Route::post('nota_debito/crear',['as'=>'admin.boleteo.nota_debito.almacenar','uses'=>'NotasDebitoController@almacenar']);//'NotasDebitoController@almacenarSubida');
		Route::get('notas-debito/comprobante',['as'=>'admin.boleteo.nota_debito.combo.comprobantes','uses'=>'NotasDebitoController@comboComprobantes']);
		Route::delete('nota_debito/{id_nota_debito}/eliminar',['as'=>'admin.boleteo.nota_debito.eliminar','uses'=>'NotasDebitoController@eliminar']);
		//ADMIN > FACTURA > DESCARGA
		Route::post('descargar/zip/nota-debito/{id_nota_debito}',['as'=>'admin.boleteo.descargar.nota_debito.zip','uses'=>'NotasDebitoController@descargarZIP']);
		Route::post('descargar/pdf/nota-debito/{id_nota_debito}',['as'=>'admin.boleteo.descargar.nota_debito.pdf','uses'=>'NotasDebitoController@descargarPDF']);


		//ADMIN > NOTAS DE CREDITO
		Route::get('notas-credito',['as'=>'admin.boleteo.nota_credito.listado','uses'=>'NotasCreditoController@listado']);
		Route::get('nota-credito/subida',['as'=>'admin.boleteo.nota_credito.subida','uses'=>'NotasCreditoController@subida']);//'NotasCreditoController@subida');
		Route::post('nota-credito/subida',['as'=>'admin.boleteo.nota_credito.subida.procesar','uses'=>'NotasCreditoController@almacenarSubida']);//'NotasCreditoController@almacenarSubida');
		Route::get('nota-credito/crear',['as'=>'admin.boleteo.nota_credito.crear','uses'=>'NotasCreditoController@crear']);//'NotasCreditoController@subida');
		Route::post('nota-credito/crear',['as'=>'admin.boleteo.nota_credito.almacenar','uses'=>'NotasCreditoController@almacenar']);//'NotasCreditoController@almacenarSubida');
		Route::get('notas-credito/comprobante',['as'=>'admin.boleteo.nota_credito.combo.comprobantes','uses'=>'NotasCreditoController@comboComprobantes']);
		Route::delete('nota-credito/{id_nota_credito}/eliminar',['as'=>'admin.boleteo.nota_credito.eliminar','uses'=>'NotasCreditoController@eliminar']);
		//ADMIN > NOTA DE CREDITO > DESCARGA
		Route::post('descargar/zip/nota-credito/{id_nota_credito}',['as'=>'admin.boleteo.descargar.nota_credito.zip','uses'=>'NotasCreditoController@descargarZIP']);
		Route::post('descargar/pdf/nota-credito/{id_nota_credito}',['as'=>'admin.boleteo.descargar.nota_credito.pdf','uses'=>'NotasCreditoController@descargarPDF']);
        Route::post('descargar/excel/nota-credito/{id_nota_credito}',['as'=>'admin.boleteo.descargar.nota_credito.excel','uses'=>'NotasCreditoController@descargarEXCEL']);
	});
});	
