<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Modulos\Facturacion\Usuario::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'usuario' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Modulos\Facturacion\Empresa::class, function (Faker\Generator $faker) {
    return [
        'ruc' => $faker->randomNumber(7),
        'razon_social' => $faker->company,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Facturacion\Factura::class, function (Faker\Generator $faker) {
    return [
    	'id_empresa' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Facturacion\NotaCredito::class, function (Faker\Generator $faker) {
    return [
        'id_empresa' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'id_factura' => $faker->numberBetween($min = 1, $max = 2),
        'id_concepto' => $faker->numberBetween($min = 1, $max = 7),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Facturacion\NotaDebito::class, function (Faker\Generator $faker) {
    return [
        'id_empresa' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'id_factura' => $faker->numberBetween($min = 1, $max = 2),
        'id_concepto' => $faker->numberBetween($min = 101, $max = 102),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});

//BOLETEO

$factory->define(App\Modulos\Boleteo\Usuario::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'usuario' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Modulos\Boleteo\Persona::class, function (Faker\Generator $faker) {
    return [
        'dni' => $faker->randomNumber(7),
        'nombre' => $faker->firstName,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Boleteo\Boleta::class, function (Faker\Generator $faker) {
    return [
        'id_persona' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Boleteo\NotaCredito::class, function (Faker\Generator $faker) {
    return [
        'id_persona' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'id_boleta' => $faker->numberBetween($min = 1, $max = 2),
        'id_concepto' => $faker->numberBetween($min = 1, $max = 7),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});

$factory->define(App\Modulos\Boleteo\NotaDebito::class, function (Faker\Generator $faker) {
    return [
        'id_persona' => $faker->numberBetween($min = 1, $max =2),
        'serie' => $faker->ean8,
        'numero' => $faker->numberBetween($min = 1, $max = 99),
        'id_boleta' => $faker->numberBetween($min = 1, $max = 2),
        'id_concepto' => $faker->numberBetween($min = 101, $max = 102),
        'fecha_emision' => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = date_default_timezone_get()),
        'ruta_zip' => str_random(10),
        'importe' => $faker->numberBetween($min = 1, $max = 9999),
        'ruta_pdf' => str_random(10),
        'estado' => 'AC'
    ];
});