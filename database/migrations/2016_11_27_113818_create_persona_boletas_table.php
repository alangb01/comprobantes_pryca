<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaBoletasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('persona_boletas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('serie');
            $table->integer('numero');
            $table->integer('id_persona')->unsigned()->default(0);
            $table->string('nombre')->nullable();
            $table->double('importe');
            $table->date('fecha_emision');
            $table->string('ruta_zip');
            $table->string('ruta_pdf')->nullable();
            $table->string('ruta_excel')->nullable();
            $table->string('estado')->default('AC');
            $table->timestamps();

            $table->unique(array('serie', 'numero'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('persona_boletas');
    }
}
