<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmpresaNotasCredito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('empresa_notas_credito',function(Blueprint $table){
            $table->integer('id_concepto')->default(0)->change();
            $table->text('concepto')->nullable()->after('id_concepto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('empresa_notas_credito',function(Blueprint $table){
            $table->dropColumn('concepto');
        });
    }
}
