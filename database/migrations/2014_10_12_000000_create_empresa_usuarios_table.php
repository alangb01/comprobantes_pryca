<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_empresa');
            $table->string('usuario');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('forzar_cambio_clave')->default(0);
            $table->string('estado')->default('AC');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresa_usuarios');
    }
}
