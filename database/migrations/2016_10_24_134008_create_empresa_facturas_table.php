<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_facturas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('serie');
            $table->integer('numero');
            $table->integer('id_empresa')->unsigned();
            $table->double('importe');
            $table->date('fecha_emision');
            $table->string('ruta_zip');
            $table->string('ruta_pdf')->nullable();
            $table->string('ruta_excel')->nullable();
            $table->string('estado')->default('AC');
            $table->timestamps();

            $table->unique(array('serie', 'numero'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresa_facturas');
    }
}
