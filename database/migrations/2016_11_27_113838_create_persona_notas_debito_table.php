<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaNotasDebitoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('persona_notas_debito', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('serie');
            $table->integer('numero');
            $table->integer('id_persona')->unsigned();
            $table->integer('id_boleta')->unsigned();
            $table->integer('id_concepto')->unsigned();
            $table->double('importe');
            $table->date('fecha_emision');
            $table->string('ruta_zip');
            $table->string('ruta_pdf')->nullable();
            $table->string('ruta_excel')->nullable();
            $table->string('estado')->default('AC');
            $table->timestamps();

            $table->unique(array('serie', 'numero'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('persona_notas_debito');
    }
}
