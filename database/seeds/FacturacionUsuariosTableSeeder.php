<?php

use Illuminate\Database\Seeder;

class FacturacionUsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresa_usuarios')->truncate();
        App\Modulos\Facturacion\Usuario::create([
        		'id_empresa' => '1',
        		'usuario'=>'charly',
        		'email' => 'alangb01@gmail.com',
        		'password' => bcrypt('charly')

        	]);
    }
}
