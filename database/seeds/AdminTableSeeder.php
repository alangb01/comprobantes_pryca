<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('administradores')->truncate();
        App\Modulos\Admin::create([
        		'usuario'=>'admin',
        		'email' => 'creyeschaponan@gmail.com',
        		'password' => bcrypt('admin')

        	]);
    }
}
