<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdminTableSeeder::class);

        // $this->call(FacturacionEmpresasTableSeeder::class);
        // $this->call(FacturacionFacturasTableSeeder::class);
        // $this->call(FacturacionNotasCreditoTableSeeder::class);
        // $this->call(FacturacionNotasDebitoTableSeeder::class);
        // $this->call(FacturacionUsuariosTableSeeder::class);

        // $this->call(BoleteoPersonasTableSeeder::class);
        // $this->call(BoleteoBoletasTableSeeder::class);
        // $this->call(BoleteoNotasCreditoTableSeeder::class);
        // $this->call(BoleteoNotasDebitoTableSeeder::class);
        // $this->call(BoleteoUsuariosTableSeeder::class);
    }
}
