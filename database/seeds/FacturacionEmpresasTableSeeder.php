<?php

use Illuminate\Database\Seeder;

class FacturacionEmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresas')->truncate();
        factory(App\Modulos\Facturacion\Empresa::class,5)->create();
    }
}
