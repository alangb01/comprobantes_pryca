<?php

use Illuminate\Database\Seeder;

class BoleteoNotasCreditoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('persona_notas_credito')->truncate();
        factory(App\Modulos\Boleteo\NotaCredito::class,1)->create();
    }
}
