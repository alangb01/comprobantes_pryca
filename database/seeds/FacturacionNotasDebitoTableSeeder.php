<?php

use Illuminate\Database\Seeder;

class FacturacionNotasDebitoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresa_notas_debito')->truncate();
        factory(App\Modulos\Facturacion\NotaDebito::class,1)->create();
    }
}
