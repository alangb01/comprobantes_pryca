<?php

use Illuminate\Database\Seeder;

class BoleteoNotasDebitoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('persona_notas_debito')->truncate();
        factory(App\Modulos\Boleteo\NotaDebito::class,1)->create();
    }
}
