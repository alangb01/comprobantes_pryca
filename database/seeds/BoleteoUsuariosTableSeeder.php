<?php

use Illuminate\Database\Seeder;

class BoleteoUsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('persona_usuarios')->truncate();
        App\Modulos\Boleteo\Usuario::create([
        		'id_persona' => '1',
        		'usuario'=>'alan',
        		'email' => 'alangb01@gmail.com',
        		'password' => bcrypt('alan')

        	]);
    }
}
