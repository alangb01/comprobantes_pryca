<?php

use Illuminate\Database\Seeder;

class FacturacionNotasCreditoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresa_notas_credito')->truncate();
        factory(App\Modulos\Facturacion\NotaCredito::class,1)->create();
    }
}
