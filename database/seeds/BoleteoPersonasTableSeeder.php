<?php

use Illuminate\Database\Seeder;

class BoleteoPersonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('personas')->truncate();
        factory(App\Modulos\Boleteo\Persona::class,5)->create();
    }
}
