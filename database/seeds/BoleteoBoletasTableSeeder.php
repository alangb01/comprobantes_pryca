<?php

use Illuminate\Database\Seeder;

class BoleteoBoletasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('persona_boletas')->truncate();
        factory(App\Modulos\Boleteo\Boleta::class,11)->create();
    }
}
