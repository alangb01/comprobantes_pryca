<?php

use Illuminate\Database\Seeder;

class FacturacionFacturasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empresa_facturas')->truncate();
        factory(App\Modulos\Facturacion\Factura::class,11)->create();
    }
}
