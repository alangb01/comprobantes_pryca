<?php

namespace App\Http\Controllers;

use App\Modulos\Boleteo\Boleta;
use App\Modulos\Boleteo\NotaCredito as NotaCreditoBoleta;
use App\Modulos\Facturacion\Factura;
use App\Modulos\Facturacion\NotaCredito as NotaCreditoFactura;
use App\Modulos\Util;
use Illuminate\Http\Request;
use Response;

class ComprobantesController extends Controller
{
    //PERSONA - BOLETA
    public function verBoleta(){
        $url = captcha_src();
        $codigo = parse_url($url);
        $capt = $codigo['query'];
        $captcha_img = captcha_img();
        $url_regresar = route('index');
        $data = compact('captcha_img','url','capt','url_regresar');
        return view('personas.boletas.formulario',$data);
    }



    public function buscarBoleta(Request $request){
        $this->validate($request,[
            'captcha' => 'required|captcha',
            'dni'=>'required_without_all:serie,numero',
            'serie'=>'required_with:numero',
            'numero'=>'required_with:serie',
        ]);
        $data=$request->only(['dni','serie','numero','ordenar','page']);
        return redirect(route('persona.buscar.boleta.resultados',$data));

    }

    public function resultadosBoletas(Request $request){
        $dni = $request->get('dni');
        $serie = $request->get('serie');
        $numero = $request->get('numero');

        $params_get=$request->only(['dni','serie','numero','ordenar','page']);
        $params_url=[];
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','dni','razon_social'],$params_get,$params_url);
        $url_regresar = route('persona.ver.boleta');

        $listado_boletas = Boleta::buscar($dni,$serie,$numero)
            ->paginate(10)
            ->appends($params_get);

        foreach ($listado_boletas as $id => $comprobante) {
            $comprobante->url_zip=route('persona.descargar.boleta.zip',[$comprobante->id]);
            $comprobante->url_pdf=route('persona.descargar.boleta.pdf',[$comprobante->id]);
            $comprobante->url_excel=route('persona.descargar.boleta.excel',[$comprobante->id]);

            if($id==0){
                $persona = $comprobante->persona_nombre;
            }
        }

        $data = compact('listado_boletas','url_regresar','persona','campos_orden');
        return view('personas.boletas.listado',$data);
    }


    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCEL(Request $request, $id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarExcel();
        }
    }

    //PERSONA - NOTA DE CRÉDITO
    public function verNotaCreditoBoleta(){
        $url = captcha_src();
        $codigo = parse_url($url);
        $capt = $codigo['query'];
        $captcha_img = captcha_img();
        $url_regresar = route('index');
        $data = compact('captcha_img','url','capt','url_regresar');
        return view('personas.notas_credito.formulario',$data);
    }

    public function buscarNotaCreditoBoleta(Request $request){
        $this->validate($request,[
            'captcha' => 'required|captcha',
            'dni'=>'required_without_all:serie,numero',
            'serie'=>'required_with:numero',
            'numero'=>'required_with:serie',
        ]);

        $data=$request->only(['dni','serie','numero','ordenar','page']);
        return redirect()->route('persona.buscar.nota.credito.resultados',$data);
    }

    public function resultadosNotasCreditoBoleta(Request $request){
        $dni=$request->get('dni');
        $serie = $request->get('serie');
        $numero = $request->get('numero');

        $params_get=$request->only(['dni','serie','numero','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);
        $url_regresar = route('persona.ver.nota.credito');

        $listado_notas_credito  = NotaCreditoBoleta::buscar($dni,$serie,$numero)->paginate(10)->appends($params_get);
        foreach ($listado_notas_credito  as $id => $comprobante) {
            $comprobante->url_zip=route('persona.descargar.boleta.nota.credito.zip',[$comprobante->id]);
            $comprobante->url_pdf=route('persona.descargar.boleta.nota.credito.pdf',[$comprobante->id]);
            $comprobante->url_excel=route('persona.descargar.boleta.nota.credito.excel',[$comprobante->id]);


            $comprobante->url_boleta_zip = route('persona.descargar.boleta.zip',$comprobante->id_boleta);
            $comprobante->url_boleta_pdf = route('persona.descargar.boleta.pdf',$comprobante->id_boleta);
            $comprobante->url_boleta_excel = route('persona.descargar.boleta.excel',$comprobante->id_boleta);

            if($id==0){
                $persona = $comprobante->persona_nombre;
            }

        }
        //dd($listado_notas_credito);
        $data = compact('listado_notas_credito','url_regresar','persona','campos_orden');
        return view('personas.notas_credito.listado',$data);
    }

    public function descargarBoletaNotaCreditoZIP(Request $request,$id_comprobante){
        $comprobante=NotaCreditoBoleta::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito");
        }else{
            return $comprobante->descargarZip();
        }
    }

    public function descargarBoletaNotaCreditoPDF(Request $request,$id_comprobante){
        $comprobante=NotaCreditoBoleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarBoletaNotaCreditoEXCEL(Request $request, $id_comprobante){
        $comprobante=NotaCreditoBoleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de la boleta");
        }else {
            return $comprobante->descargarExcel();
        }
    }


//*=====================================================================================================================================
    //EMPRESA
    public function verFactura(Request $request){
        $url = captcha_src();
        $codigo = parse_url($url);
        $capt = $codigo['query'];
        $captcha_img = captcha_img();
        $url_regresar = route('index');
        $data = compact('captcha_img','url','capt','url_regresar');
        return view('empresas.facturas.formulario',$data);
    }

    public function buscarFactura(Request $request){
        $this->validate($request,[
            'captcha' => 'required|captcha',
            'ruc'=>'required_without_all:serie,numero',
            'serie'=>'required_with:numero',
            'numero'=>'required_with:serie',
        ]);

        $data=$request->only(['ruc','serie','numero','ordenar','page']);
        return redirect()->route('empresa.buscar.factura.resultados',$data);
    }

    public function resultadosFacturas(Request $request){
        $ruc=$request->get('ruc');
        $serie = $request->get('serie');
        $numero = $request->get('numero');

        $url_regresar = route('empresa.ver.factura');

        $params_get=$request->only(['ruc','serie','numero','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $listado_facturas = Factura::buscar($ruc,$serie,$numero)->paginate(10)->appends($params_get);
        foreach ($listado_facturas as $id => $comprobante) {
            $comprobante->url_zip=route('empresa.descargar.factura.zip',[$comprobante->id]);
            $comprobante->url_pdf=route('empresa.descargar.factura.pdf',[$comprobante->id]);
            $comprobante->url_excel=route('empresa.descargar.factura.excel',[$comprobante->id]);

            if($id==0){
                $empresa = $comprobante->empresa_nombre;
            }
        }

        $data=compact('listado_facturas','campos_orden','empresa','url_regresar');
        return view('empresas.facturas.listado',$data);
    }

    public function descargarZIPFactura(Request $request,$id_comprobante)
    {
        $comprobante = Factura::findOrFail($id_comprobante);
        $archivo = $comprobante->ruta_zip;
        if (!isset($comprobante)) {
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDFFactura(Request $request,$id_comprobante){
        $comprobante=Factura::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCELFactura(Request $request, $id_comprobante){
        $comprobante=Factura::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarExcel();
        }
    }

    //PERSONA - NOTA DE CRÉDITO
    public function verNotaCreditoFactura(){
        $url = captcha_src();
        $codigo = parse_url($url);
        $capt = $codigo['query'];
        $captcha_img = captcha_img();
        $url_regresar = route('index');
        $data = compact('captcha_img','url','capt','url_regresar');
        return view('empresas.notas_credito.formulario',$data);
    }

    public function buscarNotaCreditoFactura(Request $request){
        $this->validate($request,[
            'captcha' => 'required|captcha',
            'ruc'=>'required_without_all:serie,numero',
            'serie'=>'required_with:numero',
            'numero'=>'required_with:serie',
        ]);

        $data=$request->only(['ruc','serie','numero','ordenar','page']);
        return redirect()->route('empresa.buscar.nota.credito.resultados',$data);

    }

    public function resultadosNotasCreditoFactura(Request $request){
        $ruc=$request->get('ruc');
        $serie = $request->get('serie');
        $numero = $request->get('numero');

        $params_get=$request->only(['ruc','serie','numero','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);
        $url_regresar = route('empresa.ver.nota.credito');

        $listado_notas_credito  = NotaCreditoFactura::buscar($ruc,$serie,$numero)->paginate(10)->appends($params_get);
        foreach ($listado_notas_credito as $id => $comprobante) {
            $comprobante->url_zip=route('empresa.descargar.factura.nota.credito.zip',[$comprobante->id]);
            $comprobante->url_pdf=route('empresa.descargar.factura.nota.credito.pdf',[$comprobante->id]);
            $comprobante->url_excel=route('empresa.descargar.factura.nota.credito.excel',[$comprobante->id]);

            $comprobante->url_boleta_zip = route('empresa.descargar.factura.zip',$comprobante->id_factura);
            $comprobante->url_boleta_pdf = route('empresa.descargar.factura.pdf',$comprobante->id_factura);
            $comprobante->url_boleta_excel = route('empresa.descargar.factura.excel',$comprobante->id_factura);

            if($id==0){
                $empresa = $comprobante->persona_nombre;
            }
        }
        //dd($listado_notas_credito);
        $data = compact('listado_notas_credito','url_regresar','empresa','campos_orden');
        return view('empresas.notas_credito.listado',$data);
    }

    public function descargarFacturaNotaCreditoZIP(Request $request,$id_comprobante){
        $comprobante=\App\Modulos\Facturacion\NotaCredito::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarFacturaNotaCreditoPDF(Request $request,$id_comprobante){
        $comprobante=\App\Modulos\Facturacion\NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarFacturaNotaCreditoEXCEL(Request $request, $id_comprobante){
        $comprobante=\App\Modulos\Facturacion\NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de factura");
        }else {
            return $comprobante->descargarExcel();
        }
    }
}
