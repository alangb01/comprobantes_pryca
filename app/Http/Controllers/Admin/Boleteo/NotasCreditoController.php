<?php

namespace App\Http\Controllers\Admin\Boleteo;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modulos\Boleteo\NotaCredito;
use App\Modulos\Boleteo\Persona;
use App\Modulos\Boleteo\Boleta;
use App\Modulos\Util;

// use Validator;
use Illuminate\Support\Facades\Log;
use Storage;
use File;
use Response;
use Dompdf\Dompdf;
use App\NumeroALetras;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Chumper\Zipper\Zipper;

class NotasCreditoController extends Controller
{
    //
    public function __constdnit(){
        $this->middleware('auth:admin');
    }

    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.boleteo.nota_credito.crear');
		$url_subida=route('admin.boleteo.nota_credito.subida');


		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','dni','razon_social'],$params_get,$params_url);

        // $listado_cuentas=Cuenta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);
		$listado_notas_credito=NotaCredito::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_notas_credito as $id => $nota_credito) {
            $nota_credito->url_zip=route('admin.boleteo.descargar.nota_credito.zip',[$nota_credito->id]);
//            if ($nota_credito->ruta_pdf!="") {
                $nota_credito->url_pdf=route('admin.boleteo.descargar.nota_credito.pdf',[$nota_credito->id]);
//            }
            $nota_credito->url_excel=route('admin.boleteo.descargar.nota_credito.excel',[$nota_credito->id]);
			$nota_credito->url_eliminar=route('admin.boleteo.nota_credito.eliminar',[$nota_credito->id]);
		}

		$data=compact('listado_notas_credito','campos_orden','url_nuevo','url_subida');
		return view('admin.boletas.nota_credito.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.boleteo.nota_credito.listado');

		$nota_credito=new NotaCredito();
		$nota_credito->persona=new Persona();

        $conceptos_disponibles=NotaCredito::getConceptos();

		$personas_disponibles=Persona::cargarListado(array())->get();

		$data=compact('nota_credito', 'personas_disponibles','conceptos_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.boletas.nota_credito.formulario', $data);
    }

    public function almacenar(Request $request){
         // dd($request);
    	$this->validate($request, [
    		'serie' => 'required|unique:persona_notas_credito,serie,numero',
    		'numero' => 'required|unique:persona_notas_credito,numero,serie',
    		'id_persona' => 'required',
            'id_concepto' => 'required',
            'id_boleta' => 'required',
            'zip' => 'required|mimetypes:application/zip',
            'fecha_emision' => 'required|date',
            'importe' => 'required|numeric',
            // 'xml' => 'required|mimetypes:application/xml',
            'pdf' => 'mimetypes:application/pdf',
        ]);


        $directorio_persona="boleteo/nce/";

        $ruta_zip=null;
        if ($request->file('zip')!=null && $request->file('zip')->isValid()) {
        	$ruta_zip=$request->file('zip')->getClientOriginalName();
        	Storage::disk('local')->put($directorio_persona.$ruta_zip, file_get_contents($request->file('zip')->getRealPath()));
        }else{
        	return redirect()->with('error','No se puede subir el zip');
        }

        $ruta_pdf=null;
        if ($request->file('pdf')!=null && $request->file('pdf')->isValid()) {
        	$ruta_pdf=str_replace(".zip",".pdf", $ruta_zip);
        	Storage::disk('local')->put($directorio_persona.$ruta_pdf, file_get_contents($request->file('pdf')->getRealPath()));
        }else{
        	//return response()->make($notificacion['mensaje']);
            // return redirect()->with('error','No se puede subir el zip');
        }

        $nota_credito=new NotaCredito();
        $nota_credito->serie=$request->input('serie');
        $nota_credito->numero=$request->input('numero');
        $nota_credito->id_persona=$request->input('id_persona');
        $nota_credito->id_concepto=$request->input('id_concepto');
        $nota_credito->id_boleta=$request->input('id_boleta');
        $nota_credito->fecha_emision=$request->input('fecha_emision');
        $nota_credito->importe=$request->input('importe');
        $nota_credito->ruta_zip=$ruta_zip;
        $nota_credito->ruta_pdf=$ruta_pdf;
        $nota_credito->estado='AC';//$request->input('estado');

       // dd($nota_credito);
        if ($nota_credito->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
    }


    public function eliminar(Request $request, $id_comprobante){
    	$nota_credito=NotaCredito::findOrFail($id_comprobante);
        if ($nota_credito->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function comboComprobantes(Request $request){
        $id_persona=$request->get('id_persona');
        $persona=Persona::findOrFail($id_persona);
        $boletas_disponibles=Boleta::where('id_persona',$id_persona)
                                    ->orderBy('serie')->orderBy('numero')->get();

        $data=compact('boletas_disponibles','persona');
        return view('admin.boletas.nota_credito.combo-boletas',$data);
    }

    public function subida(Request $request){
    	$url_volver=route('admin.boleteo.nota_credito.listado');
    	$data=compact('url_volver');
    	return view('admin.boletas.nota_credito.subida',$data);
    }

    public function almacenarSubida(Request $request){
    	//obtener zip
    	//abrir zip
    	//leer xml
    	//registrar persona si no existe por dni
    	//registrar boleta
    	$comprimido=$request->file('comprimido');

    	$status=false;

        	if (isset($comprimido) && $comprimido->isValid()) {
        		$data=$this->extraerContenido($comprimido);
        		extract($data);


                if ($data_comprobante['data_emisor']['dni']!=env('RUC_EMPRESA','20524310857')) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la persona ');
                    return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
                }

                $nota_credito=NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
                if($nota_credito instanceof NotaCredito){
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El comprobante ya ha sido registrado ');
                    return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
                }

                //definir ruta base
                $directorio_persona="boleteo/nce/";
                //almacenar comprimido
                $archivo_comprimido=$comprimido->getClientOriginalName();
                $contenido_comprimido=file_get_contents($comprimido->getRealPath());
                Storage::disk('local')->put($directorio_persona.$archivo_comprimido,$contenido_comprimido);
                $data_comprobante['ruta_zip']=$archivo_comprimido;

                //almacenar pdf
    			$pdf=$request->file('pdf');
        		if (isset($pdf) && $pdf->isValid()) {
        			$archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
                    $contenido_pdf=file_get_contents($pdf->getRealPath());
        			Storage::disk('local')->put($directorio_persona.$archivo_pdf, $contenido_pdf);
                    $data_comprobante['ruta_pdf']=$archivo_pdf;
        		}


                $persona=Persona::where('dni',$data_comprobante['data_persona']['dni'])->first();

                $concepto=NotaCredito::getConcepto($data_comprobante['concepto']);
                $boleta=Boleta::buscarPorNumeracion($data_comprobante['serie_boleta'],$data_comprobante['numero_boleta'])->first();
                // dd($concepto,$boleta,$data_comprobante);

                if (!isset($concepto)) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El concepto no es reconocido, por favor registre manualmente.');
                    return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
                }

                 if (!($boleta instanceof Boleta)) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'No se ha encontrado la boleta, la boleta debe estar registrada en el sistema.');
                    return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
                }


                $nota_credito=$this->registroNotaCredito($data_comprobante,$boleta,$concepto,$persona);

                $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
    		}else{
                $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
            }

    		if ($request->ajax()){
    		    return response()->json($notificacion);
    		}else{
                //return response()->make($notificacion['mensaje']);
    			return redirect()->route('admin.boleteo.nota_credito.listado')->with('notificacion',$notificacion);
    		}
    }

//    public function descargarZIP(Request $request,$id_comprobante){
//        $boleta=NotaCredito::findOrFail($id_comprobante);
//        $archivo = $boleta->ruta_zip;
////        $archivo="boleteo/bve/".$boleta->ruta_zip;
//        if($filecontent = Storage::disk('ftp')->get($archivo)){
//            return Response::make($filecontent, '200', array(
//                'Content-Type' => 'application/octet-stream',
//                'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
//            ));
//        }else{
//            return response()->make("No se encontro el archivo zip");
//        }
//    }

//    public function descargarPDF(Request $request,$id_comprobante){
//        $nota_credito=NotaCredito::findOrFail($id_comprobante);
//
//        $archivo="/".$nota_credito->ruta_pdf;
//        $nombre =  env('RUC_EMPRESA'). "-" .$nota_credito->serie . "-" .$nota_credito->numero;
//        if(Storage::disk('local')->has($archivo)){
//            return response()->download(storage_path('app').$archivo,$nombre.".pdf");
//        }else{
//            return response()->make("No se encontro el archivo pdf");
//        }
//    }
//
//    public  function  descargarEXCEL(Request $request, $id_comprobante){
//        $boleta=NotaCredito::findOrFail($id_comprobante);
//        $archivo=$boleta->ruta_excel;
//        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
//        if(Storage::disk('local')->has($archivo)){
//            //dd(storage_path('app'));
//            return response()->download(storage_path('app')."/".$archivo,$nombre.".xls");
//        }else{
//            return response()->make("No se encontro el archivo excel");
//        }
//    }
    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito");
        }else{
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCEL(Request $request, $id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de la boleta");
        }else {
            return $comprobante->descargarExcel();
        }
    }









}
