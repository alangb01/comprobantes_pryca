<?php

namespace App\Http\Controllers\Admin\Boleteo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

use Illuminate\Support\Facades\Mail;

use App\Modulos\Boleteo\Usuario;
use App\Modulos\Boleteo\Persona;
use App\Modulos\Util;

use App\Mail\UsuarioPersonaRegistrado;
use Notification;
use App\Notifications\NuevoUsuarioDeBoletasDeVentaRegistrado;

class UsuariosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listado(Request $request,$id_persona){
    	$url_nuevo=route('admin.boleteo.usuario.crear',[$id_persona]);

        $persona=Persona::findOrFail($id_persona);

    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_persona');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['nombre'],$params_get,$params_url);

        $listado_usuarios=Usuario::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);


        foreach ($listado_usuarios as $id => $usuario) {
            $usuario->url_editar=route('admin.boleteo.usuario.listado',[$usuario->id_persona,$usuario->id]);
			$usuario->url_eliminar=route('admin.boleteo.usuario.eliminar',[$usuario->id_persona,$usuario->id]);
		}

		$data=compact('listado_usuarios','campos_orden','url_nuevo','persona');
		return view('admin.boletas.persona.usuario.listado', $data);
    }

    public function crear($id_persona){
    	$url_volver=route('admin.boleteo.usuario.listado',[$id_persona]);

    	$persona=Persona::findOrFail($id_persona);

    	$usuario=new Usuario();

    	$data=compact('usuario','persona','url_volver');
		return view('admin.boletas.persona.usuario.formulario', $data);
    }

    public function almacenar(Request $request, $id_persona){
    	$this->validate($request,[
    			'email'=>'required|email|unique:persona_usuarios',
    		]);

    	$persona=Persona::findOrFail($id_persona);

        $password=str_random(10);

    	$usuario=new Usuario();
    	$usuario->id_persona=$persona->id;
        $usuario->usuario=$request->input('email');
        $usuario->password=bcrypt($password);
    	$usuario->email=$request->input('email');


    	$usuario->save();

        $url_login=route('persona.login');
        Notification::send($usuario, new NuevoUsuarioDeBoletasDeVentaRegistrado($usuario,$password,$url_login));

    	$notificacion=array('tipo'=>'success','mensaje'=>'Registrado con exito');
    	return redirect()->route('admin.boleteo.usuario.listado',[$id_persona])->with('notificacion',$notificacion);
    }

    public function eliminar($id_persona,$id_usuario){
        $usuario=Usuario::findOrFail($id_usuario);

        if ($usuario->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
        }

        return redirect()->route('admin.boleteo.usuario.listado',[$id_persona])->with('notificacion',$notificacion);
    }
}
