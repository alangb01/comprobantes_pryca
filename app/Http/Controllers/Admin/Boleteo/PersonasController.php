<?php

namespace App\Http\Controllers\Admin\Boleteo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

// use Validator;
use App\Modulos\Boleteo\Persona;
use App\Modulos\Util;

class PersonasController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __constdnit()
    {
        $this->middleware('auth:admin');
    }



    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.boleteo.persona.crear');

		// //LISTADO
        $params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','dni','nombre','apellido'],$params_get,$params_url);

        $listado_personas=Persona::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_personas as $id => $persona) {
            $persona->url_usuarios=route('admin.boleteo.usuario.listado',[$persona->id]);
			$persona->url_eliminar=route('admin.boleteo.persona.eliminar',[$persona->id]);
		}

		$data=compact('listado_personas','campos_orden','url_nuevo');
		return view('admin.boletas.persona.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.boleteo.persona.listado');

		$persona=new Persona();

		// $estados_disponibles=Persona::getEstados();

		$data=compact('persona',  'url_volver');
		return view('admin.boletas.persona.formulario', $data);

    }

    public function almacenar(Request $request){
    	$this->validate($request, [
            'dni' => 'required|size:8|unique:personas',
            // 'apellido' => 'required',
            'nombre' => 'required',
            // 'direccion' => 'required',
        ]);

        $persona=new Persona();
        $persona->dni=$request->input('dni');
        $persona->nombre=$request->input('nombre');
        // $persona->apellido=$request->input('apellido');
        $persona->estado='AC';//$request->input('estado');
       
        if ($persona->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function eliminar(Request $request,$id_persona){
    	$persona=Persona::findOrFail($id_persona);

    	if ($persona->boletas()->count()>0) {
    		$notificacion=array('tipo'=>'warning','mensaje'=>'No se puede eliminar si tiene boletas asociadas');
    	}elseif ($persona->cuentas()->count()>0) {
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se puede eliminar si tiene usuarios asociados');
        }elseif ($persona->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return back()->with('notificacion',$notificacion);
    }
}
