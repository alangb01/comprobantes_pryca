<?php

namespace App\Http\Controllers\Admin\Boleteo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

// use Validator;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Expr\Cast\Object_;
use SebastianBergmann\Environment\Console;
use Storage;
use Response;
use File;
use Chumper\Zipper\Zipper;
use Dompdf\Dompdf;
use App\NumeroALetras;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

use App\Modulos\Boleteo\Persona;
use App\Modulos\Boleteo\Boleta;
use App\Modulos\Util;

class BoletasController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.boleteo.boleta.crear');
		$url_subida=route('admin.boleteo.boleta.subida');


		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','dni','nombre'],$params_get,$params_url);

        // $listado_cuentas=Cuenta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);
		$listado_boletas=Boleta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_boletas as $id => $boleta) {
            $boleta->url_zip=route('admin.boleteo.descargar.boleta.zip',[$boleta->id]);
//            if ($boleta->ruta_pdf!="") {
                $boleta->url_pdf=route('admin.boleteo.descargar.boleta.pdf',[$boleta->id]);
//            }
            $boleta->url_excel=route('admin.boleteo.descargar.boleta.excel',[$boleta->id]);
			$boleta->url_eliminar=route('admin.boleteo.boleta.eliminar',[$boleta->id]);
		}

		$data=compact('listado_boletas','campos_orden','url_nuevo','url_subida');
		return view('admin.boletas.boleta.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.boleteo.boleta.listado');

		$boleta=new Boleta();
		$boleta->persona=new Persona();

		$personas_disponibles=Persona::cargarListado(array())->get();

		$data=compact('boleta', 'personas_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.boletas.boleta.formulario', $data);
    }

    public function almacenar(Request $request){
    	$this->validate($request, [
    		'serie' => 'required|unique:persona_boletas,serie,numero',
    		'numero' => 'required|unique:persona_boletas,numero,serie',
    		'id_persona' => 'required',
            'zip' => 'required|mimetypes:application/zip',
            'fecha_emision' => 'required|date',
            'importe' => 'required|numeric',
            // 'xml' => 'required|mimetypes:application/xml',
            //'pdf' => 'mimetypes:application/pdf',
        ]);

        $directorio_persona="boleteo/bve/";

        $ruta_zip=null;
        if ($request->file('zip')!=null && $request->file('zip')->isValid()) {
        	$ruta_zip=$request->file('zip')->getClientOriginalName();
        	Storage::disk('local')->put($directorio_persona.$ruta_zip, file_get_contents($request->file('zip')->getRealPath()));
        }else{
        	return redirect()->with('error','No se puede subir el zip');
        }


        $boleta=new Boleta();
        $boleta->serie=$request->input('serie');
        $boleta->numero=$request->input('numero');
        $boleta->id_persona=$request->input('id_persona');
        $boleta->nombre=$request->input('nombre');
        $boleta->fecha_emision=$request->input('fecha_emision');
        $boleta->importe=$request->input('importe');
        $boleta->ruta_zip=$ruta_zip;
        $boleta->estado='AC';//$request->input('estado');
        if ($boleta->save()) {
            $comprimido=$request->file('zip');
            $data=$this->extraerContenido($comprimido);
            extract($data);

            //Generar pdf
//            $this->generarPDF($data_boleta);

            //Generar Excel
//            $this->generarExcel($data_boleta);

            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return redirect()->route('admin.boleteo.boleta.listado')->with('notificacion',$notificacion);
    }

    public function eliminar(Request $request, $id_boleta){
    	$boleta=Boleta::findOrFail($id_boleta);
        if ($boleta->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function modalAsociar(Request $request,$id_boleta){
        $boleta=Boleta::findOrFail($id_boleta);
        $boleta->action_form=route('admin.boleteo.boleta.asociar.procesar',$boleta->id);
        $personas_disponibles=Persona::cargarListado(array())->get();

        $data=compact('boleta','personas_disponibles');
        return view('admin.boletas.boleta.asociar',$data);
    }

    public function procesarModalAsociar(Request $request,$id_boleta){
        $this->validate($request,[
            'id_persona'=>'required',
        ]);
        // dd($request);
        $boleta=Boleta::findOrFail($id_boleta);
        $boleta->id_persona=$request->input('id_persona');
        
        if ($boleta->save()) {
            return "OK";
        }
    }       

    public function subida(Request $request){
    	$url_volver=route('admin.boleteo.boleta.listado');
    	$data=compact('url_volver');
    	return view('admin.boletas.boleta.subida',$data);
    }
//CAMBIAR RUC - EN .ENV Y AQUÍ!!
    public function almacenarSubida(Request $request){
    	//obtener zip
    	//abrir zip
    	//leer xml
    	//registrar persona si no existe por dni
    	//registrar boleta
       // dd($request->all());
    	$comprimido=$request->file('comprimido');


        	$status=false;

        	if (isset($comprimido) && $comprimido->isValid()) {
        		$data=$this->extraerContenido($comprimido);

                extract($data);


                if ($data_boleta['data_emisor']['ruc']!=env('RUC_EMPRESA','20524310857')) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la persona ');
                    return redirect()->route('admin.boleteo.boleta.listado')->with('notificacion',$notificacion);
                }

                $boleta=Boleta::buscarPorNumeracion($data_boleta['serie'],$data_boleta['numero'])->first();
                if($boleta instanceof Boleta){
                    $notificacion=array('tipo'=>'warning','mensaje'=>'La boleta' . $data_boleta['serie'] . 'ya ha sido registrada ');
                    return redirect()->route('admin.boleteo.boleta.listado')->with('notificacion',$notificacion);
                }

                //definir ruta base
                $directorio_base="boleteo/bve/";

                //definir ruta base pdf y excel
                $directorio_base_excel = "boleteo/excel/";

                //almacenar comprimido
                $archivo_comprimido=$comprimido->getClientOriginalName();
                $contenido_comprimido=file_get_contents($comprimido->getRealPath());
                Storage::disk('local')->put($directorio_base.$archivo_comprimido,$contenido_comprimido);
                $data_boleta['ruta_zip']=$archivo_comprimido;

                //almacenar pdf
//    			$pdf=$request->file('pdf');
//        		if (isset($pdf) && $pdf->isValid()) {
//        			$archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
//                    $contenido_pdf=file_get_contents($pdf->getRealPath());
//        			Storage::disk('local')->put($directorio_base.$archivo_pdf, $contenido_pdf);
//                    $data_boleta['ruta_pdf']=$archivo_pdf;
//        		}

                $persona=Persona::buscarPorDni($data_boleta['data_persona']['dni']);

                if($persona instanceof Persona){

                }elseif(isset($data_boleta['data_persona']['dni']) && strlen($data_boleta['data_persona']['dni'])==8){
                    $persona=$this->registroPersona($data_boleta['data_persona']);  
                }else{
                    $persona=new Persona();
                    $persona->nombre=$data_boleta['data_persona']['nombre'];
                }
                
                // dd($persona);
                $boleta=$this->registroBoleta($data_boleta,$persona);

                //Generar pdf
                $this->generarPDF($data_boleta);

                //Generar Excel
                $this->generarExcel($data_boleta);

                $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
    		}else{
                $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
            }

    		if ($request->ajax()){
    		    return response()->json($notificacion);
    		}else{
                //return response()->make($notificacion['mensaje']);
    			return redirect()->route('admin.boleteo.boleta.listado')->with('notificacion',$notificacion);
    		}
    }

    public function extraerContenido($ruta_archivo){
    	$comprimido = new Zipper();

		//Abrimos el archivo a descomprimir
		$comprimido->make($ruta_archivo->getPathName());

		$nombre_original=basename($ruta_archivo->getClientOriginalName());

		$archivo=str_replace(".zip", ".xml", $nombre_original);

		$contenido=$comprimido->getFileContent($archivo);

        //PORMIENTRAS...
		$content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-1.0.xsd"'
                                ,' ', $contenido);

		$xml=simplexml_load_string($content,null,true);

		$data_boleta=$this->extraerInfoBoleta($xml);

        $data_archivo=compact('archivo','contenido');

		return compact('data_archivo','data_boleta');
    }

    private function registroPersona($data_boleta){
        $data=$data_boleta;
	
        $persona=new Persona();
        $persona->dni=$data['dni'];
        $persona->nombre=$data['nombre'];
        $persona->estado='AC';
        if ($persona->save()) {
            return $persona;   
        }
    }

    private function registroBoleta($data_boleta, $persona){
    	$boleta=new Boleta();
        $boleta->id_persona=$persona->id;
        $boleta->serie=$data_boleta['serie'];
        $boleta->numero=$data_boleta['numero'];
        if ($persona->id>0) {
            $boleta->id_persona=$persona->id;
        }else{
            $boleta->id_persona=0;
        }
        $boleta->nombre=$persona->nombre;
        $boleta->fecha_emision=$data_boleta['fecha_emision'];
        $boleta->ruta_zip=$data_boleta['ruta_zip'];
        $boleta->importe=$data_boleta['importe'];
        if (isset($data_boleta['ruta_pdf']) && $data_boleta['ruta_pdf']!="") {
            $boleta->ruta_pdf=$data_boleta['ruta_pdf'];
        }
        $boleta->estado='AC';
        if ($boleta->save()) {
            return $boleta;
        }
    }

    private function    extraerInfoBoleta($xml){
        $ns = $xml->getNamespaces(true);
        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();

        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
        $serie=$serie_numero[0];
        $numero=$serie_numero[1];

        $label = $xml->children($ns['ext'])->UBLExtensions->children($ns['ext'])[0]
                     ->ExtensionContent->children($ns['sac'])
                     ->AdditionalInformation->children($ns['sac']);

        $gravadas = $label->AdditionalMonetaryTotal[0]->children($ns['cbc'])->PayableAmount->__toString();
        $inafectas = $label->AdditionalMonetaryTotal[1]->children($ns['cbc'])->PayableAmount->__toString();
        $exoneradas = $label->AdditionalMonetaryTotal[2]->children($ns['cbc'])->PayableAmount->__toString();


        $totales = ['gravadas' => $gravadas ,'inafectas' => $inafectas,'exoneradas' => $exoneradas];


        $data_persona=$this->extraerInfoPersona($xml,$ns);

        $data_emisor=$this->extraerInfoEmpresa($xml,$ns);

        $data_producto = $this->extraerProductos($xml,$ns);

        return compact('serie','numero','fecha_emision','importe','data_persona','data_emisor','data_producto','totales');
    }

    private function extraerInfoPersona($xml,$ns){
    	$item=$xml->children($ns['cac'])->AccountingCustomerParty;

        $dni=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();

        $nombre=$item->children($ns['cac'])->Party
                ->children($ns['cac'])->PartyLegalEntity
                ->children($ns['cbc'])->RegistrationName->__toString();
        $direccion_persona=$item->children($ns['cac'])->Party
                            ->children($ns['cac'])->PostalAddress->__toString();
    //                            ->children($ns['cbc'])->StreetName->__toString();

		$data=compact('dni','nombre','direccion_persona');
		return array_map("trim", $data);
    }

    private function extraerInfoEmpresa($xml,$ns){
        $item=$xml->children($ns['cac'])->AccountingSupplierParty;

        $ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();

   
        $razon_social=$item
            ->children($ns['cac'])->Party
            ->children($ns['cac'])->PartyLegalEntity
            ->children($ns['cbc'])->RegistrationName->__toString();

        $direccion_min=$item
        ->children($ns['cac'])->Party
        ->children($ns['cac'])->PostalAddress
        ->children($ns['cbc']);

        $direccion=$direccion_min->StreetName->__toString()
            ."".$direccion_min->District->__toString()
            ." ".$direccion_min->CityName->__toString()
            ." ".$direccion_min->CountrySubentity->__toString()."";
        $data=compact('ruc','razon_social','direccion');
       
        return array_map("trim", $data);
    }

    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCEL(Request $request, $id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la boleta");
        }else {
            return $comprobante->descargarExcel();
        }
    }

    //FUNCIONALIDADES NUEVAS



//    public  function  generarPDF($data_boleta, Boleta $comprobante){
//        //dd($data_boleta);
//        $sumatoria = 0.00;
//        $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];
//        $data = [
//            'codigo_factura' => $data_boleta['serie'] . "-" . $data_boleta['numero'] ,
//            'establecimiento' => $data_boleta['data_emisor']['direccion'],
//            'nombre_empresa' => $data_boleta['data_emisor']['razon_social'],
//            'ruc_empresa' => $data_boleta['data_emisor']['ruc'],
//            'portal_web' => 'www.pordefinir.com',
//            'numero_resolucion' => '123456789',
//            'fecha_emision' => $data_boleta['fecha_emision'],
//            'razon_social' => $data_boleta['data_persona']['nombre'],
//            'ruc' => $data_boleta['data_persona']['dni'],
//            'tipo_moneda' => 'PEN',
//            'sumatoria_descuentos' => $sumatoria,
//            'importe_total' =>  $data_boleta['importe'],
//            'total_venta_gravada' => $data_boleta['totales']['gravadas'],
//            'total_venta_exonerada' => $data_boleta['totales']['exoneradas'],
//            'total_venta_inafecta'  => $data_boleta['totales']['inafectas'],
//            'sumatoria_IGV' => $igv,
//            'nombre_documento' => 'BOLETA',
//            'numero' => $data_boleta['numero'],
//            'serie' => $data_boleta['serie'],
//            'direccion_persona' => $data_boleta['data_persona']['direccion_persona']
//        ];
//        $temp = $data_boleta['data_producto']['listado_productos'];
//
//        $directorio=$this->generarRuta("pdf",$comprobante);
//
//        if($data != null && $temp != null)
//        {
//            //Convertir de numeros a letras
//            $numero_letras = NumeroALetras::convertir(number_format(floatval($data['importe_total']),2),'','CENTIMOS');
//            //Agregar todo los datos al invoice
//            $view = \View::make('admin.pdf.invoice', compact('data','temp','numero_letras'))->render();
//            //generar y guardar pdf
//            $dompdf = \App::make('dompdf.wrapper');
//            $dompdf->loadHTML($view);
//
//            //hacer el update para poder grabar la ruta de la Boleta
//            $comprobante->ruta_pdf = $directorio .  $data['ruc_empresa'] .  "-" . $data['codigo_factura'] . ".pdf";
//            $comprobante->save();
//
//            Storage::disk('local')->put($comprobante->ruta_pdf, $dompdf->output());
//
////            return $dompdf->stream();
//
//        }else{
//            throw new \Exception("Faltan datos para generar el pdf");
//        }
//    }
//
//    public  function  generarExcel($data_boleta, Boleta $comprobante){
//        $directorio = $this->generarRuta("excel",$comprobante);
//        $productos = $data_boleta['data_producto']['listado_productos'];
//
//        $nombre_excel=$data_boleta['data_emisor']['ruc']. '-' . $data_boleta['serie'] . "-" . $data_boleta['numero'];
//        Excel::create( $nombre_excel ,function($excel) use ($productos,$data_boleta) {
//            $excel->sheet('Boleta' ,function($sheet) use ($productos, $data_boleta) {
//                $razon_social = $data_boleta['data_persona']['nombre'];
//                $ruc = $data_boleta['data_persona']['dni'];
//                $importe_total =  $data_boleta['importe'];
//                $total_venta_gravada = $data_boleta['totales']['gravadas'];
//                $total_venta_exonerada = $data_boleta['totales']['exoneradas'];
//                $total_venta_inafecta  = $data_boleta['totales']['inafectas'];
//                $total_igv = $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];
//
//                //ESTILOS
//                $sheet->cell('A1:J20', function($cells) {
//                    $cells->setAlignment('center');
//                });
//                $contador = count($productos);
//                $sheet->setBorder('A6:E'. ($contador + 6), 'thin');
//                $sheet->setBorder('D'.(8 + $contador) .':E'. ($contador + 12), 'thin');
//                //PARTE IZQUIERDA CABEZERA
//                $sheet->cell('A1', function($cell) use ($data_boleta) {
//                    $nombre_empresa = $data_boleta['data_emisor']['razon_social'];
//                    $cell->setValue($nombre_empresa);
//                });
//                $sheet->cell('A2', function($cell){
//                    $cell->setValue('DNI o RUC: ');
//                });
//                $sheet->cell('B2', function($cell) use ($ruc) {
//                    $cell->setValue($ruc);
//                });
//                $sheet->cell('A3', function($cell){
//                    $cell->setValue('Nombre o Razón Social: ');
//                });
//                $sheet->cell('B3', function($cell) use ($razon_social) {
//                    $cell->setValue($razon_social);
//                });
//                //PARTE DERECHA CABEZERA
//                $sheet->cell('E1', function($cell) {
//                    $cell->setValue('BOLETA ELECTRÓNICA');
//                });
//                $sheet->cell('E2', function($cell) {
//                    $cell->setValue(env('RUC_EMPRESA'));
//                });
//                $sheet->cell('E3', function($cell) use ($data_boleta) {
//                    $codigo_factura = $data_boleta['serie'] . "-" . $data_boleta['numero'];
//                    $cell->setValue($codigo_factura);
//                });
//                $sheet->cell('E4', function($cell) use ($data_boleta) {
//                    $fecha = $data_boleta['fecha_emision'];
//                    $cell->setValue('Fecha: '. $fecha);
//                });
//                //PARTE TABLA DE PRODUCTOS
//                $sheet->fromArray($productos, null, 'A6',true);
//                //PARTE TABLA TOTAL
//
//                $sheet->cell('D'.(8 + $contador), function($cell) {
//                    $cell->setValue('Total Gravadas:');
//                });
//                $sheet->cell('D'.(9 + $contador), function($cell) {
//                    $cell->setValue('Total Inafectas:');
//                });
//                $sheet->cell('D'.(10 + $contador), function($cell) {
//                    $cell->setValue('Total Exoneradas:');
//                });
//                $sheet->cell('D'.(11 + $contador), function($cell) {
//                    $cell->setValue('Total IGV:');
//                });
//                $sheet->cell('D'.(12 + $contador), function($cell) {
//                    $cell->setValue('Importe Total:');
//                });
//
//                $sheet->cell('E'.(8 + $contador), function($cell) use($total_venta_gravada) {
//                    $cell->setValue($total_venta_gravada);
//                });
//                $sheet->cell('E'.(9 + $contador), function($cell) use ($total_venta_inafecta) {
//                    $cell->setValue($total_venta_inafecta);
//                });
//                $sheet->cell('E'.(10 + $contador), function($cell) use ( $total_venta_exonerada) {
//                    $cell->setValue( $total_venta_exonerada);
//                });
//                $sheet->cell('E'.(11 + $contador), function($cell) use ($total_igv){
//                    $cell->setValue($total_igv);
//                });
//                $sheet->cell('E'.(12 + $contador), function($cell) use ($importe_total){
//                    $cell->setValue($importe_total);
//                });
//            });
//        })->store('xls',storage_path("app/".$directorio));
//
//        $comprobante->ruta_excel = $directorio.$nombre_excel. ".xls";
//        $comprobante->save();
//
//    }
//
//    public function generarZip($archivo, Boleta $boleta){
//
//        $directorio=$this->generarRuta("zip",$boleta);
//        $nueva_ruta=$directorio.basename($archivo);
////        Storage::move($archivo, $nueva_ruta);
////        $boleta->ruta_zip=$nueva_ruta;
////        $boleta->save();
//    }



//    public function descargarPDF(Request $request,$id_boleta){
//        $boleta=Boleta::findOrFail($id_boleta);
//        $archivo=$boleta->ruta_pdf;
//        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
//        if(Storage::disk('local')->has($archivo)){
//            //dd(storage_path('app')."/".$archivo);
//            return response()->download(storage_path('app')."/".$archivo,$nombre.".pdf");
//        }else{
//            return response()->make("No se encontro el archivo pdf");
//        }
//    }
//
//    public  function  descargarEXCEL(Request $request, $id_boleta){
//        $boleta=Boleta::findOrFail($id_boleta);
//        $archivo=$boleta->ruta_excel;
//        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
//        if(Storage::disk('local')->has($archivo)){
//            //dd(storage_path('app'));
//            return response()->download(storage_path('app')."/".$archivo,$nombre.".xls");
//        }else{
//            return response()->make("No se encontro el archivo excel");
//        }
//    }

    //FUNCIONALIDADES DEL FTP
//    public function almacenarSubidaFTP(){
//
//        set_time_limit(0);
//
//        $storage_directory = Storage::disk('ftp');
//        $directory = "./";
//        $listado_archivo = $storage_directory->allFiles($directory);
//        //dd($listado_archivo);
//        $contador = count($listado_archivo);
//        for($i=0;$i< $contador;$i++){
//            $bandera_F =strpos($listado_archivo[$i],'B');
//            $bandera_C =strpos($listado_archivo[$i],'-03-');
//            if($bandera_F == true && $bandera_C == true) {
//                $nombre = explode('-', $listado_archivo[$i]);
//                $serie = $nombre[2];
//                $numero = trim(str_replace('.zip', ' ', $nombre[3]));
////                echo $serie . ' ' . $numero;
//                $boleta = Boleta::where('serie', '=', $serie)->where('numero', '=', $numero)->first();
//                if ($boleta != null) {
////                    echo $serie . "-" . $numero . "REGISTRADO" . "</br>";
//                }else{
//                    try{
//                         $mensaje[$i] = $this->subidaFTP($listado_archivo[$i]);
//                    }catch(\Exception $e){
//                        Log::error("excepcion: ".$listado_archivo[$i]." -> ".$e->getMessage());
//                    }
//
//                }
//                //
//            }
//        }
//    }
//
//    public function subidaFTP($archivo){
//        $data=$this->extraerContenidoFTP($archivo);
//        extract($data);
//        $data_boleta['ruta_zip']=$archivo;
//        $ruc_permitido=env('RUC_EMPRESA','20524310857');
//        if ($data_boleta['data_emisor']['ruc']!=$ruc_permitido) {
//            throw new \Exception('El emisor no corresponse, asociado a '.$ruc_permitido.', valor recibido:'.$data_boleta['data_emisor']['ruc']);
//        }
//
//        $boleta=Boleta::buscarPorNumeracion($data_boleta['serie'],$data_boleta['numero'])->first();
//        if($boleta instanceof Boleta){
//            throw new \Exception('La boleta ya registrada '.$archivo);
//        }
//
//        $persona=Persona::buscarPorDni($data_boleta['data_persona']['dni']);
//        if($persona instanceof Persona){
//
//        }elseif(isset($data_boleta['data_persona']['dni']) && strlen($data_boleta['data_persona']['dni'])==8){
//            $persona=$this->registroPersona($data_boleta['data_persona']);
//        }else{
//            $persona=new Persona();
//            $persona->nombre=$data_boleta['data_persona']['nombre'];
//        }
//
//        $boleta=$this->registroBoleta($data_boleta,$persona);
//
//        //Generar pdf
//        $this->generarPDF($data_boleta,$boleta);
//
//        //Generar Excel
//        $this->generarExcel($data_boleta,$boleta);
//
//        //Generar Zip
//        $this->generarZip($archivo,$boleta);
//    }
//
//    public function extraerContenidoFTP($ruta_archivo){
//        $archivo_xml=str_replace(".zip", ".xml", basename($ruta_archivo));
//
//        //Inicializar comprimido
//        $comprimido = new Zipper();
//        $comprimido->make(storage_path( 'app/' . $ruta_archivo));
//        $contenido=$comprimido->getFileContent($archivo_xml);
//
//        //PORMIENTRAS...
//        $content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-1.0.xsd"'
//            ,' ', $contenido);
//
//        $xml=simplexml_load_string($content,null,true);
//
//        $data_boleta=$this->extraerInfoBoleta($xml);
//        $data_archivo=compact('archivo','contenido');
//        $comprimido->close();
//
//        return compact('data_archivo','data_boleta');
//    }
//
//    private function generarRuta($tipo_archivo, Boleta $boleta)
//    {
//        $fecha = $boleta->fecha_emision;
//        $anio = $fecha->year;
//        $mes = Util::mesToString($fecha->month);
//        $dia = $fecha->day;
////
//        $directorio = "boleteo/".$tipo_archivo."/".$anio."/".$mes."/".$dia."/";
//        if(!Storage::disk('local')->exists($directorio)){
//            Storage::disk('local')->makeDirectory($directorio);
//        }
//
//        return $directorio;
//    }
}


