<?php

namespace App\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

// use Validator;
use Illuminate\Support\Facades\Log;
use Storage;
use File;
use Response;
use Chumper\Zipper\Zipper;
use Dompdf\Dompdf;
use App\NumeroALetras;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

use App\Modulos\Facturacion\Empresa;
use App\Modulos\Facturacion\Factura;
use App\Modulos\Util;
use ZipArchive;

class FacturasController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.facturacion.factura.crear');
		$url_subida=route('admin.facturacion.factura.subida');


		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        // $listado_cuentas=Cuenta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);
		$listado_facturas=Factura::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_facturas as $id => $factura) {
            $factura->url_zip=route('admin.facturacion.descargar.factura.zip',[$factura->id]);
//            if ($factura->ruta_pdf!="") {
                $factura->url_pdf=route('admin.facturacion.descargar.factura.pdf',[$factura->id]);
//            }
            $factura->url_excel=route('admin.facturacion.descargar.factura.excel',[$factura->id]);
			$factura->url_eliminar=route('admin.facturacion.factura.eliminar',[$factura->id]);
		}

		$data=compact('listado_facturas','campos_orden','url_nuevo','url_subida');
		return view('admin.facturacion.factura.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.facturacion.factura.listado');

		$factura=new Factura();
		$factura->empresa=new Empresa();

		$empresas_disponibles=Empresa::cargarListado(array())->get();

		$data=compact('factura', 'empresas_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.facturacion.factura.formulario', $data);
    }

    public function almacenar(Request $request){
    	$this->validate($request, [
    		'serie' => 'required|unique:empresa_facturas,serie,numero',
    		'numero' => 'required|unique:empresa_facturas,numero,serie',
    		'id_empresa' => 'required',
            'zip' => 'required|mimetypes:application/zip',
            'fecha_emision' => 'required|date',
            'importe' => 'required|numeric',
            // 'xml' => 'required|mimetypes:application/xml',
            'pdf' => 'mimetypes:application/pdf',
        ]);

       // dd("x");

        $empresa=Empresa::findOrFail($request->input('id_empresa'))."/fe/";
        $directorio_empresa="facturacion/".$empresa->ruc;

        $ruta_zip=null;
        if ($request->file('zip')!=null && $request->file('zip')->isValid()) {
        	$ruta_zip=$request->file('zip')->getClientOriginalName();
        	Storage::disk('local')->put($directorio_empresa.$ruta_zip, file_get_contents($request->file('zip')->getRealPath()));
        }else{
        	return redirect()->with('error','No se puede subir el zip');
        }

        $factura=new Factura();
        $factura->serie=$request->input('serie');
        $factura->numero=$request->input('numero');
        $factura->id_empresa=$request->input('id_empresa');
        $factura->fecha_emision=$request->input('fecha_emision');
        $factura->importe=$request->input('importe');
        $factura->ruta_zip=$ruta_zip;
        $factura->estado='AC';//$request->input('estado');
       
        if ($factura->save()) {
            $comprimido=$request->file('zip');
            $data=$this->extraerContenido($comprimido);
            extract($data);

            //Generar pdf
            $this->generarPDF($data_boleta);

            //Generar Excel
            $this->generarExcel($data_boleta);

            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
    }


    public function eliminar(Request $request, $id_factura){
    	$factura=Factura::findOrFail($id_factura);
        if ($factura->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function cambiarEstado(Request $request, $id_factura){



        $factura=Factura::findOrFail($id_factura);
        $factura->cambiarEstado();

        if ($factura->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Cambio de estado realizado con exito');
        }else{
            $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo cambiar el estado');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function subida(Request $request){
    	$url_volver=route('admin.facturacion.factura.listado');
    	$data=compact('url_volver');
    	return view('admin.facturacion.factura.subida',$data);
    }

    public function almacenarSubida(Request $request){
    	//obtener zip
    	//abrir zip
    	//leer xml
    	//registrar empresa si no existe por ruc
    	//registrar factura
    	$comprimido=$request->file('comprimido');
        // foreach ($compri as $comprimido) {
            $status=false;

            if (isset($comprimido) && $comprimido->isValid()) {
                $data=$this->extraerContenido($comprimido);
                extract($data); 

                
                if ($data_factura['data_emisor']['ruc']!=env('RUC_EMPRESA','20524310857')) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la empresa ');
                    return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
                }

                $factura=Factura::buscarPorNumeracion($data_factura['serie'],$data_factura['numero'])->first();
                if($factura instanceof Factura){
                    $notificacion=array('tipo'=>'warning','mensaje'=>'La factura '. $data_factura['numero'] . ' ya ha sido registrada ');
                    return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
                }

                //definir ruta base
                $directorio_empresa="facturacion/".$data_factura['data_empresa']['ruc']."/fe/";
                //almacenar comprimido
                $archivo_comprimido=$comprimido->getClientOriginalName();
                $contenido_comprimido=file_get_contents($comprimido->getRealPath());
                Storage::disk('local')->put($directorio_empresa.$archivo_comprimido,$contenido_comprimido);
                $data_factura['ruta_zip']=$archivo_comprimido;

                //almacenar pdf
//                $pdf=$request->file('pdf');
//                if (isset($pdf) && $pdf->isValid()) {
//                    $archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
//                    $contenido_pdf=file_get_contents($pdf->getRealPath());
//                    Storage::disk('local')->put($directorio_empresa.$archivo_pdf, $contenido_pdf);
//                    $data_factura['ruta_pdf']=$archivo_pdf;
//                }

                $empresa=Empresa::buscarPorRuc($data_factura['data_empresa']['ruc']);

                $esInstancia=$empresa instanceof Empresa;
                if(!$esInstancia){
                  $empresa=$this->registroEmpresa($data_factura);  
                }
                
                $factura=$this->registroFactura($data_factura,$empresa);

                //Generar pdf
                $this->generarPDF($data_factura);

                //Generar Excel
                $this->generarExcel($data_factura);


                $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
            }else{
                $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
            }


            if ($request->ajax()){
                return response()->json($notificacion);
            }else{
                //return response()->make($notificacion['mensaje']);
                return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
            }

    }

//    public function extraerContenido($ruta_archivo){
//        $archivo_xml=str_replace(".zip", ".xml", $ruta_archivo);
//
//    	$comprimido = new Zipper();
//		$comprimido->make($ruta_archivo);
//		$contenido=$comprimido->getFileContent(basename($archivo_xml));
//
//        //PORMIENTRAS...
//        $content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-1.0.xsd"'
//            ,' ', $contenido);
//
//		$xml=simplexml_load_string($content,null,true);
//
//		$data_factura=$this->extraerInfoFactura($xml);
//
//        $data_archivo=compact('archivo','contenido');
//
//		return compact('data_archivo','data_factura');
//    }

//    private function registroEmpresa($data_factura){
//        $data=$data_factura['data_empresa'];
//
//        $empresa=new Empresa();
//        $empresa->ruc=$data['ruc'];
//        $empresa->razon_social=$data['razon_social'];
//        $empresa->direccion=$data['direccion'];
//        $empresa->estado='AC';
//        if ($empresa->save()) {
//            return $empresa;
//        }
//    }

//    private function registroFactura($data_factura, $empresa){
//
//    	$factura=new Factura();
//        $factura->id_empresa=$empresa->id;
//        $factura->serie=$data_factura['serie'];
//        $factura->numero=$data_factura['numero'];
//        $factura->fecha_emision=$data_factura['fecha_emision'];
//        $factura->ruta_zip=$data_factura['ruta_zip'];
//        $factura->importe=$data_factura['importe'];
//        if (isset($data_factura['ruta_pdf']) && $data_factura['ruta_pdf']!="") {
//            $factura->ruta_pdf=$data_factura['ruta_pdf'];
//        }
//        $factura->estado='AC';
//        $factura->save();
//        return $factura;
//    }

//    private function extraerInfoFactura($xml){
//        $ns = $xml->getNamespaces(true);
//        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
//        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();
//
//        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
//        $serie=$serie_numero[0];
//        $numero=$serie_numero[1];
//
//        $label = $xml->children($ns['ext'])->UBLExtensions->children($ns['ext'])[0]
//                ->ExtensionContent->children($ns['sac'])
//                ->AdditionalInformation->children($ns['sac']);
//
//        $gravadas = $label->AdditionalMonetaryTotal[0]->children($ns['cbc'])->PayableAmount->__toString();
//        $inafectas = $label->AdditionalMonetaryTotal[1]->children($ns['cbc'])->PayableAmount->__toString();
//        $exoneradas = $label->AdditionalMonetaryTotal[2]->children($ns['cbc'])->PayableAmount->__toString();
//
//        $totales = ['gravadas' => $gravadas ,'inafectas' => $inafectas,'exoneradas' => $exoneradas];
//
//        $data_empresa=$this->extraerInfoEmpresa($xml,$ns,'AccountingCustomerParty');
//
//        $data_emisor=$this->extraerInfoEmpresa($xml,$ns,'AccountingSupplierParty');
//
//        $data_producto = $this->extraerProductos($xml,$ns);
//
//        return compact('serie','numero','fecha_emision','importe','data_empresa','data_emisor','data_producto','totales');
//    }

//    private function extraerInfoEmpresa($xml,$ns,$seleccion){
//    	$item=$xml->children($ns['cac'])->$seleccion;
//
//        $direccion_min=$item
//            ->children($ns['cac'])->Party
//            ->children($ns['cac'])->PostalAddress
//            ->children($ns['cbc']);
//
//    	$ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
//		$razon_social=$item
//			->children($ns['cac'])->Party
//			->children($ns['cac'])->PartyLegalEntity
//			->children($ns['cbc'])->RegistrationName->__toString();
//
//		$direccion=$direccion_min->StreetName->__toString()
//			." ".$direccion_min->District->__toString()
//			." ".$direccion_min->CityName->__toString()
//			." ".$direccion_min->CountrySubentity->__toString()." ";
//
//		$data=compact('ruc','razon_social','direccion');
//		return array_map("trim", $data);
//    }

//    public function descargarZIP(Request $request,$id_factura){
//        $factura=Factura::findOrFail($id_factura);
//        //dd($factura);
//        $archivo = $factura->ruta_zip;
//        //$archivo='facturacion/'.$factura->empresa->ruc."/fe/".$factura->ruta_zip;
//        if($filecontent = Storage::disk('ftp')->get($archivo)){
//            return Response::make($filecontent, '200', array(
//                'Content-Type' => 'application/octet-stream',
//                'Content-Disposition' => 'attachment; filename="'.$archivo.'"'
//            ));
//        }else{
//            return response()->make("No se encontro el archivo zip");
//        }
//    }

    //NUEVAS FUNCIONALIDADES
//    public function extraerProductos($xml,$ns){
//        $contador = count($xml->children($ns['cac'])->InvoiceLine);
//        //dd($contador);
//        $listado_productos = array();
//        for ($i = 0; $i < $contador; $i++){
//            $label = $xml->children($ns['cac'])->InvoiceLine[$i];
//            //dd($label);
//            $DescripcionProducto=$label->children($ns['cac'])->Item->children($ns['cbc'])->Description->__toString();
//
//            $CantidadUnidad=$label->children($ns['cbc'])->InvoicedQuantity->__toString();
//
//            $TipoCantidad = $label->children($ns['cbc'])->InvoicedQuantity->attributes()->unitCode->__toString();
//
//            $PrecioVentaUnitario = $label->children($ns['cac'])->Price->children($ns['cbc'])->PriceAmount->__toString();
//
////            $PrecioVentaTotal=$label->children($ns['cac'])->PricingReference
////                ->children($ns['cac'])->AlternativeConditionPrice->children($ns['cbc'])
////                ->PriceAmount->__toString();
//            $listado_productos[$i] = [ 'descripcion_producto' => trim($DescripcionProducto),
//                'cantidadUnidad' => trim($CantidadUnidad),
//                'tipoCantidad' => trim($TipoCantidad),
//                'PrecioVentaUnitario' => trim($PrecioVentaUnitario) ];
//                //'PrecioVentaTotal' => trim($PrecioVentaTotal)];
//        }
//        $data=compact('listado_productos');
//       // dd($data);
//        return $data;
//    }

//    public  function  generarPDF($data_boleta, Factura $comprobante){
//        //dd($data_boleta);
//        $sumatoria = 0.00;
//        $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];
//        $data = [
//            'codigo_factura' => $data_boleta['serie'] . "-" . $data_boleta['numero'] ,
//            'establecimiento' => $data_boleta['data_emisor']['direccion'],
//            'nombre_empresa' => $data_boleta['data_emisor']['razon_social'],
//            'ruc_empresa' => $data_boleta['data_emisor']['ruc'],
//            'portal_web' => 'www.pordefinir.com',
//            'numero_resolucion' => '123456789',
//            'fecha_emision' => $data_boleta['fecha_emision'],
//            'razon_social' => $data_boleta['data_empresa']['razon_social'],
//            'ruc' => $data_boleta['data_empresa']['ruc'],
//            'direccion_persona' => $data_boleta['data_empresa']['direccion'],
//            'tipo_moneda' => 'PEN',
//            'sumatoria_descuentos' => $sumatoria,
//            'importe_total' =>  $data_boleta['importe'],
//            'total_venta_gravada' => $data_boleta['totales']['gravadas'],
//            'total_venta_exonerada' => $data_boleta['totales']['exoneradas'],
//            'total_venta_inafecta'  => $data_boleta['totales']['inafectas'],
//            'sumatoria_IGV' => $igv,
//            'nombre_documento' => 'FACTURA',
//            'numero' => $data_boleta['numero'],
//            'serie' => $data_boleta['serie']
//        ];
//        $temp = $data_boleta['data_producto']['listado_productos'];
//
//        //dd($temp[0]['descripcion_producto']);
//
//        $directorio=$this->generarRuta("pdf",$comprobante);
//
//        if($data != null && $temp != null)
//        {
//            //Convertir de numeros a letras
//            $numero_letras = NumeroALetras::convertir(number_format(floatval($data['importe_total']),2),'','CENTIMOS');
//            //Agregar todo los datos al invoice
//            $view = \View::make('admin.pdf.invoice', compact('data','temp','numero_letras'))->render();
//            $dompdf = \App::make('dompdf.wrapper');
//            $dompdf->loadHTML($view);
//
//            $comprobante->ruta_pdf = $directorio . $data['ruc_empresa'] .  "-" . $data['codigo_factura'] . ".pdf";
//            $comprobante->save();
//
//            Storage::disk('local')->put($comprobante->ruta_pdf, $dompdf->output());
//
//        }else{
//            throw new \Exception("No se pudo generar el pdf");
//        }
//    }

//    private function generarRuta($tipo_archivo, Factura $factura)
//    {
//        $fecha = $factura->fecha_emision;
//        $anio = $fecha->year;
//        $mes = Util::mesToString($fecha->month);
//        $dia = $fecha->day;
////
//        $directorio = "facturacion/".$tipo_archivo."/".$anio."/".$mes."/".$dia."/";
//        if(!Storage::disk('local')->exists($directorio)){
//            Storage::disk('local')->makeDirectory($directorio);
//        }
//
//        return $directorio;
//    }
//
//    public  function  generarExcel($data_boleta, Factura $comprobante){
//        $productos = $data_boleta['data_producto']['listado_productos'];
//
//        $directorio=$this->generarRuta("excel",$comprobante);
//        $nombre_excel=$data_boleta['data_empresa']['ruc']. '-' . $data_boleta['serie'] . "-" . $data_boleta['numero'];
//
//
//        Excel::create($nombre_excel ,function($excel) use ($productos,$data_boleta) {
//            $excel->sheet('Boleta' ,function($sheet) use ($productos, $data_boleta) {
//                $razon_social = $data_boleta['data_empresa']['razon_social'];
//                $ruc = $data_boleta['data_empresa']['ruc'];
//                $importe_total =  $data_boleta['importe'];
//                $total_venta_gravada = $data_boleta['totales']['gravadas'];
//                $total_venta_exonerada = $data_boleta['totales']['exoneradas'];
//                $total_venta_inafecta  = $data_boleta['totales']['inafectas'];
//                $total_igv = $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];
//
//                //ESTILOS
//                $sheet->cell('A1:J20', function($cells) {
//                    $cells->setAlignment('center');
//                });
//                $contador = count($productos);
//                $sheet->setBorder('A6:E'. ($contador + 6), 'thin');
//                $sheet->setBorder('D'.(8 + $contador) .':E'. ($contador + 12), 'thin');
//                //PARTE IZQUIERDA CABEZERA
//                $sheet->cell('A1', function($cell) use ($data_boleta) {
//                    $nombre_empresa = $data_boleta['data_emisor']['razon_social'];
//                    $cell->setValue($nombre_empresa);
//                });
//                $sheet->cell('A2', function($cell){
//                    $cell->setValue('DNI o RUC: ');
//                });
//                $sheet->cell('B2', function($cell) use ($ruc) {
//                    $cell->setValue($ruc);
//                });
//                $sheet->cell('A3', function($cell){
//                    $cell->setValue('Nombre o Razón Social: ');
//                });
//                $sheet->cell('B3', function($cell) use ($razon_social) {
//                    $cell->setValue($razon_social);
//                });
//                //PARTE DERECHA CABEZERA
//                $sheet->cell('E1', function($cell) {
//                    $cell->setValue('FACTURA ELECTRÓNICA');
//                });
//                $sheet->cell('E2', function($cell) {
//                    $cell->setValue(env('RUC_EMPRESA'));
//                });
//                $sheet->cell('E3', function($cell) use ($data_boleta) {
//                    $codigo_factura = $data_boleta['serie'] . "-" . $data_boleta['numero'];
//                    $cell->setValue($codigo_factura);
//                });
//                $sheet->cell('E4', function($cell) use ($data_boleta) {
//                    $fecha = $data_boleta['fecha_emision'];
//                    $cell->setValue('Fecha: '. $fecha);
//                });
//                //PARTE TABLA DE PRODUCTOS
//                $sheet->fromArray($productos, null, 'A6',true);
//                //PARTE TABLA TOTAL
//
//                $sheet->cell('D'.(8 + $contador), function($cell) {
//                    $cell->setValue('Total Gravadas:');
//                });
//                $sheet->cell('D'.(9 + $contador), function($cell) {
//                    $cell->setValue('Total Inafectas:');
//                });
//                $sheet->cell('D'.(10 + $contador), function($cell) {
//                    $cell->setValue('Total Exoneradas:');
//                });
//                $sheet->cell('D'.(11 + $contador), function($cell) {
//                    $cell->setValue('Total IGV:');
//                });
//                $sheet->cell('D'.(12 + $contador), function($cell) {
//                    $cell->setValue('Importe Total:');
//                });
//
//                $sheet->cell('E'.(8 + $contador), function($cell) use($total_venta_gravada) {
//                    $cell->setValue($total_venta_gravada);
//                });
//                $sheet->cell('E'.(9 + $contador), function($cell) use ($total_venta_inafecta) {
//                    $cell->setValue($total_venta_inafecta);
//                });
//                $sheet->cell('E'.(10 + $contador), function($cell) use ( $total_venta_exonerada) {
//                    $cell->setValue( $total_venta_exonerada);
//                });
//                $sheet->cell('E'.(11 + $contador), function($cell) use ($total_igv){
//                    $cell->setValue($total_igv);
//                });
//                $sheet->cell('E'.(12 + $contador), function($cell) use ($importe_total){
//                    $cell->setValue($importe_total);
//                });
//            });
//        })->store('xls',storage_path("app/".$directorio));
//
//        $comprobante->ruta_excel = $directorio.$nombre_excel.".xls";
//        $comprobante->save();
//    }

//    public function descargarPDF(Request $request,$id_boleta){
//        $boleta=Factura::findOrFail($id_boleta);
//        $archivo=$boleta->ruta_pdf;
//        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
//        if(Storage::disk('local')->has($archivo)){
//            //dd(storage_path('app')."/".$archivo);
//            return response()->download(storage_path('app')."/".$archivo,$nombre.".pdf");
//        }else{
//            return response()->make("No se encontro el archivo pdf");
//        }
//    }
//
//    public  function  descargarEXCEL(Request $request, $id_boleta){
//        $boleta=Factura::findOrFail($id_boleta);
//        $archivo=$boleta->ruta_excel;
//        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
//        if(Storage::disk('local')->has($archivo)){
//            //dd(storage_path('app'));
//            return response()->download(storage_path('app')."/".$archivo,$nombre.".xls");
//        }else{
//            return response()->make("No se encontro el archivo excel");
//        }
//    }

    public function descargarZIP(Request $request,$id_comprobante)
    {
        $comprobante = Factura::findOrFail($id_comprobante);
        $archivo = $comprobante->ruta_zip;
        if (!isset($comprobante)) {
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $comprobante=Factura::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCEL(Request $request, $id_comprobante){
        $comprobante=Factura::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la factura");
        }else {
            return $comprobante->descargarExcel();
        }
    }


    public function almacenarZIP(Request $request){
        $status=false;
//        dd($request->all());
        $comprimido=$request->file('comprimido');
        // foreach ($compri as $comprimido) {
        $status=false;

        if (isset($comprimido) && $comprimido->isValid()) {
            $data=$this->extraerContenido($comprimido);
            extract($data);

            if ($data_factura['data_emisor']['ruc']!=env('RUC_EMPRESA','20524310857')) {
                $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la empresa ');
                return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
            }

            $factura=Factura::buscarPorNumeracion($data_factura['serie'],$data_factura['numero'])->first();
            if($factura instanceof Factura){
                $notificacion=array('tipo'=>'warning','mensaje'=>'La factura '. $data_factura['numero'] . ' ya ha sido registrada ');
                return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
            }

            //definir ruta base
            $directorio_empresa="facturacion/".$data_factura['data_empresa']['ruc']."/fe/";
            //almacenar comprimido
            $archivo_comprimido=$comprimido->getClientOriginalName();
            $contenido_comprimido=file_get_contents($comprimido->getRealPath());
            Storage::disk('local')->put($directorio_empresa.$archivo_comprimido,$contenido_comprimido);
            $data_factura['ruta_zip']=$archivo_comprimido;

            //almacenar pdf
//                $pdf=$request->file('pdf');
//                if (isset($pdf) && $pdf->isValid()) {
//                    $archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
//                    $contenido_pdf=file_get_contents($pdf->getRealPath());
//                    Storage::disk('local')->put($directorio_empresa.$archivo_pdf, $contenido_pdf);
//                    $data_factura['ruta_pdf']=$archivo_pdf;
//                }

            $empresa=Empresa::buscarPorRuc($data_factura['data_empresa']['ruc']);

            $esInstancia=$empresa instanceof Empresa;
            if(!$esInstancia){
                $empresa=$this->registroEmpresa($data_factura);
            }

            $factura=$this->registroFactura($data_factura,$empresa);

            //Generar pdf
            $this->generarPDF($data_factura);

            //Generar Excel
            $this->generarExcel($data_factura);


            $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
        }else{
            $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }


        if ($request->ajax()){
            return response()->json($notificacion);
        }else{
            //return response()->make($notificacion['mensaje']);
            return redirect()->route('admin.facturacion.factura.listado')->with('notificacion',$notificacion);
        }

    }


    //FUNCIONALIDADES DEL FTP
    public function almacenarSubidaFTP(){
        set_time_limit(0);
        //dd("HOLA");
        $storage_directory = Storage::disk('ftp');
        $directory = "./";
        $listado_archivo = $storage_directory->allFiles($directory);

        $contador = count($listado_archivo);
        for($i=0;$i< $contador;$i++){
            $bandera_F =strpos($listado_archivo[$i],'F');
            $bandera_C = strpos($listado_archivo[$i],'-01-');
            if($bandera_F == true && $bandera_C == true){
                $nombre = explode('-',$listado_archivo[$i]);
                $serie = $nombre[2];
                $numero = trim(str_replace('.zip',' ',$nombre[3]));
                $factura = Factura::where('serie','=',$serie)->where('numero','=',$numero)->first();
                if($factura != null){
                    echo $serie . "-" . $numero . "REGISTRADO" . "</br>";
                    //Log::info($serie . "-" . $numero . "REGISTRADO");
                }else{
                    try{
                        $mensaje[$i] = $this->subidaFTP($listado_archivo[$i]);
                    }catch(\Exception $e){
                        Log::error("excepcion: ".$listado_archivo[$i]." -> ".$e->getMessage());
                    }
                }
            }
        }
    }

    public function subidaFTP($archivo){
        $data=$this->extraerContenidoFTP($archivo);
        extract($data);
        $data_factura['ruta_zip']=$archivo;
        $ruc_permitido=env('RUC_EMPRESA','20524310857');
        if ($data_factura['data_emisor']['ruc']!=$ruc_permitido) {
            throw new \Exception('El emisor no corresponse, asociado a '.$ruc_permitido.', valor recibido:'.$data_factura['data_emisor']['ruc']);
        }

        $factura=Factura::buscarPorNumeracion($data_factura['serie'],$data_factura['numero'])->first();
        if($factura instanceof Factura){
            throw new \Exception('La factura ya registrada '.$archivo);
        }

        $empresa=Empresa::buscarPorRuc($data_factura['data_empresa']['ruc']);
        if($empresa instanceof Empresa){

        }else{
            $empresa=$this->registroEmpresa($data_factura);
        }

        $factura=$this->registroFactura($data_factura,$empresa);

        //Generar pdf
        $this->generarPDF($data_factura,$factura);

        //Generar Excel
        $this->generarExcel($data_factura,$factura);

        //Generar Zip
        $this->generarZIP($archivo,$factura);
    }

//    public function generarZip($archivo, Factura $comprobante){
//        $directorio=$this->generarRuta("zip",$comprobante);
//        $nueva_ruta=$directorio.basename($archivo);
//        Storage::disk('local')->move($archivo, $nueva_ruta);
//        $comprobante->ruta_zip=$nueva_ruta;
//        $comprobante->save();
//    }
//
//    public function extraerContenidoFTP($ruta_archivo){
//        $archivo_xml=str_replace(".zip", ".xml", basename($ruta_archivo));
//
//        $comprimido = new Zipper();
//        $comprimido->make(storage_path( 'app/' . $ruta_archivo));
//        $contenido=$comprimido->getFileContent($archivo_xml);
//
//        $content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-1.0.xsd"'
//            ,' ', $contenido);
//
//        $xml=simplexml_load_string($content,null,true);
//
//        $data_factura=$this->extraerInfoFactura($xml);
//
//        $data_archivo=compact('archivo','contenido');
//
//        $comprimido->close();
//
//        return compact('data_archivo','data_factura');
//    }
}
