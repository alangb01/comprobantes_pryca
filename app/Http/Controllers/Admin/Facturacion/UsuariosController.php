<?php

namespace App\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

use Illuminate\Support\Facades\Mail;

use App\Modulos\Facturacion\Usuario;
use App\Modulos\Facturacion\Empresa;
use App\Modulos\Util;

use Notification;
use App\Notifications\NuevoUsuarioDeFacturasRegistrado;

class UsuariosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listado(Request $request,$id_empresa){
    	$url_nuevo=route('admin.facturacion.usuario.crear',[$id_empresa]);

        $empresa=Empresa::findOrFail($id_empresa);

    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_empresa');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['nombre'],$params_get,$params_url);

        $listado_usuarios=Usuario::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);


        foreach ($listado_usuarios as $id => $usuario) {
            $usuario->url_editar=route('admin.facturacion.usuario.listado',[$usuario->id_empresa,$usuario->id]);
			$usuario->url_eliminar=route('admin.facturacion.usuario.eliminar',[$usuario->id_empresa,$usuario->id]);
		}

		$data=compact('listado_usuarios','campos_orden','url_nuevo','empresa');
		return view('admin.facturacion.empresa.usuario.listado', $data);
    }

    public function crear($id_empresa){
    	$url_volver=route('admin.facturacion.usuario.listado',[$id_empresa]);

    	$empresa=Empresa::findOrFail($id_empresa);

    	$usuario=new Usuario();

    	$data=compact('usuario','empresa','url_volver');
		return view('admin.facturacion.empresa.usuario.formulario', $data);
    }

    public function almacenar(Request $request, $id_empresa){
    	$this->validate($request,[
    			'email'=>'required|email|unique:empresa_usuarios',
    		]);

    	$empresa=Empresa::findOrFail($id_empresa);

        $password=str_random(10);

    	$usuario=new Usuario();
    	$usuario->id_empresa=$empresa->id;
        $usuario->usuario=$request->input('email');
        $usuario->password=bcrypt($password);
    	$usuario->email=$request->input('email');


    	$usuario->save();

        $url_login=route('empresa.login');
        Notification::send($usuario, new NuevoUsuarioDeFacturasRegistrado($usuario,$password,$url_login));

        // Mail::to($usuario->email)->send(new UsuarioEmpresaRegistrado($usuario,$password));

    	$notificacion=array('tipo'=>'success','mensaje'=>'Registrado con exito');
    	return redirect()->route('admin.facturacion.usuario.listado',[$id_empresa])->with('notificacion',$notificacion);
    }

    public function eliminar($id_empresa,$id_usuario){
        $usuario=Usuario::findOrFail($id_usuario);

        if ($usuario->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
        }

        return redirect()->route('admin.facturacion.usuario.listado',[$id_empresa])->with('notificacion',$notificacion);
    }
}
