<?php

namespace App\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modulos\Facturacion\NotaDebito;
use App\Modulos\Facturacion\Empresa;
use App\Modulos\Facturacion\Factura;
use App\Modulos\Util;

// use Validator;
use Storage;
use File;
use Chumper\Zipper\Zipper;

class NotasDebitoController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth:admin');
    }



    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.facturacion.nota_debito.crear');
		$url_subida=route('admin.facturacion.nota_debito.subida');


		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        // $listado_cuentas=Cuenta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);
		$listado_notas_debito=NotaDebito::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_notas_debito as $id => $nota_debito) {
            $nota_debito->url_zip=route('admin.facturacion.descargar.nota_debito.zip',[$nota_debito->id]);
            if ($nota_debito->ruta_pdf!="") {
                $nota_debito->url_pdf=route('admin.facturacion.descargar.nota_debito.pdf',[$nota_debito->id]);
            }
			$nota_debito->url_eliminar=route('admin.facturacion.nota_debito.eliminar',[$nota_debito->id]);
		}

		$data=compact('listado_notas_debito','campos_orden','url_nuevo','url_subida');
		return view('admin.facturacion.nota_debito.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.facturacion.nota_debito.listado');

		$nota_debito=new NotaDebito();
		$nota_debito->empresa=new Empresa();

        $conceptos_disponibles=NotaDebito::getConceptos();

		$empresas_disponibles=Empresa::cargarListado(array())->get();

		$data=compact('nota_debito', 'empresas_disponibles','conceptos_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.facturacion.nota_debito.formulario', $data);
    }

    public function almacenar(Request $request){
    	$this->validate($request, [
    		'serie' => 'required|unique:empresa_notas_debito,serie,numero',
    		'numero' => 'required|unique:empresa_notas_debito,numero,serie',
    		'id_empresa' => 'required',
            'id_concepto' => 'required',
            'id_factura' => 'required',
            'zip' => 'required|mimetypes:application/zip',
            'fecha_emision' => 'required|date',
            'importe' => 'required|numeric',
            // 'xml' => 'required|mimetypes:application/xml',
            'pdf' => 'mimetypes:application/pdf',
        ]);

       // dd("x");

        $empresa=Empresa::findOrFail($request->input('id_empresa'));
        $directorio_empresa="facturacion/".$empresa->ruc."/nce/";

        $ruta_zip=null;
        if ($request->file('zip')!=null && $request->file('zip')->isValid()) {
        	$ruta_zip=$request->file('zip')->getClientOriginalName();
        	Storage::disk('local')->put($directorio_empresa.$ruta_zip, file_get_contents($request->file('zip')->getRealPath()));
        }else{
        	return redirect()->with('error','No se puede subir el zip');
        }

        $ruta_pdf=null;
        if ($request->file('pdf')!=null && $request->file('pdf')->isValid()) {
        	$ruta_pdf=str_replace(".zip",".pdf", $ruta_zip);
        	Storage::disk('local')->put($directorio_empresa.$ruta_pdf, file_get_contents($request->file('pdf')->getRealPath()));
        }else{
        	//return response()->make($notificacion['mensaje']);
            // return redirect()->with('error','No se puede subir el zip');
        }

        $nota_debito=new NotaDebito();
        $nota_debito->serie=$request->input('serie');
        $nota_debito->numero=$request->input('numero');
        $nota_debito->id_empresa=$request->input('id_empresa');
        $nota_debito->id_concepto=$request->input('id_concepto');
        $nota_debito->id_factura=$request->input('id_factura');
        $nota_debito->fecha_emision=$request->input('fecha_emision');
        $nota_debito->importe=$request->input('importe');
        $nota_debito->ruta_zip=$ruta_zip;
        $nota_debito->ruta_pdf=$ruta_pdf;
        $nota_debito->estado='AC';//$request->input('estado');
       
       // dd($nota_debito);
        if ($nota_debito->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
    }


    public function eliminar(Request $request, $id_comprobante){
    	$nota_debito=NotaDebito::findOrFail($id_comprobante);
        if ($nota_debito->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function comboComprobantes(Request $request){
        $id_empresa=$request->get('id_empresa');
        $empresa=Empresa::findOrFail($id_empresa);
        $facturas_disponibles=Factura::where('id_empresa',$id_empresa)
                                    ->orderBy('serie')->orderBy('numero')->get();

        $data=compact('facturas_disponibles','empresa');
        return view('admin.facturacion.nota_credito.combo-facturas',$data);
    }

    public function subida(Request $request){
    	$url_volver=route('admin.facturacion.nota_debito.listado');
    	$data=compact('url_volver');
    	return view('admin.facturacion.nota_debito.subida',$data);
    }

    public function almacenarSubida(Request $request){
    	//obtener zip
    	//abrir zip
    	//leer xml
    	//registrar empresa si no existe por ruc
    	//registrar factura
    	 $comprimido=$request->file('comprimido');
    	$status=false;

        	if (isset($comprimido) && $comprimido->isValid()) {
        		$data=$this->extraerContenido($comprimido);
        		extract($data);	

                
                if ($data_comprobante['data_emisor']['ruc']!=env('RUC_EMPRESA','20561400939')) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la empresa ');
                    return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
                }

                $nota_debito=NotaDebito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
                if($nota_debito instanceof NotaDebito){
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El comprobante ya ha sido registrado ');
                    return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
                }

                //definir ruta base
                $directorio_empresa="facturacion/".$data_comprobante['data_empresa']['ruc']."/nce/";
                //almacenar comprimido
                $archivo_comprimido=$comprimido->getClientOriginalName();
                $contenido_comprimido=file_get_contents($comprimido->getRealPath());
                Storage::disk('local')->put($directorio_empresa.$archivo_comprimido,$contenido_comprimido);
                $data_comprobante['ruta_zip']=$archivo_comprimido;

                //almacenar pdf
    			$pdf=$request->file('pdf');
        		if (isset($pdf) && $pdf->isValid()) {
        			$archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
                    $contenido_pdf=file_get_contents($pdf->getRealPath());
        			Storage::disk('local')->put($directorio_empresa.$archivo_pdf, $contenido_pdf);
                    $data_comprobante['ruta_pdf']=$archivo_pdf;
        		}

               

                // $esInstancia=$empresa instanceof Empresa;
                // if(!$esInstancia){
                //   $empresa=$this->registroEmpresa($data_comprobante);  
                // }
                $empresa=Empresa::where('ruc',$data_comprobante['data_empresa']['ruc'])->first();
                
                $concepto=NotaDebito::getConcepto($data_comprobante['concepto']);
                $factura=Factura::buscarPorNumeracion($data_comprobante['serie_factura'],$data_comprobante['numero_factura'])->first();
                // dd($concepto,$factura,$data_comprobante);

                if (!isset($concepto)) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'El concepto no es reconocido, por favor registre manualmente.');
                    return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
                }

                 if (!($factura instanceof Factura)) {
                    $notificacion=array('tipo'=>'warning','mensaje'=>'No se ha encontrado la factura, la factura debe estar registrada en el sistema.');
                    return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
                }


                $nota_debito=$this->registroNotaDebito($data_comprobante,$factura,$concepto,$empresa);

                $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
    		}else{
                $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
            }

    		if ($request->ajax()){
    		    return response()->json($notificacion);
    		}else{
                //return response()->make($notificacion['mensaje']);
    			return redirect()->route('admin.facturacion.nota_debito.listado')->with('notificacion',$notificacion);
            }
    		
    }

    public function extraerContenido($ruta_archivo){
    	$comprimido = new Zipper();
 
		//Abrimos el archivo a descomprimir
		$comprimido->make($ruta_archivo->getPathName());
		
		$nombre_original=basename($ruta_archivo->getClientOriginalName());

		$archivo=str_replace(".zip", ".XML", $nombre_original);
		$contenido=$comprimido->getFileContent($archivo);
		
		$xml=simplexml_load_string($contenido,null,true);
		
		$data_comprobante=$this->extraerInfoNotaDebito($xml);

        $data_archivo=compact('archivo','contenido');

		return compact('data_archivo','data_comprobante');
    }

   
    // private function registroEmpresa($data_comprobante){
    //     $data=$data_comprobante['data_empresa'];
	
    //     $empresa=new Empresa();
    //     $empresa->ruc=$data['ruc'];
    //     $empresa->razon_social=$data['razon_social'];
    //     $empresa->direccion=$data['direccion'];
    //     $empresa->estado='AC';
    //     if ($empresa->save()) {
    //         return $empresa;   
    //     }
    // }

    private function registroNotaDebito($data_comprobante, $factura,$concepto,$empresa){


    	$nota_debito=new NotaDebito();
        $nota_debito->serie=$data_comprobante['serie'];
        $nota_debito->numero=$data_comprobante['numero'];
        $nota_debito->fecha_emision=$data_comprobante['fecha_emision'];
        $nota_debito->id_empresa=$empresa->id;
        $nota_debito->id_factura=$factura->id;
        // $nota_debito->id_cliente=$data_comprobante['id_'];
        $nota_debito->id_concepto=$concepto->id;
        $nota_debito->importe=$data_comprobante['importe'];

        $nota_debito->ruta_zip=$data_comprobante['ruta_zip'];
        $nota_debito->importe=$data_comprobante['importe'];
        if (isset($data_comprobante['ruta_pdf']) && $data_comprobante['ruta_pdf']!="") {
            $nota_debito->ruta_pdf=$data_comprobante['ruta_pdf'];
        }
        $nota_debito->estado='AC';
        if ($nota_debito->save()) {
            // dd($nota_debito);
            return $nota_debito;
        }
    }

    private function extraerInfoNotaDebito($xml){
        $ns = $xml->getNamespaces(true);
        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
        $serie_numero_factura = explode("-",$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->ReferenceID->__toString());
        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();

        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
        $serie=$serie_numero[0];
        $numero=$serie_numero[1];

        $serie_factura=$serie_numero_factura[0];
        $numero_factura=$serie_numero_factura[1];

        $data_empresa=$this->extraerInfoEmpresa($xml,$ns,'AccountingCustomerParty');

        $concepto=$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->Description->__toString();
        $data_emisor=$this->extraerInfoEmpresa($xml,$ns,'AccountingSupplierParty');



        $data=compact('serie','numero','serie_factura','numero_factura','fecha_emision','importe','concepto');
        $data_array=compact('data_empresa','data_emisor');
        $data=array_map("trim",$data);
        $data=array_merge($data,$data_array);

        // dd($data);
        return $data;
    }

    private function extraerInfoEmpresa($xml,$ns,$seleccion){
    	$item=$xml->children($ns['cac'])->$seleccion;

        // dd($selec$item->children($ns['cac'])->Party->children($ns['cac'])->PostalAddress->children($ns['cbc']));
        // $direccion_min=$item
        //     ->children($ns['cac'])->Party
        //     ->children($ns['cac'])->PostalAddress
        //     ->children($ns['cbc']);

    	$ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
		// $razon_social=$item
		// 	->children($ns['cac'])->Party
		// 	->children($ns['cac'])->PartyLegalEntity
		// 	->children($ns['cbc'])->RegistrationName->__toString();

		

        // dd($concepto);
		// 	."[".$direccion_min->District->__toString()
		// 	.", ".$direccion_min->CityName->__toString()
		// 	.", ".$direccion_min->CountrySubentity->__toString()."]";

		$data=compact('ruc');

		return array_map("trim", $data);
    }

    public function descargarZIP(Request $request,$id_comprobante){
        $nota_debito=NotaDebito::findOrFail($id_comprobante);

        $archivo=$nota_debito->empresa->ruc."/nce/".$nota_debito->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app').$archivo,$nota_debito->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $nota_debito=NotaDebito::findOrFail($id_comprobante);

        $archivo=$nota_debito->empresa->ruc."/nce/".$nota_debito->ruta_pdf;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app').$archivo,$nota_debito->ruta_pdf);    
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }
}
