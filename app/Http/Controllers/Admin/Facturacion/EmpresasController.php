<?php

namespace App\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

// use Validator;
use App\Modulos\Facturacion\Empresa;
use App\Modulos\Util;

class EmpresasController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.facturacion.empresa.crear');

		// //LISTADO
        $params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $listado_empresas=Empresa::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_empresas as $id => $empresa) {
            $empresa->url_usuarios=route('admin.facturacion.usuario.listado',[$empresa->id]);
			$empresa->url_eliminar=route('admin.facturacion.empresa.eliminar',[$empresa->id]);
		}

		$data=compact('listado_empresas','campos_orden','url_nuevo');
		return view('admin.facturacion.empresa.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.facturacion.empresa.listado');

		$empresa=new Empresa();

		$estados_disponibles=collect();
		$estados_disponibles->push((object)array('id'=>Empresa::ESTADO_ACTIVO,'nombre'=>'Activo'));
		$estados_disponibles->push((object)array('id'=>Empresa::ESTADO_INACTIVO,'nombre'=>'Inactivo'));

		$data=compact('empresa', 'estados_disponibles', 'url_volver');
		return view('admin.facturacion.empresa.formulario', $data);

    }

    public function almacenar(Request $request){
    	$this->validate($request, [
            'razon_social' => 'required',
            'ruc' => 'required|size:11|unique:empresas',
            'direccion' => 'required',
        ]);

        $empresa=new Empresa();
        $empresa->ruc=$request->input('ruc');
        $empresa->razon_social=$request->input('razon_social');
        $empresa->direccion=$request->input('direccion');
        $empresa->estado='AC';//$request->input('estado');
       
        if ($empresa->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function eliminar(Request $request,$id_empresa){
    	$empresa=Empresa::findOrFail($id_empresa);

    	if ($empresa->facturas()->count()>0) {
    		$notificacion=array('tipo'=>'warning','mensaje'=>'No se puede eliminar si tiene facturas asociadas');
    	}elseif ($empresa->cuentas()->count()>0) {
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se puede eliminar si tiene usuarios asociados');
        }elseif ($empresa->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return back()->with('notificacion',$notificacion);
    }
}
