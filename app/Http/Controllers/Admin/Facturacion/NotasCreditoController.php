<?php

namespace App\Http\Controllers\Admin\Facturacion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modulos\Facturacion\NotaCredito;
use App\Modulos\Facturacion\Empresa;
use App\Modulos\Facturacion\Factura;
use App\Modulos\Util;

// use Validator;
use Illuminate\Support\Facades\Log;
use Storage;
use File;
use Response;
use Dompdf\Dompdf;
use App\NumeroALetras;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Chumper\Zipper\Zipper;


class NotasCreditoController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function listado(Request $request){
    	//preparar filtro y ordenado
		$url_nuevo=route('admin.facturacion.nota_credito.crear');
		$url_subida=route('admin.facturacion.nota_credito.subida');


		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        // $listado_cuentas=Cuenta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);
		$listado_notas_credito=NotaCredito::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_notas_credito as $id => $nota_credito) {
            $nota_credito->url_zip=route('admin.facturacion.descargar.nota_credito.zip',[$nota_credito->id]);
//            if ($nota_credito->ruta_pdf!="") {
                $nota_credito->url_pdf=route('admin.facturacion.descargar.nota_credito.pdf',[$nota_credito->id]);
//            }
			$nota_credito->url_eliminar=route('admin.facturacion.nota_credito.eliminar',[$nota_credito->id]);
            $nota_credito->url_excel = route('admin.facturacion.descargar.nota_credito.excel',[$nota_credito->id]);
		}

		$data=compact('listado_notas_credito','campos_orden','url_nuevo','url_subida');
		return view('admin.facturacion.nota_credito.listado', $data);
    }

    public function crear(){
    	$url_volver=route('admin.facturacion.nota_credito.listado');

		$nota_credito=new NotaCredito();
		$nota_credito->empresa=new Empresa();

        $conceptos_disponibles=NotaCredito::getConceptos();

		$empresas_disponibles=Empresa::cargarListado(array())->get();

		$data=compact('nota_credito', 'empresas_disponibles','conceptos_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.facturacion.nota_credito.formulario', $data);
    }

    public function almacenar(Request $request){
    	$this->validate($request, [
    		'serie' => 'required|unique:empresa_notas_credito,serie,numero',
    		'numero' => 'required|unique:empresa_notas_credito,numero,serie',
    		'id_empresa' => 'required',
            'id_concepto' => 'required',
            'id_factura' => 'required',
            'zip' => 'required|mimetypes:application/zip',
            'fecha_emision' => 'required|date',
            'importe' => 'required|numeric',
            // 'xml' => 'required|mimetypes:application/xml',
            'pdf' => 'mimetypes:application/pdf',
        ]);

       // dd("x");

        $empresa=Empresa::findOrFail($request->input('id_empresa'));
        $directorio_empresa="facturacion/".$empresa->ruc."/nce/";

        $ruta_zip=null;
        if ($request->file('zip')!=null && $request->file('zip')->isValid()) {
        	$ruta_zip=$request->file('zip')->getClientOriginalName();
        	Storage::disk('local')->put($directorio_empresa.$ruta_zip, file_get_contents($request->file('zip')->getRealPath()));
        }else{
        	return redirect()->with('error','No se puede subir el zip');
        }

        $ruta_pdf=null;
        if ($request->file('pdf')!=null && $request->file('pdf')->isValid()) {
        	$ruta_pdf=str_replace(".zip",".pdf", $ruta_zip);
        	Storage::disk('local')->put($directorio_empresa.$ruta_pdf, file_get_contents($request->file('pdf')->getRealPath()));
        }else{
        	//return response()->make($notificacion['mensaje']);
            // return redirect()->with('error','No se puede subir el zip');
        }

        $nota_credito=new NotaCredito();
        $nota_credito->serie=$request->input('serie');
        $nota_credito->numero=$request->input('numero');
        $nota_credito->id_empresa=$request->input('id_empresa');
        $nota_credito->id_concepto=$request->input('id_concepto');
        $nota_credito->id_factura=$request->input('id_factura');
        $nota_credito->fecha_emision=$request->input('fecha_emision');
        $nota_credito->importe=$request->input('importe');
        $nota_credito->ruta_zip=$ruta_zip;
        $nota_credito->ruta_pdf=$ruta_pdf;
        $nota_credito->estado='AC';//$request->input('estado');
       
       // dd($nota_credito);
        if ($nota_credito->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Guardado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo guardar');
        }

        return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
    }


    public function eliminar(Request $request, $id_comprobante){
    	$nota_credito=NotaCredito::findOrFail($id_comprobante);
        if ($nota_credito->delete()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
        }else{
        	$notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

        return back()->with('notificacion',$notificacion);
    }

    public function comboComprobantes(Request $request){
        $id_empresa=$request->get('id_empresa');
        $empresa=Empresa::findOrFail($id_empresa);
        $facturas_disponibles=Factura::where('id_empresa',$id_empresa)
                                    ->orderBy('serie')->orderBy('numero')->get();

        $data=compact('facturas_disponibles','empresa');
        return view('admin.facturacion.nota_credito.combo-facturas',$data);
    }

    public function subida(Request $request){
    	$url_volver=route('admin.facturacion.nota_credito.listado');
    	$data=compact('url_volver');
    	return view('admin.facturacion.nota_credito.subida',$data);
    }

    public function almacenarSubida(Request $request){
    	//obtener zip
    	//abrir zip
    	//leer xml
    	//registrar empresa si no existe por ruc
    	//registrar factura

        $comprimido=$request->file('comprimido');
    	$status=false;

    	if (isset($comprimido) && $comprimido->isValid()) {
    		$data=$this->extraerContenido($comprimido);
    		extract($data);	

            
            if ($data_comprobante['data_emisor']['ruc']!=env('RUC_EMPRESA','20524310857')) {
                $notificacion=array('tipo'=>'warning','mensaje'=>'El emisor no corresponse a la empresa ');
                return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
            }

            $nota_credito=NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
            if($nota_credito instanceof NotaCredito){
                $notificacion=array('tipo'=>'warning','mensaje'=>'El comprobante ya ha sido registrado ');
                return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
            }

            //definir ruta base
            $directorio_empresa="facturacion/".$data_comprobante['data_empresa']['ruc']."/nce/";
            //almacenar comprimido
            $archivo_comprimido=$comprimido->getClientOriginalName();
            $contenido_comprimido=file_get_contents($comprimido->getRealPath());
            Storage::disk('local')->put($directorio_empresa.$archivo_comprimido,$contenido_comprimido);
            $data_comprobante['ruta_zip']=$archivo_comprimido;

            //almacenar pdf
			$pdf=$request->file('pdf');
    		if (isset($pdf) && $pdf->isValid()) {
    			$archivo_pdf=str_replace(".zip", ".pdf", $comprimido->getClientOriginalName());
                $contenido_pdf=file_get_contents($pdf->getRealPath());
    			Storage::disk('local')->put($directorio_empresa.$archivo_pdf, $contenido_pdf);
                $data_comprobante['ruta_pdf']=$archivo_pdf;
    		}

           

            // $esInstancia=$empresa instanceof Empresa;
            // if(!$esInstancia){
            //   $empresa=$this->registroEmpresa($data_comprobante);  
            // }
            $empresa=Empresa::where('ruc',$data_comprobante['data_empresa']['ruc'])->first();
            
            $concepto=NotaCredito::getConcepto($data_comprobante['concepto']);
            $factura=Factura::buscarPorNumeracion($data_comprobante['serie_factura'],$data_comprobante['numero_factura'])->first();
             //d($concepto,$factura,$data_comprobante);

            if (!isset($concepto)) {
                $notificacion=array('tipo'=>'warning','mensaje'=>'El concepto no es reconocido, por favor registre manualmente.');
                return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
            }

             if (!($factura instanceof Factura)) {
                $notificacion=array('tipo'=>'warning','mensaje'=>'No se ha encontrado la factura, la factura debe estar registrada en el sistema.');
                return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
            }


            $nota_credito=$this->registroNotaCredito($data_comprobante,$factura,$concepto,$empresa);

            $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
		}else{
            $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo eliminar');
        }

		if ($request->ajax()){
		    return response()->json($notificacion);
		}else{
            //return response()->make($notificacion['mensaje']);
			return redirect()->route('admin.facturacion.nota_credito.listado')->with('notificacion',$notificacion);
		}
    }

    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);

        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito");
        }else {
            return $comprobante->descargarZip();
        }
    }

    public function descargarPDF(Request $request,$id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de factura");
        }else {
            return $comprobante->descargarPdf();
        }
    }

    public  function  descargarEXCEL(Request $request, $id_comprobante){
        $comprobante=NotaCredito::findOrFail($id_comprobante);
        if(!isset($comprobante)){
            return response()->make("No se encontro la nota de credito de factura");
        }else {
            return $comprobante->descargarExcel();
        }
    }
//    public function extraerContenido($ruta_archivo){
//    	$comprimido = new Zipper();
//
//		//Abrimos el archivo a descomprimir
//		$comprimido->make($ruta_archivo->getPathName());
//
//		$nombre_original=basename($ruta_archivo->getClientOriginalName());
//
//		$archivo=str_replace(".zip", ".xml", $nombre_original);
//		$contenido=$comprimido->getFileContent($archivo);
//
//		//Por mientras
//        $content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 ..\xsd\maindoc\UBLPE-CreditNote-1.0.xsd"',' ',
//            $contenido);
//		$xml=simplexml_load_string($content,null,true);
//
//		$data_comprobante=$this->extraerInfoNotaCredito($xml);
//
//        $data_archivo=compact('archivo','contenido');
//
//		return compact('data_archivo','data_comprobante');
//    }

//    private function registroNotaCredito($data_comprobante, $factura,$concepto,$empresa){
//    	$nota_credito=new NotaCredito();
//        $nota_credito->serie=$data_comprobante['serie'];
//        $nota_credito->numero=$data_comprobante['numero'];
//        $nota_credito->fecha_emision=$data_comprobante['fecha_emision'];
//        $nota_credito->id_empresa=$empresa->id;
//        $nota_credito->id_factura=$factura->id;
//        // $nota_credito->id_cliente=$data_comprobante['id_'];
//        $nota_credito->id_concepto=$concepto->id;
//        $nota_credito->importe=$data_comprobante['importe'];
//
//        $nota_credito->ruta_zip=$data_comprobante['ruta_zip'];
//        $nota_credito->importe=$data_comprobante['importe'];
//        if (isset($data_comprobante['ruta_pdf']) && $data_comprobante['ruta_pdf']!="") {
//            $nota_credito->ruta_pdf=$data_comprobante['ruta_pdf'];
//        }
//        $nota_credito->estado='AC';
//        if ($nota_credito->save()) {
//            // dd($nota_credito);
//            return $nota_credito;
//        }
//    }

//    private function extraerInfoNotaCredito($xml){
//        $ns = $xml->getNamespaces(true);
//        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
//        $serie_numero_factura = explode("-",$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->ReferenceID->__toString());
//        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();
//
//        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
//        $serie=$serie_numero[0];
//        $numero=$serie_numero[1];
//
//        $serie_factura=$serie_numero_factura[0];
//        $numero_factura=$serie_numero_factura[1];
//
//        $label = $xml->children($ns['ext'])->UBLExtensions->children($ns['ext'])[0]
//            ->ExtensionContent->children($ns['sac'])
//            ->AdditionalInformation->children($ns['sac']);
//
//        $gravadas = $label->AdditionalMonetaryTotal[0]->children($ns['cbc'])->PayableAmount->__toString();
//        $inafectas = $label->AdditionalMonetaryTotal[1]->children($ns['cbc'])->PayableAmount->__toString();
//        $exoneradas = $label->AdditionalMonetaryTotal[2]->children($ns['cbc'])->PayableAmount->__toString();
//
//
//        $totales = ['gravadas' => $gravadas ,'inafectas' => $inafectas,'exoneradas' => $exoneradas];
//
//        $data_empresa=$this->extraerInfoEmpresa($xml,$ns,'AccountingCustomerParty');
//
//        $concepto=$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->Description->__toString();
//        $data_emisor=$this->extraerInfoEmpresa($xml,$ns,'AccountingSupplierParty');
//        $data_producto = $this->extraerProductos($xml,$ns);
//
//
//        $data=compact('serie','numero','serie_factura','numero_factura','fecha_emision','importe','concepto');
//        $data_array=compact('data_empresa','data_emisor','data_producto','totales');
//        $data=array_map("trim",$data);
//        $data=array_merge($data,$data_array);
//
//        // dd($data);
//        return $data;
//    }

//    public function extraerProductos($xml,$ns){
//        $contador = count($xml->children($ns['cac'])->CreditNoteLine);
//        //dd($contador);
//        $listado_productos = array();
//        for ($i = 0; $i < $contador; $i++){
//            $label = $xml->children($ns['cac'])->CreditNoteLine[$i];
//
//            $DescripcionProducto=$label->children($ns['cac'])->Item->children($ns['cbc'])->Description->__toString();
//
//            $CantidadUnidad=$label->children($ns['cbc'])->CreditedQuantity->__toString();
//
//            $TipoCantidad = $label->children($ns['cbc'])->CreditedQuantity->attributes()->unitCode->__toString();//UNIDAD DE MEDIDA
//
//            $PrecioVentaUnitario = $label->children($ns['cac'])->Price->children($ns['cbc'])->PriceAmount->__toString();
//
//            $listado_productos[$i] = [ 'descripcion_producto' => trim($DescripcionProducto),
//                'cantidadUnidad' => trim($CantidadUnidad),
//                'tipoCantidad' => trim($TipoCantidad),
//                'PrecioVentaUnitario' => trim($PrecioVentaUnitario)];
//            //'PrecioVentaTotal' => trim($PrecioVentaTotal)];
//        }
//
//        $data=compact('listado_productos');
//
//        return $data;
//    }
//
//    private function extraerInfoEmpresa($xml,$ns,$seleccion){
//    	$item=$xml->children($ns['cac'])->$seleccion;
//
//        // dd($selec$item->children($ns['cac'])->Party->children($ns['cac'])->PostalAddress->children($ns['cbc']));
//        // $direccion_min=$item
//        //     ->children($ns['cac'])->Party
//        //     ->children($ns['cac'])->PostalAddress
//        //     ->children($ns['cbc']);
//
//    	$ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
//		 $razon_social=$item
//		 	->children($ns['cac'])->Party
//		 	->children($ns['cac'])->PartyLegalEntity
//		 	->children($ns['cbc'])->RegistrationName->__toString();
//
//
//
//        // dd($concepto);
//		// 	."[".$direccion_min->District->__toString()
//		// 	.", ".$direccion_min->CityName->__toString()
//		// 	.", ".$direccion_min->CountrySubentity->__toString()."]";
//
//		$data=compact('ruc','razon_social');
//
//		return array_map("trim", $data);
//    }



//    //FUNCIONALIDADES DEL FTP
//    public function almacenarSubidaFTP(){
//
//        set_time_limit(0);
//
//        $storage_directory = Storage::disk('ftp');
//        $directory = "./";
//        $listado_archivo = $storage_directory->allFiles($directory);
//       // dd($listado_archivo);
//        $contador = count($listado_archivo);
//        for($i=0;$i< $contador;$i++){
//            $bandera_F =strpos($listado_archivo[$i],'F');
//            $bandera_C = strpos($listado_archivo[$i],'-07-');
//            if($bandera_F == true && $bandera_C == true){
//                $nombre = explode('-',$listado_archivo[$i]);
//                $serie = $nombre[2];
//                $numero = trim(str_replace('.zip',' ',$nombre[3]));
//                echo $numero;
//                $factura = NotaCredito::where('serie','=',$serie)->where('numero','=',$numero)->first();
//                if($factura != null){
//                    echo $serie . "-" . $numero . "REGISTRADO" . "</br>";
//                }else{
//                    try{
//                        $mensaje[$i] = $this->subidaFTP($listado_archivo[$i]);
//                    }catch(\Exception $e){
//                        Log::error("excepcion: ".$listado_archivo[$i]." -> ".$e->getMessage());
//                    }
//                }
//            }
//
//        }
//    }
//
//    public function SubidaFTP($archivo){
//            $data=$this->extraerContenidoFTP($archivo);
//            extract($data);
//
//            $ruc_permitido=env('RUC_EMPRESA','20524310857');
//            if ($data_comprobante['data_emisor']['ruc']!=$ruc_permitido) {
//                throw new \Exception('El emisor en ncf no corresponse, asociado a '.$ruc_permitido.', valor recibido:'.$data_comprobante['data_emisor']['ruc']);
//            }
//
//            $nota_credito=NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
//            if($nota_credito instanceof NotaCredito){
//                throw new \Exception("El nota de credito factura ya registrado.".$archivo);
//            }
//
//            $data_comprobante['ruta_zip']=$archivo;
//
//            $empresa=Empresa::where('ruc',$data_comprobante['data_empresa']['ruc'])->first();
//
//            $concepto=NotaCredito::getConcepto($data_comprobante['concepto']);
//            $factura=Factura::buscarPorNumeracion($data_comprobante['serie_factura'],$data_comprobante['numero_factura'])->first();
//
//
//            if (!isset($concepto)) {
//                throw new \Exception("El concepto en ncf no reconocido: id=".$data_comprobante['concepto']);
//            }
//
//            if (!($factura instanceof Factura)) {
//                throw new \Exception("Factura en ncf no encontrada: ".$archivo);
//            }
//
//
//            $nota_credito=$this->registroNotaCredito($data_comprobante,$factura,$concepto,$empresa);
//
//            //Generar pdf
//            $this->generarPDF($data_comprobante);
//
//            //Generar Excel
//            $this->generarExcel($data_comprobante);
//
//            Storage::disk('local')->delete($archivo);
//
//            $notificacion=array('tipo'=>'success','mensaje'=>'Subido con exito');
//        return $notificacion;
//    }
//
////    public function extraerContenidoFTP($ruta_archivo){
////
////        $comprimido = new Zipper();
////        $nombre_original = $ruta_archivo;
////
////        $comprimido->make(storage_path( 'app/' . $nombre_original));
////
////        $archivo=str_replace(".zip", ".xml", $nombre_original);
////
////        $contenido=$comprimido->getFileContent($archivo);
////
//////        //Por mientras
////        $content = str_replace('xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 ..\xsd\maindoc\UBLPE-CreditNote-1.0.xsd"',' ',
////            $contenido);
////        $xml=simplexml_load_string($content,null,true);
////
////        $data_comprobante=$this->extraerInfoNotaCredito($xml);
////
////        $data_archivo=compact('archivo','contenido');
////
////        $comprimido->close();
////
////        return compact('data_archivo','data_comprobante');
////    }
//
////    public function generarPDF($data_comprobante){
////        //dd($data_comprobante);
////        $sumatoria = 0.00;
////        $igv = $data_comprobante['importe'] - $data_comprobante['totales']['gravadas'];
////        $data = [
////            'codigo_factura' => $data_comprobante['serie'] . "-" . $data_comprobante['numero'] ,
////            'serie_boleta' => $data_comprobante['serie_factura'],
////            'numero_boleta' => $data_comprobante['numero_factura'],
////            'establecimiento' => 'LIMA',
////            'nombre_empresa' => 'TOTAL CALIDAD SAC',
////            'ruc_empresa' => $data_comprobante['data_emisor']['ruc'],
////            'fecha_emision' => $data_comprobante['fecha_emision'],
////            'razon_social' => $data_comprobante['data_empresa']['razon_social'],
////            'ruc' => $data_comprobante['data_empresa']['ruc'],
////            'tipo_moneda' => 'PEN',
////            'sumatoria_descuentos' => $sumatoria,
////            'importe_total' =>  $data_comprobante['importe'],
////            'concepto' =>  $data_comprobante['concepto'],
////            'total_venta_gravada' => $data_comprobante['totales']['gravadas'],
////            'total_venta_exonerada' => $data_comprobante['totales']['exoneradas'],
////            'total_venta_inafecta'  => $data_comprobante['totales']['inafectas'],
////            'sumatoria_IGV' => $igv,
////            'nombre_documento' => 'NOTA DE CREDITO',
////            'numero' => $data_comprobante['numero'],
////            'serie' => $data_comprobante['serie']
////            //'direccion_persona' => $data_comprobante['data_empresa']['direccion_persona']
////        ];
////
////        $temp = $data_comprobante['data_producto']['listado_productos'];
////
////        $fecha = Carbon::now();
////        $anio = $fecha->year;
////        $month = $fecha->month;
////
////        $mes = $this->sacarMes($month);
////
////        //Crear la carpeta general
////        $general = "facturacion/pdf/";
////        //Crear Directorio
////        $exists = Storage::disk('local')->exists($general);
////        $exists2 = Storage::disk('local')->exists($general . $anio);
////        $exists3 = Storage::disk('local')->exists($general . $anio. '/' . $mes);
////
////        if($exists){
////        }else{
////            Storage::makeDirectory  ($general);
////        }
////        if($exists2){
////        }else{
////            Storage::makeDirectory($general . $anio);
////        }
////        if($exists3){
////        }else{
////            Storage::makeDirectory($general . $anio. '/' . $mes);
////        }
////
////        if($data != null && $temp != null)
////        {
////            //Convertir de numeros a letras
////            $numero_letras = NumeroALetras::convertir(number_format(floatval($data['importe_total']),2),'','CENTIMOS');
////            //Agregar todo los datos al invoice
////            $view = \View::make('admin.pdf.nota_credito.comprobante', compact('data','temp','numero_letras'))->render();
////
////            $pdf = \App::make('dompdf.wrapper');
////
////            $dompdf = new Dompdf();
////            $dompdf = $pdf;
////
////            $numero = $data['numero'];
////            $serie = $data['serie'];
////            $codigo_factura = $data['codigo_factura'];
////
////            $tipo = '07';
////            $compro = "Notas/Credito";
////            Storage::disk('local')->exists($general . $anio. '/' . $mes . '/' . $compro) or Storage::makeDirectory( $general . '/' . $anio. '/' . $mes . '/' .$compro);
////            //hacer el update para poder grabar la ruta de la Boleta
////            $bol = NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero']);
////
////            $comprobante = $bol->first();
////            //dd($comprobante);
////            $comprobante->ruta_pdf = $general . $anio. '/' . $mes . '/' . $compro . '/' . $data['ruc_empresa'] .  "-" . $codigo_factura . ".pdf";
////
////            $comprobante->save();
////
////            $pdf->loadHTML($view);
////            $filename = $general . $anio. '/' . $mes . '/' . $compro . '/' . $data['ruc_empresa'] . "-" . $codigo_factura . ".pdf";
////
////            Storage::disk('local')->put($filename, $dompdf->output());
////
////            return $dompdf->stream();
////
////        }else{
////            $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo generar el pdf');
////            return $notificacion;
////        }
////
////    }
////
////    public function generarExcel($data_comprobante){
////
////        $general = 'app/facturacion/Notas/Credito/excel';
////        $productos = $data_comprobante['data_producto']['listado_productos'];
////        Excel::create( env('RUC_EMPRESA'). '-' . $data_comprobante['serie'] . "-" . $data_comprobante['numero'] ,function($excel) use ($productos,$data_comprobante) {
////            $excel->sheet('Nota-Credito' ,function($sheet) use ($productos, $data_comprobante) {
////                $razon_social = $data_comprobante['data_empresa']['razon_social'];
////                $ruc = $data_comprobante['data_empresa']['ruc'];
////                $importe_total =  $data_comprobante['importe'];
////                $total_venta_gravada = $data_comprobante['totales']['gravadas'];
////                $total_venta_exonerada = $data_comprobante['totales']['exoneradas'];
////                $total_venta_inafecta  = $data_comprobante['totales']['inafectas'];
////                $total_igv = $igv = $data_comprobante['importe'] - $data_comprobante['totales']['gravadas'];
////                $motivo = $data_comprobante['concepto'];
////
////                //ESTILOS
////                $sheet->cell('A1:J20', function($cells) {
////                    $cells->setAlignment('center');
////                });
////                $contador = count($productos);
////                $sheet->setBorder('A6:E'. ($contador + 6), 'thin');
////                $sheet->setBorder('D'.(8 + $contador) .':E'. ($contador + 12), 'thin');
////                //PARTE IZQUIERDA CABEZERA
////                $sheet->cell('A1', function($cell) use ($data_comprobante) {
////                    $nombre_empresa = $data_comprobante['data_emisor']['razon_social'];
////                    $cell->setValue($nombre_empresa);
////                });
////                $sheet->cell('A2', function($cell){
////                    $cell->setValue('ruc o RUC: ');
////                });
////                $sheet->cell('B2', function($cell) use ($ruc) {
////                    $cell->setValue($ruc);
////                });
////                $sheet->cell('A3', function($cell){
////                    $cell->setValue('Nombre o Razón Social: ');
////                });
////                $sheet->cell('B3', function($cell) use ($razon_social) {
////                    $cell->setValue($razon_social);
////                });
////                $sheet->cell('A4', function($cell){
////                    $cell->setValue('Motivo: ');
////                });
////                $sheet->cell('B4', function($cell) use ($motivo) {
////                    $cell->setValue($motivo);
////                });
////                //PARTE DERECHA CABEZERA
////                $sheet->cell('E1', function($cell) {
////                    $cell->setValue('NOTA DE CRÉDITO ELECTRÓNICA');
////                });
////                $sheet->cell('E2', function($cell) {
////                    $cell->setValue(env('RUC_EMPRESA'));
////                });
////                $sheet->cell('E3', function($cell) use ($data_comprobante) {
////                    $codigo_factura = $data_comprobante['serie'] . "-" . $data_comprobante['numero'];
////                    $cell->setValue($codigo_factura);
////                });
////                $sheet->cell('E4', function($cell) use ($data_comprobante) {
////                    $fecha = $data_comprobante['fecha_emision'];
////                    $cell->setValue('Fecha: '. $fecha);
////                });
////                //PARTE TABLA DE PRODUCTOS
////                $sheet->fromArray($productos, null, 'A6',true);
////                //PARTE TABLA TOTAL
////
////                $sheet->cell('D'.(8 + $contador), function($cell) {
////                    $cell->setValue('Total Gravadas:');
////                });
////                $sheet->cell('D'.(9 + $contador), function($cell) {
////                    $cell->setValue('Total Inafectas:');
////                });
////                $sheet->cell('D'.(10 + $contador), function($cell) {
////                    $cell->setValue('Total Exoneradas:');
////                });
////                $sheet->cell('D'.(11 + $contador), function($cell) {
////                    $cell->setValue('Total IGV:');
////                });
////                $sheet->cell('D'.(12 + $contador), function($cell) {
////                    $cell->setValue('Importe Total:');
////                });
////
////                $sheet->cell('E'.(8 + $contador), function($cell) use($total_venta_gravada) {
////                    $cell->setValue($total_venta_gravada);
////                });
////                $sheet->cell('E'.(9 + $contador), function($cell) use ($total_venta_inafecta) {
////                    $cell->setValue($total_venta_inafecta);
////                });
////                $sheet->cell('E'.(10 + $contador), function($cell) use ( $total_venta_exonerada) {
////                    $cell->setValue( $total_venta_exonerada);
////                });
////                $sheet->cell('E'.(11 + $contador), function($cell) use ($total_igv){
////                    $cell->setValue($total_igv);
////                });
////                $sheet->cell('E'.(12 + $contador), function($cell) use ($importe_total){
////                    $cell->setValue($importe_total);
////                });
////            });
////        })->store('xls',storage_path($general));
////
////        $bol = NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero']);
////        $comprobante = $bol->first();
////        $comprobante->ruta_excel = 'facturacion/Notas/Credito/excel' .'/' .env('RUC_EMPRESA'). '-' .$data_comprobante['serie'] . "-" . $data_comprobante['numero'] . ".xls";
////        if($comprobante->save()){
////        }else{
////            $notificacion=array('tipo'=>'error','mensaje'=>'No se pudo generar el Excel');
////            return $notificacion;
////        }
////    }
////
////    public function sacarMes($month){
////        if ($month == 1){
////            $mes = "Enero";
////        }elseif ($month == 2){
////            $mes = "Febrero";
////        }elseif ($month == 3){
////            $mes = "Marzo";
////        }elseif ($month == 4){
////            $mes = "Abril";
////        }elseif ($month == 5){
////            $mes = "Mayo";
////        }elseif ($month == 6){
////            $mes = "Junio";
////        }elseif ($month == 7) {
////            $mes = "Julio";
////        }elseif ($month == 8) {
////            $mes = "Agosto";
////        }elseif ($month == 9) {
////            $mes = "Septiembre";
////        }elseif ($month == 10) {
////            $mes = "Octubre";
////        }elseif ($month == 11) {
////            $mes = "Noviembre";
////        }elseif ($month == 12) {
////            $mes = "Diciembre";
////        }
////        return $mes;
////    }
}
