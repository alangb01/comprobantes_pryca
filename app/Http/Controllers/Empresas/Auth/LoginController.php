<?php

namespace App\Http\Controllers\Empresas\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Mews\Captcha;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:empresa', ['except' => 'logout']);
    }

    protected $redirectTo = '/empresas';
    // protected $guard = 'admin';

    public function showLoginForm(){
        $url = captcha_src();
        $codigo = parse_url($url);
        $capt = $codigo['query'];
        $captcha_img = captcha_img();
        $data = compact('captcha_img','url','capt');
        return view('empresas.auth.login',$data);
    }

    public function showRegistrationForm(){
        return view('empresas.auth.register');
    } 

    public function username(){
        return 'usuario';
    }

    protected function guard(){
        return Auth::guard('empresa');
    }

    protected function validator(array $data){
        return Validator::make($data, [
            'usuario' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data){
        return App\Modulos\Facturacion\Usuario::create([
            'usuario' => $data['usuario'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


}
