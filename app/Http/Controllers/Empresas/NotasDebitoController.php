<?php

namespace App\Http\Controllers\Empresas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modulos\Facturacion\NotaDebito;
use Storage;

use App\Modulos\Util;

use Illuminate\Support\Facades\Auth;

class NotasDebitoController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth:empresa']);
    }

    public function listado(Request $request){
        
        $empresa=Auth::guard('empresa')->user()->empresa;


    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $params_internos=array('id_empresa'=>$empresa->id);

        $listado_notas_debito=NotaDebito::cargarListado(array_merge($params_internos,array_merge($params_get,$params_url)))->paginate(10)->appends($params_get);    

        foreach ($listado_notas_debito as $id => $nota_debito) {
            $nota_debito->url_factura_zip=route('empresa.descargar.factura.zip',$nota_debito->factura->id);
            if ($nota_debito->factura->ruta_pdf!="") {
                $nota_debito->url_factura_pdf=route('empresa.descargar.factura.pdf',$nota_debito->factura->id);
            }

            $nota_debito->url_zip=route('empresa.descargar.nota_debito.zip',[$nota_debito->id]);
            if ($nota_debito->ruta_pdf!="") {
                $nota_debito->url_pdf=route('empresa.descargar.nota_debito.pdf',[$nota_debito->id]);

            }
        }


    	$data=compact('listado_notas_debito','campos_orden','empresa');
    	return view('empresas.notas_debito.listado',$data);
    }

    public function descargarZIP(Request $request,$id_nota_debito){
        $nota_debito=NotaDebito::findOrFail($id_nota_debito);

        $archivo='facturacion/'.$nota_debito->empresa->ruc."/nce/".$nota_debito->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_debito->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_nota_debito){
    	$nota_debito=NotaDebito::findOrFail($id_nota_debito);
    	$archivo='facturacion/'.$nota_debito->empresa->ruc."/nce/".$nota_debito->ruta_pdf;
    	if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_debito->ruta_pdf);    
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }

}
