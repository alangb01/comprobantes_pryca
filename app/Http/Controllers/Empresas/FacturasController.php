<?php

namespace App\Http\Controllers\Empresas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modulos\Facturacion\Factura;
use Storage;

use App\Modulos\Util;

use Illuminate\Support\Facades\Auth;

class FacturasController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth:empresa']);
    }

    public function listado(Request $request){
        
        $empresa=Auth::guard('empresa')->user()->empresa;
        // dd($empresa);

    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $params_internos=array('id_empresa'=>$empresa->id);

        $listado_facturas=Factura::cargarListado(array_merge($params_internos,array_merge($params_get,$params_url)))->paginate(10)->appends($params_get);    

        foreach ($listado_facturas as $id => $comprobante) {
            $comprobante->url_zip=route('empresa.descargar.factura.zip',[$comprobante->id]);
            if ($comprobante->ruta_pdf!="") {
                $comprobante->url_pdf=route('empresa.descargar.factura.pdf',[$comprobante->id]);
            }
            $comprobante->url_excel=route('empresa.descargar.factura.excel',[$comprobante->id]);
        }


    	$data=compact('listado_facturas','campos_orden','empresa');
    	return view('empresas.facturas.listado',$data);
    }

    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=Factura::findOrFail($id_comprobante);

        $archivo='facturacion/'.$comprobante->empresa->ruc."/fe/".$comprobante->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$comprobante->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_boleta){
        $boleta=Factura::findOrFail($id_boleta);
        $archivo=$boleta->ruta_pdf;
        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
        if(Storage::disk('local')->has($archivo)){
            //dd(storage_path('app')."/".$archivo);
            return response()->download(storage_path('app')."/".$archivo,$nombre.".pdf");
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }

    public  function  descargarEXCEL(Request $request, $id_boleta){
        $boleta=Factura::findOrFail($id_boleta);
        $archivo=$boleta->ruta_excel;
        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
        if(Storage::disk('local')->has($archivo)){
            //dd(storage_path('app'));
            return response()->download(storage_path('app')."/".$archivo,$nombre.".xls");
        }else{
            return response()->make("No se encontro el archivo excel");
        }
    }

}
