<?php

namespace App\Http\Controllers\Empresas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modulos\Facturacion\NotaCredito;
use Storage;

use App\Modulos\Util;

use Illuminate\Support\Facades\Auth;

class NotasCreditoController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth:empresa']);
    }

    public function listado(Request $request){
        
        $empresa=Auth::guard('empresa')->user()->empresa;


    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $params_internos=array('id_empresa'=>$empresa->id);

        $listado_notas_credito=NotaCredito::cargarListado(array_merge($params_internos,array_merge($params_get,$params_url)))->paginate(10)->appends($params_get);    

        foreach ($listado_notas_credito as $id => $nota_credito) {
            $nota_credito->url_factura_zip=route('empresa.descargar.factura.zip',$nota_credito->factura->id);
            if ($nota_credito->factura->ruta_pdf!="") {
                $nota_credito->url_factura_pdf=route('empresa.descargar.factura.pdf',$nota_credito->factura->id);
            }

            $nota_credito->url_zip=route('empresa.descargar.nota_credito.zip',[$nota_credito->id]);
            if ($nota_credito->ruta_pdf!="") {
                $nota_credito->url_pdf=route('empresa.descargar.nota_credito.pdf',[$nota_credito->id]);

            }
        }


    	$data=compact('listado_notas_credito','campos_orden','empresa');
    	return view('empresas.notas_credito.listado',$data);
    }

    public function descargarZIP(Request $request,$id_nota_credito){
        $nota_credito=NotaCredito::findOrFail($id_nota_credito);

        $archivo='facturacion/'.$nota_credito->empresa->ruc."/nce/".$nota_credito->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_credito->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_nota_credito){
    	$nota_credito=NotaCredito::findOrFail($id_nota_credito);
    	$archivo='facturacion/'.$nota_credito->empresa->ruc."/nce/".$nota_credito->ruta_pdf;
    	if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_credito->ruta_pdf);    
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }

}
