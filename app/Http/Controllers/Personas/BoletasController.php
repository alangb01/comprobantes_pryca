<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modulos\Boleteo\Boleta;
use Storage;

use App\Modulos\Util;

use Illuminate\Support\Facades\Auth;

class BoletasController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth:persona']);
    }

    protected function guard(){
        return Auth::guard('persona');
    }

    public function listado(Request $request){
        $persona=$this->guard()->user()->persona;

        // dd($persona->id_persona);
        // 
    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $params_internos=array('id_persona'=>$persona->id);

        $listado_boletas=Boleta::cargarListado(array_merge($params_internos,array_merge($params_get,$params_url)))->paginate(10)->appends($params_get);    

        foreach ($listado_boletas as $id => $comprobante) {
            $comprobante->url_zip=route('persona.descargar.boleta.zip',[$comprobante->id]);
            if ($comprobante->ruta_pdf!="") {
                $comprobante->url_pdf=route('persona.descargar.boleta.pdf',[$comprobante->id]);

            }
            $comprobante->url_excel=route('persona.descargar.boleta.excel',[$comprobante->id]);
        }


    	$data=compact('listado_boletas','campos_orden','persona');
    	return view('personas.boletas.listado',$data);
    }

    public function descargarZIP(Request $request,$id_comprobante){
        $comprobante=Boleta::findOrFail($id_comprobante);

        $archivo="boleteo/bve/".$comprobante->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$comprobante->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_boleta){
        $boleta=Boleta::findOrFail($id_boleta);
        $archivo=$boleta->ruta_pdf;
        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
        if(Storage::disk('local')->has($archivo)){
            //dd(storage_path('app')."/".$archivo);
            return response()->download(storage_path('app')."/".$archivo,$nombre.".pdf");
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }

    public  function  descargarEXCEL(Request $request, $id_boleta){
        $boleta=Boleta::findOrFail($id_boleta);
        $archivo=$boleta->ruta_excel;
        $nombre =  env('RUC_EMPRESA'). "-" .$boleta->serie . "-" .$boleta->numero;
        if(Storage::disk('local')->has($archivo)){
            //dd(storage_path('app'));
            return response()->download(storage_path('app')."/".$archivo,$nombre.".xls");
        }else{
            return response()->make("No se encontro el archivo excel");
        }
    }

}
