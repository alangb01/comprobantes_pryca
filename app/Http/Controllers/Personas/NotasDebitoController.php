<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modulos\Boleteo\NotaDebito;
use Storage;

use App\Modulos\Util;

use Illuminate\Support\Facades\Auth;

class NotasDebitoController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth:persona']);
    }

    public function guard(){
        return Auth::guard('persona');
    }

    public function listado(Request $request){
        
        $persona=$this->guard()->user()->persona;


    	$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['numero','serie','ruc','razon_social'],$params_get,$params_url);

        $params_internos=array('id_persona'=>$persona->id);

        $listado_notas_debito=NotaDebito::cargarListado(array_merge($params_internos,array_merge($params_get,$params_url)))->paginate(10)->appends($params_get);    

        foreach ($listado_notas_debito as $id => $nota_debito) {

            $nota_debito->url_boleta_zip=route('persona.descargar.boleta.zip',$nota_debito->boleta->id);
            if ($nota_debito->boleta->ruta_pdf!="") {
                $nota_debito->url_boleta_pdf=route('persona.descargar.boleta.pdf',$nota_debito->boleta->id);
            }
            $nota_debito->url_zip=route('persona.descargar.nota_debito.zip',[$nota_debito->id]);
            if ($nota_debito->ruta_pdf!="") {
                $nota_debito->url_pdf=route('persona.descargar.nota_debito.pdf',[$nota_debito->id]);
            }
        }

    	$data=compact('listado_notas_debito','campos_orden','persona');
    	return view('personas.notas_debito.listado',$data);
    }

    public function descargarZIP(Request $request,$id_nota_debito){
        $nota_debito=NotaDebito::findOrFail($id_nota_debito);

        $archivo="boleteo/nde/".$nota_debito->ruta_zip;
        if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_debito->ruta_zip);    
        }else{
            return response()->make("No se encontro el archivo zip");
        }
    }

    public function descargarPDF(Request $request,$id_nota_debito){
    	$nota_debito=NotaDebito::findOrFail($id_nota_debito);
    	$archivo="boleteo/nde/".$nota_debito->ruta_pdf;
    	if(Storage::disk('local')->has($archivo)){
            return response()->download(storage_path('app')."/".$archivo,$nota_debito->ruta_pdf);    
        }else{
            return response()->make("No se encontro el archivo pdf");
        }
    }

}
