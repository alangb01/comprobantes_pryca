<?php

namespace App\Http\Controllers\Personas\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/persona';
   

    public function __construct() {
        $this->middleware('guest:persona');
    }

    protected function guard() {
        return Auth::guard('persona');
    }
   
    protected function broker() {
        return Password::broker('persona');
    }

    public function showLinkRequestForm()
    {
        return view('personas.auth.passwords.email');
    }
     
}
