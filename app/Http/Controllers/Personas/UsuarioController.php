<?php

namespace App\Http\Controllers\Personas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class UsuarioController extends Controller
{
    //
    public function __construct(){
    	$this->middleware(['auth:persona']);
    }

    public function index(){
    	return view('personas.index');
    }

    public function editar()
    {
        $usuario=Auth::user();
        $data=compact('usuario');
        return view('personas.cuenta.cambiar-datos',$data);
    }

    public function actualizar(Request $request)
    {
        //
        $usuario=Auth::user();
        
        $this->validate($request,[
            'usuario' => 'required|max:50|unique:persona_usuarios,id,:id',
            'email' => 'required|email|unique:persona_usuarios,id,:id',
            'password' => 'required'
        ]);
       

        if (!\Hash::check($request->input('password'),$usuario->password)) {
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe ingresar la contraseña actual');
            return back()->with('notificacion',$mensaje)
                        ->withInput();
        }

        $usuario->usuario=$request->input('usuario');
        $usuario->email=$request->input('email');
         
        if ($usuario->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
        }else{
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se pudo actualizar');
        }

        return back()->with('notificacion',$notificacion);
    }

  
    public function cambiarClave(){
        $usuario=Auth::user();
        $data=compact('usuario');
        return view('personas.cuenta.cambiar-clave',$data);
    }

    public function procesarCambioClave(Request $request){
        $usuario=Auth::user();

        $this->validate($request,[
            'password_actual' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        if (!\Hash::check($request->input('password'),$usuario->password)) {
            $mensaje=array('tipo'=>'warning','mensaje'=>'Debe ingresar la contraseña actual');
            return back()->with('notificacion',$mensaje)
                        ->withErrors();
        }
        //si no cumple las validaciones, regresa
        $usuario->password=bcrypt($request->input('password'));
        $usuario->forzar_cambio_clave=0;
       
        if ($usuario->save()) {
            $notificacion=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
        }else{
            $notificacion=array('tipo'=>'warning','mensaje'=>'No se pudo actualizar');
        }
        return back()->with('notificacion',$notificacion);
    }
}
