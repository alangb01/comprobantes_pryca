<?php

namespace App\Console;

use App\Console\Commands\EnviarBoletas;
use App\Console\Commands\EnviarFacturas;
use App\Console\Commands\EnviarNotaCreditoBoleta;
use App\Console\Commands\EnviarNotaCreditoFactura;
use App\Console\Commands\ProcesarComprobantesPendientes;
use App\Console\Commands\SincronizarFtp;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ProcesarComprobantesPendientes::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('comprobantes:procesar')
             ->everyThirtyMinutes()->between('7:00', '22:00')
             ->timezone('America/Lima')
             ->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
