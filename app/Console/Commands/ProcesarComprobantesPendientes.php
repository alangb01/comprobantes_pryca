<?php

namespace App\Console\Commands;

use App\Modulos\Boleteo\NotaCredito;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Storage;
use Carbon\Carbon;

use App\Modulos\Boleteo\Boleta;
use App\Modulos\Boleteo\NotaCredito as NotaCreditoDeBoleta;
use App\Modulos\Facturacion\Factura;
use App\Modulos\Facturacion\NotaCredito as NotaCreditoDeFactura;

class ProcesarComprobantesPendientes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comprobantes:procesar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Procesa los comprobantes pendientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    private $disco;
    private  $bar;

    private function limpiarParaPruebas(){
        \DB::table('persona_boletas')->truncate();
        Storage::disk('local')->deleteDirectory('boleteo');

        \DB::table('empresa_facturas')->truncate();
        Storage::disk('local')->deleteDirectory('facturacion');

        \DB::table('persona_notas_credito')->truncate();
        Storage::disk('local')->deleteDirectory('boleteo/notas_credito');

        \DB::table('empresa_notas_credito')->truncate();
        Storage::disk('local')->deleteDirectory('facturacion/notas_credito');
    }
    public function handle()
    {
        ini_set('memory_limit', '256M');
        $this->procesarArchivosPendientes();
    }

    private $listado_boletas=[];
    private $listado_facturas=[];
    private $listado_notas_credito_boletas=[];
    private $listado_notas_credito_facturas=[];
    private $listado_otros=[];

    public function procesarArchivosPendientes(){
        $this->log('SINCRONIZANDO FTP');
        $this->disco=Storage::disk("ftp");
        $this->conexion=$this->disco->getDriver()->getAdapter()->getConnection();
        $directorio="pryca";
        $listado_pendientes=ftp_nlist($this->conexion, $directorio);//$this->disco->files($directorio);

        $this->log("clasificando archivos");
        $this->separarListados($listado_pendientes);

        sleep(1);
        $this->procesarListadoFacturas();
        sleep(1);
        $this->procesarListadoNotasDeCreditoDeFacturas();
        sleep(1);
        $this->procesarListadoBoletas();
        sleep(1);
        $this->procesarListadoNotasDeCreditoDeBoletas();

        $this->log('FINALIZADO');
    }

    private function procesarListadoBoletas(){
        // $this->info("");

        $total=count($this->listado_boletas);
        // $this->info("total de boletas en ftp: ".$total);
        // $bar = $this->output->createProgressBar($total);
        // $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %memory:6s%');
        // $bar->setRedrawFrequency(ceil($total*0.01));
        // $bar->start();
        // $this->bar=// $bar;

        foreach($this->listado_boletas as $comprobante){
            $this->procesarBoleta($comprobante);
            // $bar->advance();
        }

        // $bar->finish();
        // $this->info("");
    }

    private function procesarListadoNotasDeCreditoDeBoletas(){
        // $this->info("");

        $total=count($this->listado_notas_credito_boletas);
        // $this->info("total de notas de credito de boletas en ftp: ".$total);
        // $bar = $this->output->createProgressBar($total);
        // $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %memory:6s%');
        // $bar->setRedrawFrequency(ceil($total*0.01));
        // $bar->start();
        // $this->bar=// $bar;

        foreach($this->listado_notas_credito_boletas as $comprobante){
            $this->procesarNotaCreditoDeBoleta($comprobante);
            // $bar->advance();
        }

        // $bar->finish();
        // $this->info("");
    }

    private function procesarListadoFacturas(){
        // $this->info("");

        $total=count($this->listado_facturas);
        // $this->info("total de facturas en ftp: ".$total);
        // $bar = $this->output->createProgressBar($total);
        // $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %memory:6s%');
        // $bar->setRedrawFrequency(ceil($total*0.01));
        // $bar->start();
        // $this->bar=// $bar;

        foreach($this->listado_facturas as $comprobante){
            $this->procesarFactura($comprobante);
            // $bar->advance();
        }

        // $bar->finish();
        // $this->info("");
    }

    private function procesarListadoNotasDeCreditoDeFacturas(){
        // $this->info("");

        $total=count($this->listado_notas_credito_facturas);
        // $this->info("total de notas de credito de facturas en ftp: ".$total);
        // $bar = $this->output->createProgressBar($total);
        // $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %memory:6s%');
        // $bar->setRedrawFrequency(ceil($total*0.01));
        // $bar->start();
        // $this->bar=// $bar;

        foreach($this->listado_notas_credito_facturas as $comprobante){
            $this->procesarNotaCreditoDeFactura($comprobante);
            // $bar->advance();
        }

        // $bar->finish();
        // $this->info("");
    }

    private function separarListados($listado_pendientes){
        $listado_pendientes_restantes=[];

        $total=count($listado_pendientes);
        $this->log("total de archivos en ftp: ".$total);
        // $bar = $this->output->createProgressBar($total);
        // $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %memory:6s%');
        // $bar->setRedrawFrequency(ceil($total*0.01));
        // $bar->start();
        // $this->bar=// $bar;
        // El primer recorrido de los pendientes solo buscara boletas o facturas.
        foreach($listado_pendientes as $idb1=>$comprobante){
            if($this->esBoleta($comprobante)){
                $this->listado_boletas[]=$comprobante;
                // $this->bar->advance();
            }else if($this->esFactura($comprobante)){
                $this->listado_facturas[]=$comprobante;
                // $this->bar->advance();
            }else{
                $listado_pendientes_restantes[]=$comprobante;
            }
            unset($listado_pendientes[$idb1]);
        }

        //El segundo recorrido de los pendientes solo recorrera aquellos no identificados como boletas o facturas
        foreach($listado_pendientes_restantes as $idb2=>$comprobante){
            if($this->esNotaCreditoDeBoleta($comprobante)){
                $this->listado_notas_credito_boletas[]=$comprobante;
                // $bar->advance();
            }else if($this->esNotaCreditoDeFactura($comprobante)){
                $this->listado_notas_credito_facturas[]=$comprobante;
                // $bar->advance();
            }else{
                $excepciones[]=$comprobante;
                $this->listado_otros[]=$comprobante;
//                $this->mover($comprobante,"pendientes/excepciones/otros");
                // $bar->advance();
            }
            unset($listado_pendientes_restantes[$idb2]);
        }

        // $bar->finish();
        // $this->info("");
        $this->log("nro facturas          : ".count($this->listado_facturas));
        $this->log("nro n.credito facturas: ".count($this->listado_notas_credito_facturas));
        $this->log("nro boletas           : ".count($this->listado_boletas));
        $this->log("nro n. credito boletas: ".count($this->listado_notas_credito_boletas));
        $this->log("nro otros             : ".count($this->listado_notas_credito_boletas));
        // $this->info("");
    }

//    private function mover($archivo,$carpeta){
//        $nueva_ruta=$carpeta;
//        //agrega / al final de la carpeta si no lo tiene
//        if(substr($nueva_ruta,0,-1)!="/"){
//            $nueva_ruta.="/";
//        }
//        //agrega el nombre del archivo
//        $nueva_ruta.=basename($archivo);
//        //mueve el fichero de su ubicacion original a la nueva en disco seleccionado
//        return $this->disco->move($archivo,$nueva_ruta);
//    }



    private function log($mensaje,$mostrar_fecha_hora=true){
        Log::info($mensaje);
        if($mostrar_fecha_hora){
            $mensaje=$mensaje." -- ".Carbon::now();
        }
        dump($mensaje);
    }

    private function getInfoComprobante($archivo){
        $parametros_archivo = explode('-',$archivo);
        $serie = $parametros_archivo[2];
        $numero = trim(str_replace('.zip',' ',$parametros_archivo[3]));

        return (object)compact('serie','numero','archivo');
    }

    private function esBoleta($archivo){
        return $this->esTipoDocumento($archivo,"B","-03-");
    }

    private function esFactura($archivo){
        return $this->esTipoDocumento($archivo,"F","-01-");
    }

    private function esNotaCreditoDeBoleta($archivo){
        return $this->esTipoDocumento($archivo,"B","-07-");
    }
    private function esNotaCreditoDeFactura($archivo){
        return $this->esTipoDocumento($archivo,"F","-07-");
    }

    private function esTipoDocumento($archivo,$prefijo,$identificador){
        $posicion_prefijo = strpos($archivo,$prefijo );
        $posicion_identificador = strpos($archivo,$identificador);

        return $posicion_prefijo!=false && $posicion_identificador!=false;
    }

    private function procesarBoleta($archivo){
        $comprobante=$this->getInfoComprobante($archivo);
        $boleta = Boleta::where('serie',$comprobante->serie)
            ->where('numero',$comprobante->numero)->first();

        if($boleta instanceof Boleta){
//            try{
//                $boleta->generarArchivos($archivo);
//            }catch(\Exception $e){
////                $this->mover($archivo,"pendientes/duplicados/boletas");
//                $this->log("actualizacion boletas: ".$archivo." -> ".$e->getMessage());
//            }
        }else{
            try{
                //si se copio un nuevo archivo
                if($this->copiarLocalmenteDesdeFtp($archivo)){
                    Boleta::procesarArchivo("pendientes/".$archivo);
                }
            }catch(\Exception $e){
//                $this->mover($archivo,"pendientes/excepciones");
                $this->log("creacion boletas: ".$archivo." -> ".$e->getMessage()." linea ".$e->getLine());
//                $this->log($e->getTraceAsString());

            }
        }
    }

    private function procesarNotaCreditoDeBoleta($archivo)
    {
        $comprobante=$this->getInfoComprobante($archivo);
        $nota_credito_boleta = NotaCreditoDeBoleta::where('serie',$comprobante->serie)
            ->where('numero',$comprobante->numero)->first();

        if($nota_credito_boleta instanceof NotaCreditoDeBoleta){
//            try{
//                $nota_credito_boleta->generarArchivos($archivo);
//            }catch(\Exception $e){
////                $this->mover($archivo,"pendientes/duplicados/boletas-notas-credito");
//                $this->log("actualizacion notas credito boletas: ".$archivo." -> ".$e->getMessage());
//            }
//            $this->mover($archivo,"pendientes/duplicados/boletas-notas-credito");
        }else{
            try{
                if($this->copiarLocalmenteDesdeFtp($archivo)){
                    NotaCreditoDeBoleta::procesarArchivo("pendientes/".$archivo);
                }
//                // $this->info("ok");
            }catch(\Exception $e){
//                $this->mover($archivo,"pendientes/excepciones");
                $this->log("excepcion notas credito boletas: ".$archivo." -> ".$e->getMessage());
            }
        }
    }

    private function procesarFactura($archivo)
    {
        $comprobante=$this->getInfoComprobante($archivo);
        $factura = Factura::where('serie',$comprobante->serie)
            ->where('numero',$comprobante->numero)->first();


        if($factura instanceof Factura){
//            try{
//                $factura->generarArchivos($archivo);
//            }catch(\Exception $e){
////                $this->mover($archivo,"pendientes/duplicados/facturas");
//                $this->log("actualizacion notas credito boletas: ".$archivo." -> ".$e->getMessage());
//            }
        }else{
            try{

                if($this->copiarLocalmenteDesdeFtp($archivo)){
                    Factura::procesarArchivo("pendientes/".$archivo);
                }

//                // $this->info("ok");
            }catch(\Exception $e){
//                $this->mover($archivo,"pendientes/excepciones");
                $this->log("excepcion facturas: ".$archivo." -> ".$e->getMessage());
            }
        }
    }

    private function procesarNotaCreditoDeFactura($archivo)
    {

        $comprobante=$this->getInfoComprobante($archivo);
        $nota_credito_factura = NotaCreditoDeFactura::where('serie',$comprobante->serie)
            ->where('numero',$comprobante->numero)->first();

        if($nota_credito_factura != null){
//            try{
//                $nota_credito_factura->generarArchivos($archivo);
//            }catch(\Exception $e){
////                $this->mover($archivo,"pendientes/duplicados/facturas-notas-credito");
//                $this->log("actualizacion notas credito factura: ".$archivo." -> ".$e->getMessage());
//            }

        }else{
            try{
                if($this->copiarLocalmenteDesdeFtp($archivo)){
                    NotaCreditoDeFactura::procesarArchivo("pendientes/".$archivo);
                }
            }catch(\Exception $e){
//                $this->mover($archivo,"pendientes/excepciones");
                $this->log("excepcion notas credito facturas: ".$archivo." -> ".$e->getMessage());
            }
        }
    }

    public function copiarLocalmenteDesdeFtp($archivo){
//        // $this->info("");
        if( ! Storage::disk('local')->exists("pendientes/".$archivo) &&  ! Storage::disk('local')->exists("pendientes/excepciones/".$archivo)){

            $directorio='pendientes/';
            if(!Storage::disk('local')->exists($directorio)){
                Storage::disk('local')->makeDirectory($directorio);
            }

            $ruta_local=storage_path("app/".$directorio.$archivo);
            $ruta_ftp=$archivo;
            try{
                if (ftp_get($this->conexion, $ruta_local , $ruta_ftp, FTP_BINARY)) {
                    // $this->bar->setMessage("copiado con exito ".$ruta_local);
                    $this->log("copiado con exito ".$ruta_local);
                    return true;
                } else {
//                // $this->bar->setMessage("no pudo copiar ".$ruta_local);
                    $this->log("no pudo copiar ".$ruta_local);
                    return false;
                }
            }catch (\Exception $e){
                dd($e,"MIERDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                return $e->getMessage();
            }

        }else{
//            // $this->bar->setMessage("Ya existe ".$archivo);
            $this->log("Ya existe en disco".$archivo);
            return true;
        }
    }

    private $conexion;

}
