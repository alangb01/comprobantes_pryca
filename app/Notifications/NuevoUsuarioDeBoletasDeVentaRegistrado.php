<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevoUsuarioDeBoletasDeVentaRegistrado extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $usuario;
    private $password;
    private $url_login;

    public function __construct($usuario,$password,$url_login)
    {
        //
        $this->usuario=$usuario;
        $this->password=$password;
        $this->url_login=$url_login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Notificacion de registro en el sistema.')
                    ->line('Se ha creado el usuario:')
                    ->line(' usuario: "'.$this->usuario.'"')
                    ->line(' clave:   "'.$this->password.'"')
                    ->action('Panel para usuarios  ', $this->url_login)
                    ->line('Si presenta algun inconveniente por favor contactenos.')
                    ->line('Gracias por usar nuestra aplicación');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
