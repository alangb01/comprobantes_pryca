<?php 
namespace App\Modulos;
use Carbon\Carbon;
use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\Route;

class Util
{
	
	public static function camposOrdenados($campos,$params_get,$params_url){
        $campos_orden=collect($campos);

        $ruta=Route::currentRouteName();
            

        $separador="-";
        $orden_seteado=explode($separador,$params_get['ordenar']);
        foreach ($campos_orden as $id => $campo) {
            $orden='asc';
            if (isset($orden_seteado[0]) && isset($orden_seteado[1]) && $orden_seteado[0]==$campo) {
                $orden_asignado=$orden_seteado[1];
                if ($orden_asignado=='asc') { $orden='desc';}
                elseif($orden_asignado=="desc"){ $orden=null;}
                else{   $orden='asc'; unset($params_get['ordenar']);}
            }

            $params_get_interno=$params_get;
            $params_get_interno['ordenar']=$campo.$separador.$orden;
            $campo_actual=array('url'=>route($ruta,array_merge($params_url,$params_get_interno)),'orden'=>$orden);

            // dd($campo_actual);
            $campos_orden[$campo]=\View::make('admin.comun.campo-orden')->with('campo',$campo_actual);
            unset($campos_orden[$id]);
        }

        return $campos_orden;
    }

    public static function mesToString($numero_mes){
        $meses=['enero','febrero','marzo','abril','mayo','junio','julio','agosto','setiembre','octubre','noviembre','diciembre'];
        return $meses[$numero_mes-1];
    }

    public static function extraerXmlDeZip($ruta_zip, $reemplazo){
        $archivo_xml=str_replace(".zip", ".xml", basename($ruta_zip));

        $comprimido = new Zipper();
        $comprimido->make(storage_path( 'app/'.$ruta_zip));
        $contenido=$comprimido->getFileContent($archivo_xml);

        //Por mientras
        $content = str_replace($reemplazo,' ',
            $contenido);

        $comprimido->close();
        return simplexml_load_string($content,null,true);
    }







}