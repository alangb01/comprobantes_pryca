<?php

namespace App\Modulos\Boleteo;

use App\Modulos\Facturacion\Empresa;
use App\Modulos\Util;
use App\NumeroALetras;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class NotaCredito extends Model
{
    //
    const EXCEPCION_CREAR = 1;
    const EXCEPCION_ACTUALIZAR = 2;

    const CONCEPTO_TOTAL_OPERACIONES_GRAVADAS = 1001;
    const CONCEPTO_TOTAL_OPERACIONES_INAFECTAS = 1002;
    const CONCEPTO_TOTAL_OPERACIONES_EXONERADAS = 1003;
    const CONCEPTO_TOTAL_OPERACIONES_GRATUITAS = 1004;
    const CONCEPTO_SUBTOTAL_VENTA = 1005;
    const CONCEPTO_PERCEPCIONES = 2001;
    const CONCEPTO_RETENCIONES = 2002;
    const CONCEPTO_DETRACCIONES = 2003;
    const CONCEPTO_BONIFICACIONES = 2004;
    const CONCEPTO_TOTAL_DESCUENTOS= 2005;
    const CONCEPTO_FISE = 3001;

    protected $table = 'persona_notas_credito';

    protected $dates = ['fecha_emision'];

    protected function boleta(){
        return $this->belongsTo(Boleta::class,'id_boleta','id');
    }

    public function persona(){
        return $this->belongsTo(Persona::class,'id_persona','id');
    }

    public function scopeCargarListado($query,$params=array()){
        $query->orderBy('fecha_emision','desc');
        $query->orderBy('serie','desc');
        $query->orderBy('numero','desc');

        if(isset($params['id_persona']) && $params['id_persona']!=""){
            $query->where('id_persona',$params['id_persona']);
        }


        if (isset($params['buscar']) && $params['buscar']!="" ) {
            $query->where(function($subquery) use ($params){
                $persona=Persona::where('dni',$params['buscar'])->first();
                if($persona instanceof Persona){
                    $subquery->where('id_persona',$persona->id);
                }
                // $subquery->where('empresas.ruc','like','%'.$params['buscar'].'%');
                // $subquery->orWhere('empresas.razon_social','like','%'.$params['buscar'].'%');
                $subquery->orWhere('serie','like','%'.$params['buscar'].'%');
                $subquery->orWhere('numero','like','%'.$params['buscar'].'%');

            });
            return $query;
        }
        return $query;
	}

    public function scopeBuscar($query,$dni,$serie,$numero){

        if($dni!=""){
            $persona=Persona::where('dni',$dni)->first();
            if($persona instanceof Persona){
                $query->where('id_persona',$persona->id);
            }else{
                $query->where('id_persona',-1);
            }
        }

        if($serie!=""){
            $query->where('serie',$serie);

        }
        if($numero!="") {
            $query->where('numero', $numero);
        }

        //dump($query->toSql(),$query->getBindings());

        return $query;
    }

    public function scopeBuscarPorNumeracion($query,$serie,$numero){
         $query->orderBy('fecha_emision','desc');
        $query->where('serie',$serie)->where('numero',$numero);
        $query->orderBy('serie','desc')->orderBy('numero','desc');
        //echo $query->toSql();
        return $query;
    }
//
//    public function scopeBuscarPorPersonaODni($query,$dni,$serie,$numero){
//        $query->select('persona_notas_credito.id','persona_notas_credito.serie','persona_notas_credito.numero',
//            'persona_notas_credito.importe','persona_notas_credito.fecha_emision','persona_notas_credito.ruta_zip',
//            'persona_notas_credito.ruta_pdf','persona_notas_credito.ruta_excel', 'persona_notas_credito.id_boleta as id_boleta',
//            'personas.nombre as persona_nombre');
//        $query->join('personas','persona_notas_credito.id_persona','=','personas.id');
//        $query->where(function($subquery) use ($dni,$serie,$numero){
//            if($serie != null && $numero != null){
//                $subquery->where('serie',$serie);
//                $subquery->where('numero',$numero);
//            }
//
//            if($dni != null){
//                $subquery->orWhere('personas.dni',$dni);
//            }
//        });
//        $query->orderBy('fecha_emision','desc');
//        $query->orderBy('serie','desc')->orderBy('numero','desc');
//        return $query;
//    }

	const CONCEPTO_ANULACION_POR_OPERACION=1;
	const CONCEPTO_ANULACION_POR_ERROR=2;
	const CONCEPTO_DESCUENTO_GLOBAL=3;
	const CONCEPTO_DEVOLUCION_TOTAL=4;
	const CONCEPTO_CORRECCION_POR_ERROR=5;
	const CONCEPTO_DEVOLUCION_POR_ITEM=6;
	const CONCEPTO_DESCUENTO_POR_ITEM=7;

	public static function getConceptos(){
		$conceptos=collect();
		$conceptos->push((object)array('id'=>self::CONCEPTO_ANULACION_POR_OPERACION,'nombre'=>'Anulación de la operación'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_ANULACION_POR_ERROR,'nombre'=>'Anulación por error en el RUC'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_DESCUENTO_GLOBAL,'nombre'=>'Descuento global'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_DEVOLUCION_TOTAL,'nombre'=>'Devolución total'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_CORRECCION_POR_ERROR,'nombre'=>'Correccion por error en la descripción'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_DEVOLUCION_POR_ITEM,'nombre'=>'Devolución por item'));
		$conceptos->push((object)array('id'=>self::CONCEPTO_DESCUENTO_POR_ITEM,'nombre'=>'MERCADERIA'));
		return $conceptos;
	}

    public static function getConceptosAnulacion(){
        $conceptos=collect();
        $conceptos->push((object)array('id'=>self::CONCEPTO_ANULACION_POR_OPERACION,'nombre'=>'Anulación de la operación'));
        $conceptos->push((object)array('id'=>self::CONCEPTO_ANULACION_POR_ERROR,'nombre'=>'Anulación por error en el RUC'));
        // $conceptos->push((object)array('id'=>self::CONCEPTO_DESCUENTO_GLOBAL,'nombre'=>'Descuento global'));
        $conceptos->push((object)array('id'=>self::CONCEPTO_DEVOLUCION_TOTAL,'nombre'=>'Devolución total'));
        $conceptos->push((object)array('id'=>self::CONCEPTO_CORRECCION_POR_ERROR,'nombre'=>'Correccion por error en la descripción'));
        // $conceptos->push((object)array('id'=>self::CONCEPTO_DEVOLUCION_POR_ITEM,'nombre'=>'Devolución por item'));
        // $conceptos->push((object)array('id'=>self::CONCEPTO_DESCUENTO_POR_ITEM,'nombre'=>'Descuento por item'));
         $conceptos=collect();
        $conceptos->push(self::CONCEPTO_ANULACION_POR_OPERACION);
        $conceptos->push(self::CONCEPTO_ANULACION_POR_ERROR);
        $conceptos->push(self::CONCEPTO_DEVOLUCION_TOTAL);
        $conceptos->push(self::CONCEPTO_CORRECCION_POR_ERROR);
        return $conceptos->toArray();
        
    }

	public function conceptoToString(){
        $id_concepto_actual=$this->id_concepto;
        $conceptos_disponibles=self::getConceptos();
        $indice=$conceptos_disponibles->search(function ($concepto) use ($id_concepto_actual){
            return $concepto->id==$id_concepto_actual;
        });
        
        if($indice!==false){
            return $conceptos_disponibles->get($indice)->nombre;
        }

        return "no especificado";
    }

    public static function getConcepto($nombre){
        $conceptos_disponibles=self::getConceptos();
        $indice=$conceptos_disponibles->search(function ($concepto) use ($nombre){
            return strtolower($concepto->nombre)==strtolower($nombre);
        });
        
        if($indice!==false){
            return $conceptos_disponibles->get($indice);
        }

        return null;
    }

    public function descargarZip(){
        return response()->download(storage_path("app/".$this->ruta_zip));
    }

    public function descargarExcel(){
        $data_comprobante=self::extraerInfoNotaCreditoDesdeXML($this->ruta_zip);
        try{
            $this->generarExcel($data_comprobante);
            return response()->download(storage_path("app/".$this->ruta_excel))
                ->deleteFileAfterSend(true);
        }catch(\Exception $e){
            return response()->make("No se pudo generar el excel. ".$e->getMessage());
        }
    }

    public function descargarPdf(){
        $data_comprobante=self::extraerInfoNotaCreditoDesdeXML($this->ruta_zip);
        try{
            $this->generarPDF($data_comprobante);
            return response()->download(storage_path("app/".$this->ruta_pdf))
                ->deleteFileAfterSend(true);
        }catch(\Exception $e){
            return response()->make("No se pudo generar el pdf. ".$e->getMessage());
        }
    }

    public function generarArchivos($archivo=null){
        $numero_modificaciones=0;
        $disco=Storage::disk('local');
        //SI HAY ARCHIVO

        if(isset($archivo)){
            $data_comprobante=Boleta::extraerInfoBoletaDesdeXML($archivo);
            $this->generarZip($archivo);
            $numero_modificaciones++;
        }elseif($this->ruta_zip!="" && strpos($this->ruta_zip,"pendientes")==false && $disco->exists($this->ruta_zip)){
            $data_comprobante=Boleta::extraerInfoBoletaDesdeXML($this->ruta_zip);
        }else{
            throw new \Exception("Zip no encontrado".$archivo);
        }

        if($this->ruta_pdf=="" || !$disco->exists($this->ruta_pdf)){
            $this->generarPDF($data_comprobante);
            $numero_modificaciones++;
        }

        if($this->ruta_excel!="" || !$disco->exists($this->ruta_excel)){
            $this->generarExcel($data_comprobante);
            $numero_modificaciones++;
        }

        if($numero_modificaciones>0){
            $this->save();
        }
    }

    public static function registrarDesdeDatosXml($data_comprobante,$archivo){
        $ruc_permitido=env('RUC_EMPRESA','20524310857');
        if ($data_comprobante['data_emisor']['ruc']!=env('RUC_EMPRESA','20524310857')) {
            throw new \Exception('El emisor '.$ruc_permitido.' no habilitado, solo se permiten para el ruc :'.$data_comprobante['data_emisor']['ruc']);
        }

        $persona=Persona::buscarPorDni($data_comprobante['data_persona']['dni']);
        if($persona instanceof Persona){

        }elseif(isset($data_comprobante['data_persona']['dni']) && strlen($data_comprobante['data_persona']['dni'])==8){
            $persona=Persona::registrarDesdeDatosXml($data_comprobante['data_persona']);
        }else{
            $persona=new Persona();
            $persona->id=0;
//            throw new \Exception("No se ha especificado datos de una persona");
        }

        $concepto=NotaCredito::getConcepto($data_comprobante['concepto']);
        if (isset($concepto)) {

        }elseif($data_comprobante['concepto']!=""){
            $concepto=(object)array('id'=>0,'nombre'=>$data_comprobante['concepto']);
        }else{
            throw new \Exception("El concepto ncb  no reconocido: id -> ".$data_comprobante['concepto']);
        }

        $boleta=Boleta::buscarPorNumeracion($data_comprobante['serie_boleta'],$data_comprobante['numero_boleta'])->first();
//        if (!($boleta instanceof Boleta)) {
//            throw new \Exception("Boleta no encontrada para la nota de credito ".$data_comprobante['serie']."-".$data_comprobante['numero']);
//        }
        if(!isset($boleta)){
            $boleta=new Boleta();
            $boleta->id=0;
        }

        $nota_credito=new NotaCredito();
        $nota_credito->serie=$data_comprobante['serie'];
        $nota_credito->numero=$data_comprobante['numero'];
        $nota_credito->fecha_emision=$data_comprobante['fecha_emision'];
        $nota_credito->id_persona=$persona->id;
        $nota_credito->id_boleta=$boleta->id;
        // $nota_credito->id_cliente=$data_comprobante['id_'];
        $nota_credito->id_concepto=$concepto->id;
//        if($concepto->id==0){
            $nota_credito->concepto=$concepto->nombre;
//        }
        $nota_credito->importe=$data_comprobante['importe'];

        $nota_credito->estado='AC';

        //Generar pdf
//        $nota_credito->generarPDF($data_comprobante);
        //Generar Excel
//        $nota_credito->generarExcel($data_comprobante);
        //Generar Zip
        $nota_credito->generarZIP($archivo);

        $nota_credito->save();
        return $nota_credito;
    }

    //procesar XML
    private static function extraerInfoNotaCreditoDesdeXML($archivo){
        $xml=Util::extraerXmlDeZip($archivo,
            'xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 ..\xsd\maindoc\UBLPE-CreditNote-1.0.xsd"');


        $ns = $xml->getNamespaces(true);
        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
        $serie_numero_boleta = explode("-",$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->ReferenceID->__toString());
//        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();
        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();
//            ." ".$xml->children($ns['cbc'])->IssueTime->__toString();
        $fecha_vencimiento = $fecha_emision;//$xml->children($ns['cbc'])->DueDate->__toString();

        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
        $serie=$serie_numero[0];
        $numero=$serie_numero[1];

        $serie_boleta=$serie_numero_boleta[0];
        $numero_boleta=$serie_numero_boleta[1];


        $label = $xml->children($ns['ext'])->UBLExtensions->children($ns['ext'])[0]
            ->ExtensionContent->children($ns['sac'])
            ->AdditionalInformation->children($ns['sac']);

        //
        $gravadas = 0.00; //$label->AdditionalMonetaryTotal[0]->children($ns['cbc'])->PayableAmount->__toString();
        $inafectas = 0.00; //$label->AdditionalMonetaryTotal[1]->children($ns['cbc'])->PayableAmount->__toString();
        $exoneradas = 0.00; //$label->AdditionalMonetaryTotal[2]->children($ns['cbc'])->PayableAmount->__toString();
        $gratuitas = 0.00;
        $subtotal = 0.00;
        $percepciones = 0.00;
        $retenciones = 0.00;
        $detracciones = 0.00;
        $bonificaciones = 0.00;
        $descuentos = 0.00;
        $fise = 0.00;

        foreach($label->AdditionalMonetaryTotal as $id=>$bloque){
            if($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_GRAVADAS) {
                $gravadas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_INAFECTAS){
                $inafectas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_EXONERADAS){
                $exoneradas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_GRATUITAS){
                $gratuitas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_SUBTOTAL_VENTA){
                $subtotal = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_PERCEPCIONES){
                $percepciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_RETENCIONES){
                $retenciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_DETRACCIONES){
                $detracciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_BONIFICACIONES){
                $bonificaciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_DESCUENTOS){
                $descuentos = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_FISE){
                $fise = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }
        }

        $totales = compact('gravadas','inafectas','exoneradas','gratuitas','subtotal',
            'percepciones','retenciones','detracciones','bonificaciones','descuentos',
            'fise');

        $data_persona=Persona::extraerInfoPersona($xml,$ns);

        $concepto=$xml->children($ns['cac'])->DiscrepancyResponse->children($ns['cbc'])->Description->__toString();
        $data_emisor=Empresa::extraerInfoEmisor($xml,$ns);
        $data_producto = self::extraerInfoProductos($xml,$ns);


        $data=compact('serie','numero','serie_boleta','numero_boleta','fecha_emision','fecha_vencimiento','importe','concepto');
        $data_array=compact('data_persona','data_emisor','data_producto','totales');
        $data=array_map("trim",$data);
        $data=array_merge($data,$data_array);

        $data['ruta_zip']=$archivo;
        // dd($data);
        return $data;
    }

    private static function extraerInfoProductos($xml,$ns){
        $contador = count($xml->children($ns['cac'])->CreditNoteLine);
        //dd($contador);
        $listado_productos = array();
        for ($i = 0; $i < $contador; $i++){
            $label = $xml->children($ns['cac'])->CreditNoteLine[$i];

            $DescripcionProducto=$label->children($ns['cac'])->Item->children($ns['cbc'])->Description->__toString();

            $CantidadUnidad=$label->children($ns['cbc'])->CreditedQuantity->__toString();

            $TipoCantidad = $label->children($ns['cbc'])->CreditedQuantity->attributes()->unitCode->__toString();//UNIDAD DE MEDIDA

            $PrecioVentaUnitario = $label->children($ns['cac'])->Price->children($ns['cbc'])->PriceAmount->__toString();

            $listado_productos[$i] = [ 'descripcion_producto' => trim($DescripcionProducto),
                'cantidadUnidad' => trim($CantidadUnidad),
                'tipoCantidad' => trim($TipoCantidad),
                'PrecioVentaUnitario' => trim($PrecioVentaUnitario)];
            //'PrecioVentaTotal' => trim($PrecioVentaTotal)];
        }

        $data=compact('listado_productos');

        return $data;

    }

    public static function procesarArchivo($archivo){
        $data_comprobante=NotaCredito::extraerInfoNotaCreditoDesdeXML($archivo);

        //


        $nota_credito=NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
        if($nota_credito instanceof NotaCredito){
//            try{
//                $nota_credito->generarArchivos($archivo);
//            }catch(\Exception $e){
//                throw new \Exception($e->getMessage(),self::EXCEPCION_ACTUALIZAR);
//            }
        }else{
            try{
                NotaCredito::registrarDesdeDatosXml($data_comprobante,$archivo);
            }catch(\Exception $e){
                throw new \Exception($e->getMessage(),self::EXCEPCION_CREAR);
            }

        }

//        $nota_credito=NotaCredito::registrarDesdeDatosXml($data_comprobante);

    }

    private function generarRuta($tipo_archivo, $fecha_emision)
    {
        $fecha =$fecha_emision;
        $anio = $fecha->year;
        $mes = Util::mesToString($fecha->month);
        $dia = $fecha->day;
//
        $directorio = "boletas-notas-credito/".$tipo_archivo."/".$anio."/".$mes."/".$dia."/";
        if(!Storage::disk('local')->exists($directorio)){
            Storage::disk('local')->makeDirectory($directorio);
        }

        return $directorio;
    }

    public function generarPDF($data_comprobante){

        $sumatoria = 0.00;
        $igv = $data_comprobante['importe'] - $data_comprobante['totales']['gravadas'];
        $data = [
            'codigo_factura' => $data_comprobante['serie'] . "-" . $data_comprobante['numero'] ,
            'serie_boleta' => $data_comprobante['serie_boleta'],
            'numero_boleta' => $data_comprobante['numero_boleta'],
            'establecimiento' => $data_comprobante['data_emisor']['direccion'],
            'nombre_empresa' =>  $data_comprobante['data_emisor']['razon_social'],
            'ruc_empresa' => $data_comprobante['data_emisor']['ruc'],
            'fecha_emision' => $data_comprobante['fecha_emision'],
            'fecha_vencimiento' => $data_comprobante['fecha_vencimiento'],
            'razon_social' => $data_comprobante['data_persona']['nombre'],
            'ruc' => $data_comprobante['data_persona']['dni'],
            'tipo_moneda' => 'PEN',
            'sumatoria_descuentos' => $sumatoria,
            'importe_total' =>  $data_comprobante['importe'],
            'concepto' =>  $data_comprobante['concepto'],
            'total_venta_gravada' => $data_comprobante['totales']['gravadas'],
            'total_venta_exonerada' => $data_comprobante['totales']['exoneradas'],
            'total_venta_inafecta'  => $data_comprobante['totales']['inafectas'],
            'sumatoria_IGV' => $igv,
            'nombre_documento' => 'NOTA DE CREDITO',
            'numero' => $data_comprobante['numero'],
            'serie' => $data_comprobante['serie']
            //'direccion_persona' => $data_comprobante['data_persona']['direccion_persona']
        ];
        $temp = $data_comprobante['data_producto']['listado_productos'];

        if($data != null && $temp != null)
        {
            //Convertir de numeros a letras
            $numero_letras = NumeroALetras::convertir(number_format(floatval($data['importe_total']),2),'','CENTIMOS');
            //Agregar todo los datos al invoice
            $view = \View::make('admin.pdf.nota_credito.comprobante', compact('data','temp','numero_letras'))->render();
            $dompdf = \App::make('dompdf.wrapper');
            $dompdf->loadHTML($view);

            //generar ruta
            $directorio=$this->generarRuta("pdf",$this->fecha_emision);

            $this->ruta_pdf = $directorio . str_replace(".zip",".pdf",basename($data_comprobante['ruta_zip']));

            Storage::disk('local')->put($this->ruta_pdf, $dompdf->output());
        }else{
            throw new \Exception("No se pudo generar el pdf");
        }

    }

    public function generarExcel($data_comprobante){
//        dd($data_comprobante);
        $directorio=$this->generarRuta("excel",$this->fecha_emision);
        $productos = $data_comprobante['data_producto']['listado_productos'];
        $nombre_excel=str_replace(".zip","",basename($data_comprobante['ruta_zip']));
        Excel::create($nombre_excel ,function($excel) use ($productos,$data_comprobante) {
            $excel->sheet('Nota-Credito' ,function($sheet) use ($productos, $data_comprobante) {
//                dd($data_comprobante);
                $razon_social = $data_comprobante['data_persona']['nombre'];
                $ruc = $data_comprobante['data_persona']['dni'];
                $importe_total =  $data_comprobante['importe'];
                $total_venta_gravada = $data_comprobante['totales']['gravadas'];
                $total_venta_exonerada = $data_comprobante['totales']['exoneradas'];
                $total_venta_inafecta  = $data_comprobante['totales']['inafectas'];
                $total_igv = $igv = $data_comprobante['importe'] - $data_comprobante['totales']['gravadas'];
                $motivo = $data_comprobante['concepto'];

                //ESTILOS
                $sheet->cell('A1:J20', function($cells) {
                    $cells->setAlignment('center');
                });
                $contador = count($productos);
                $sheet->setBorder('A6:E'. ($contador + 6), 'thin');
                $sheet->setBorder('D'.(8 + $contador) .':E'. ($contador + 12), 'thin');
                //PARTE IZQUIERDA CABEZERA
                $sheet->cell('A1', function($cell) use ($data_comprobante) {
                    $nombre_empresa = $data_comprobante['data_emisor']['razon_social'];
                    $cell->setValue($nombre_empresa);
                });
                $sheet->cell('A2', function($cell){
                    $cell->setValue('DNI o RUC: ');
                });
                $sheet->cell('B2', function($cell) use ($ruc) {
                    $cell->setValue($ruc);
                });
                $sheet->cell('A3', function($cell){
                    $cell->setValue('Nombre o Razón Social: ');
                });
                $sheet->cell('B3', function($cell) use ($razon_social) {
                    $cell->setValue($razon_social);
                });
                $sheet->cell('A4', function($cell){
                    $cell->setValue('Motivo: ');
                });
                $sheet->cell('B4', function($cell) use ($motivo) {
                    $cell->setValue($motivo);
                });
                //PARTE DERECHA CABEZERA
                $sheet->cell('E1', function($cell) {
                    $cell->setValue('NOTA DE CRÉDITO ELECTRÓNICA');
                });
                $sheet->cell('E2', function($cell) {
                    $cell->setValue(env('RUC_EMPRESA'));
                });
                $sheet->cell('E3', function($cell) use ($data_comprobante) {
                    $codigo_factura = $data_comprobante['serie'] . "-" . $data_comprobante['numero'];
                    $cell->setValue($codigo_factura);
                });
                $sheet->cell('E4', function($cell) use ($data_comprobante) {
                    $fecha = $data_comprobante['fecha_emision'];
                    $cell->setValue('Fecha: '. $fecha);
                });
                //PARTE TABLA DE PRODUCTOS
                $sheet->fromArray($productos, null, 'A6',true);
                //PARTE TABLA TOTAL

                $sheet->cell('D'.(8 + $contador), function($cell) {
                    $cell->setValue('Total Gravadas:');
                });
                $sheet->cell('D'.(9 + $contador), function($cell) {
                    $cell->setValue('Total Inafectas:');
                });
                $sheet->cell('D'.(10 + $contador), function($cell) {
                    $cell->setValue('Total Exoneradas:');
                });
                $sheet->cell('D'.(11 + $contador), function($cell) {
                    $cell->setValue('Total IGV:');
                });
                $sheet->cell('D'.(12 + $contador), function($cell) {
                    $cell->setValue('Importe Total:');
                });

                $sheet->cell('E'.(8 + $contador), function($cell) use($total_venta_exonerada) {
                    $cell->setValue($total_venta_exonerada);
                });
                $sheet->cell('E'.(9 + $contador), function($cell) use ($total_venta_inafecta) {
                    $cell->setValue($total_venta_inafecta);
                });
                $sheet->cell('E'.(10 + $contador), function($cell) use ( $total_venta_gravada) {
                    $cell->setValue( $total_venta_gravada);
                });
                $sheet->cell('E'.(11 + $contador), function($cell) use ($total_venta_gravada){
                    $cell->setValue($total_venta_gravada);
                });
                $sheet->cell('E'.(12 + $contador), function($cell) use ($importe_total){
                    $cell->setValue($importe_total);
                });
            });
        })->store('xls',storage_path("app/".$directorio));


        $this->ruta_excel = $directorio.$nombre_excel. ".xls";
    }

    public function generarZip($archivo){

        $directorio=$this->generarRuta("zip",$this->fecha_emision);
        $nueva_ruta=$directorio.basename($archivo);

        if(!Storage::disk("local")->has($nueva_ruta)){
            Storage::disk("local")->move($archivo, $nueva_ruta);
        }
        $this->ruta_zip=$nueva_ruta;
    }
}
