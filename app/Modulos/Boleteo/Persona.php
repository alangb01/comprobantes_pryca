<?php

namespace App\Modulos\Boleteo;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table = 'personas';

    public function scopeCargarListado($query,$params=array()){
         if (isset($params['buscar']) && $params['buscar']!="" ) {
            $query->where(function($subquery) use ($params){
                $subquery->where('dni','like','%'.$params['buscar'].'%');
                $subquery->orWhere('nombre','like','%'.$params['buscar'].'%');
                // $subquery->orWhere('direccion','like','%'.$params['buscar'].'%');

            });
        }

        $query->orderBy('nombre','asc');

		return $query;
	}

    public function scopeBuscarPorDni($query,$dni){
        return $query->where('dni',$dni)->orderBy('nombre')->first();
    }

	public function boletas(){
		return $this->hasMany(Boleta::class,'id_persona','id');
	}

 

    public function cuentas(){
        return $this->hasMany(Usuario::class,'id_persona','id');
    }

    public function __toString(){
        return $this->apellido." ".$this->nombre;
    }

    //
    public static function registrarDesdeDatosXml($data){
//        dd($data);
        $persona=new Persona();
        $persona->dni=$data['dni'];
        $persona->nombre=$data['nombre'];
        $persona->estado='AC';
        $persona->save();
        return $persona;
    }

    //procesar XML
    public static function extraerInfoPersona($xml,$ns){
        //Para boletas y nota de credito de boleta
        $item=$xml->children($ns['cac'])->AccountingCustomerParty;

        $dni=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
        $nombre=$item
            ->children($ns['cac'])->Party
            ->children($ns['cac'])->PartyLegalEntity
            ->children($ns['cbc'])->RegistrationName->__toString();

        $direccion=$item->children($ns['cac'])->Party
            ->children($ns['cac'])->PostalAddress->__toString();

        $data=compact('dni','nombre','direccion');

        return array_map("trim", $data);
    }



}
