<?php

namespace App\Modulos\Boleteo;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'persona_usuarios';

    public function scopeCargarListado($query,$params){
        if (isset($params['id_persona']) && $params['id_persona']!="") {
            $query->where('id_persona',$params['id_persona']);
        }

        return $query;
    }

    public function persona(){
        return $this->belongsTo(Persona::class,'id_persona','id');
    }

    public function __toString(){
        return $this->usuario;
    }
}
