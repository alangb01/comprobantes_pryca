<?php

namespace App\Modulos\Facturacion;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'empresa_usuarios';

    public function scopeCargarListado($query,$params){
        if (isset($params['id_empresa']) && $params['id_empresa']!="") {
            $query->where('id_empresa',$params['id_empresa']);
        }

        return $query;
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class,'id_empresa','id');
    }

    public function __toString(){
        return $this->usuario;
    }
}
