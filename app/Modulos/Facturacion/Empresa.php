<?php

namespace App\Modulos\Facturacion;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table="empresas";

    public function scopeCargarListado($query,$params=array()){
         if (isset($params['buscar']) && $params['buscar']!="" ) {
            $query->where(function($subquery) use ($params){
                $subquery->where('ruc','like','%'.$params['buscar'].'%');
                $subquery->orWhere('razon_social','like','%'.$params['buscar'].'%');
                $subquery->orWhere('direccion','like','%'.$params['buscar'].'%');

            });
        }

        $query->orderBy('ruc','desc');
        $query->orderBy('razon_social','desc');

		return $query;
	}

	public function facturas(){
		return $this->hasMany(Factura::class,'id_empresa','id');
	}

    public function scopeBuscarPorRuc($query,$ruc){
    	return $query->where('ruc',$ruc)->orderBy('razon_social')->first();
    }

    public function cuentas(){
        return $this->hasMany(Usuario::class,'id_empresa','id');
    }

    public function __toString(){
        return $this->razon_social;
    }

    public static function registrarDesdeDatosXml($data){
        $empresa=new Empresa();
        $empresa->ruc=$data['ruc'];
        $empresa->razon_social=$data['razon_social'];
        $empresa->direccion=isset($data['direccion'])?$data['direccion']:'--';
        $empresa->estado='AC';
        $empresa->save();
        return $empresa;
    }

    public static function extraerInfoEmisor($xml,$ns){
        //Para boletas y nota de credito de boleta
        $item=$xml->children($ns['cac'])->AccountingSupplierParty;

        $ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
        $razon_social=$item
            ->children($ns['cac'])->Party
            ->children($ns['cac'])->PartyLegalEntity
            ->children($ns['cbc'])->RegistrationName->__toString();



        $direccion_min=$item
            ->children($ns['cac'])->Party
            ->children($ns['cac'])->PostalAddress
            ->children($ns['cbc']);

        $direccion=$direccion_min->StreetName->__toString()
            ."".$direccion_min->District->__toString()
            ." ".$direccion_min->CityName->__toString()
            ." ".$direccion_min->CountrySubentity->__toString()."";

        $data=compact('ruc','razon_social','direccion');

        return array_map("trim", $data);
    }

    public static function extraerInfoEmpresa($xml,$ns){

        $item=$xml->children($ns['cac'])->AccountingCustomerParty;

        $ruc=$item->children($ns['cbc'])->CustomerAssignedAccountID->__toString();
        $razon_social=$item
            ->children($ns['cac'])->Party
            ->children($ns['cac'])->PartyLegalEntity
            ->children($ns['cbc'])->RegistrationName->__toString();


        $direccion="";

//        dd($item
//            ->children($ns['cac'])->Party,
//            $item
//                ->children($ns['cac'])->PostalAddress->__toString());
        try{
            if(($item
                    ->children($ns['cac'])->PostalAddress)!=null
                || $item
                    ->children($ns['cbc'])->StreetName->__toString()!=null
            ){
                $direccion_min=$item
                    ->children($ns['cac'])->Party
                    ->children($ns['cac'])->PostalAddress
                    ->children($ns['cbc']);
                $direccion=$direccion_min->StreetName->__toString();

            }

        }catch (\Exception $e){

        }

        $data=compact('ruc','razon_social','direccion');

        return array_map("trim", $data);
    }
}
