<?php

namespace App\Modulos\Facturacion;

use Illuminate\Database\Eloquent\Model;

class NotaDebito extends Model
{
    //
    protected $table = "empresa_notas_debito";

    protected $dates = ['fecha_emision'];
    
    protected function factura(){
        return $this->belongsTo(Factura::class,'id_factura','id');
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class,'id_empresa','id');
    }

    public function scopeBuscarPorNumeracion($query,$serie,$numero){
    	$query->where('serie',$serie)->where('numero',$numero);
    	$query->orderBy('serie','desc')->orderBy('numero','desc');

    	return $query;
    }

     public function scopeCargarListado($query,$params=array()){
        $query->orderBy('serie','desc');
        $query->orderBy('numero','desc');

        if(isset($params['id_empresa']) && $params['id_empresa']!=""){
            $query->where('id_empresa',$params['id_empresa']);
        }


        if (isset($params['buscar']) && $params['buscar']!="" ) {
            $query->where(function($subquery) use ($params){
                // $subquery->where('empresas.ruc','like','%'.$params['buscar'].'%');
                // $subquery->orWhere('empresas.razon_social','like','%'.$params['buscar'].'%');
                $subquery->orWhere('serie','like','%'.$params['buscar'].'%');
                $subquery->orWhere('numero','like','%'.$params['buscar'].'%');

            });
            return $query;
        }
        return $query;
	}

    const CONCEPTO_INTERES_POR_MORA=101;
    const CONCEPTO_AUMENTO_EN_EL_vALOR=102;

    public static function getConceptos(){
        $conceptos=collect();
        $conceptos->push((object)array('id'=>self::CONCEPTO_INTERES_POR_MORA,'nombre'=>'Interes por mora'));
        $conceptos->push((object)array('id'=>self::CONCEPTO_AUMENTO_EN_EL_vALOR,'nombre'=>'Aumento en el valor'));
        return $conceptos;
    }

    public static function getConceptosAnulacion(){
        $conceptos=collect();
        return $conceptos;
    }

    public function conceptoToString(){
        $id_concepto_actual=$this->id_concepto;
        $conceptos_disponibles=self::getConceptos();
        $indice=$conceptos_disponibles->search(function ($concepto) use ($id_concepto_actual){
            return $concepto->id==$id_concepto_actual;
        });
        
        if($indice!==false){
            return $conceptos_disponibles->get($indice)->nombre;
        }

        return "no especificado";
    }

    public static function getConcepto($nombre){
        $conceptos_disponibles=self::getConceptos();
        $indice=$conceptos_disponibles->search(function ($concepto) use ($nombre){
            return strtolower($concepto->nombre)==strtolower($nombre);
        });
        
        if($indice!==false){
            return $conceptos_disponibles->get($indice);
        }

        return null;
    }
}
