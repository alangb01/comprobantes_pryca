<?php

namespace App\Modulos\Facturacion;

use App\Modulos\Util;
use App\NumeroALetras;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class Factura extends Model
{
    //
    const ESTADO_EMITIDO='EM';
    const ESTADO_MODIFICADO='MO';
    const ESTADO_ANULADO='AN';

    const EXCEPCION_CREAR = 1;
    const EXCEPCION_ACTUALIZAR = 2;

    const CONCEPTO_TOTAL_OPERACIONES_GRAVADAS = 1001;
    const CONCEPTO_TOTAL_OPERACIONES_INAFECTAS = 1002;
    const CONCEPTO_TOTAL_OPERACIONES_EXONERADAS = 1003;
    const CONCEPTO_TOTAL_OPERACIONES_GRATUITAS = 1004;
    const CONCEPTO_SUBTOTAL_VENTA = 1005;
    const CONCEPTO_PERCEPCIONES = 2001;
    const CONCEPTO_RETENCIONES = 2002;
    const CONCEPTO_DETRACCIONES = 2003;
    const CONCEPTO_BONIFICACIONES = 2004;
    const CONCEPTO_TOTAL_DESCUENTOS= 2005;
    const CONCEPTO_FISE = 3001;


    protected $table = "empresa_facturas";
    protected $dates = ['fecha_emision']; 

	public function scopeCargarListado($query,$params=array()){
        $query->orderBy('fecha_emision','desc');
        $query->orderBy('serie','desc');
        $query->orderBy('numero','desc');

        if(isset($params['id_empresa']) && $params['id_empresa']!=""){
            $query->where('id_empresa',$params['id_empresa']);
        }


        if (isset($params['buscar']) && $params['buscar']!="" ) {
            $query->where(function($subquery) use ($params){
                $empresa=Empresa::where('ruc',$params['buscar'])->first();
                if($empresa instanceof Empresa){
                    $subquery->where('id_empresa',$empresa->id);
                }
                // $subquery->where('empresas.ruc','like','%'.$params['buscar'].'%');
                // $subquery->orWhere('empresas.razon_social','like','%'.$params['buscar'].'%');
                $subquery->orWhere('serie','like','%'.$params['buscar'].'%');
                $subquery->orWhere('numero','like','%'.$params['buscar'].'%');

            });
            return $query;
        }
        return $query;
	}

	public function scopeBuscar($query,$ruc,$serie,$numero){
        if($ruc!=""){
            $empresa=Empresa::where('ruc',$ruc)->first();
            if($empresa instanceof Empresa){
                $query->where('id_empresa',$empresa->id);
            }else{
                $query->where('id',-1);
            }
        }

        if($serie!=""){
            $query->where('serie',$serie);
        }
        if($numero!="") {
            $query->where('numero', $numero);
        }

//                dump($query->toSql(),$query->getBindings());

        return $query;
    }

    public function scopeBuscarPorNumeracion($query,$serie,$numero){
        $query->orderBy('fecha_emision','desc');
    	$query->where('serie',$serie)->where('numero',$numero);
    	$query->orderBy('serie','desc')->orderBy('numero','desc');

    	return $query;
    }
//
//    public function scopeBuscarPorEmpresaORuc($query,$ruc,$serie,$numero){
//        $query->select('empresa_facturas.id','empresa_facturas.serie','empresa_facturas.numero','empresa_facturas.id_empresa',
//            'empresa_facturas.importe','empresa_facturas.fecha_emision','empresa_facturas.ruta_zip',
//            'empresa_facturas.ruta_pdf','empresa_facturas.ruta_excel',
//            'empresas.razon_social as empresa_nombre');
//        $query->join('empresas','empresa_facturas.id_empresa','=','empresas.id');
//        $query->where(function($subquery) use ($ruc,$serie,$numero){
//            if($serie != null && $numero != null){
//                $subquery->where('serie',$serie);
//                $subquery->where('numero',$numero);
//            }
//
//            if($ruc != null){
//                $subquery->orWhere('empresas.ruc',$ruc);
//            }
//        });
//        $query->orderBy('fecha_emision','desc');
//        $query->orderBy('serie','desc')->orderBy('numero','desc');
//        return $query;
//    }

    public function empresa(){
    	return $this->belongsTo(Empresa::class,'id_empresa','id');
    }

    public function notasCredito(){
        return $this->hasMany(NotaCredito::class,'id_factura','id');
    }

    public function notasDebito(){
         return $this->hasMany(NotaDebito::class,'id_factura','id');
    }



    public function estado(){
        $notas_credito_asociadas=$this->notasCredito;

        if($notas_credito_asociadas->count()==0){
            return self::ESTADO_EMITIDO;
        }elseif($notas_credito_asociadas->count()==1){
            $nota_credito=$notas_credito_asociadas->first();
            
            $conceptos_anulacion=NotaCredito::getConceptosAnulacion();
            if (in_array($nota_credito->id_concepto,$conceptos_anulacion)) {
                return self::ESTADO_ANULADO;
            }else{
                return self::ESTADO_MODIFICADO;
            }
        }else{
            return self::ESTADO_MODIFICADO;
        }
    }

    public function estadoToString(){
        $estado_actual=$this->estado();

        $estados_disponibles=self::getEstados();
        $indice=$estados_disponibles->search(function ($estado) use ($estado_actual){
            return $estado->id==$estado_actual;
        });
        
        if($indice!==false){
            return $estados_disponibles->get($indice)->nombre;
        }

        return "no especificado";
    }

     public static function getEstados(){
        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>self::ESTADO_EMITIDO,'nombre'=>"Emitido"));
        $estados_disponibles->push((object)array('id'=>self::ESTADO_ANULADO,'nombre'=>"Anulado"));
        $estados_disponibles->push((object)array('id'=>self::ESTADO_MODIFICADO,'nombre'=>"Modificado"));
        return $estados_disponibles;
    }

    public function descargarZip(){
        return response()->download(storage_path("app/".$this->ruta_zip));
    }

    public function descargarExcel(){
        $data_comprobante=self::extraerInfoFacturaDesdeXml($this->ruta_zip);
        try{
            $this->generarExcel($data_comprobante);
            return response()->download(storage_path("app/".$this->ruta_excel))
                ->deleteFileAfterSend(true);
        }catch(\Exception $e){
            return response()->make("No se pudo generar el excel. ".$e->getMessage());
        }
    }

    public function descargarPdf(){
        $data_comprobante=self::extraerInfoFacturaDesdeXml($this->ruta_zip);
        try{
            $this->generarPDF($data_comprobante);
            return response()->download(storage_path("app/".$this->ruta_pdf))
                ->deleteFileAfterSend(true);
        }catch(\Exception $e){
            return response()->make("No se pudo generar el pdf. ".$e->getMessage());
        }
    }

    public function generarArchivos($archivo=null){
        $numero_modificaciones=0;
        $disco=Storage::disk('local');
        //SI HAY ARCHIVO

        if(isset($archivo)){
            $data_comprobante=Factura::extraerInfoFacturaDesdeXml($archivo);
            $this->generarZip($archivo);
            $numero_modificaciones++;
        }elseif($this->ruta_zip!="" && strpos($this->ruta_zip,"pendientes")==false && $disco->exists($this->ruta_zip)){
            $data_comprobante=Factura::extraerInfoFacturaDesdeXml($this->ruta_zip);
        }else{
            throw new \Exception("Zip no encontrado".$archivo);
        }

        if($this->ruta_pdf=="" || !$disco->exists($this->ruta_pdf)){
            $this->generarPDF($data_comprobante);
            $numero_modificaciones++;
        }

        if($this->ruta_excel!="" || !$disco->exists($this->ruta_excel)){
            $this->generarExcel($data_comprobante);
            $numero_modificaciones++;
        }

        if($numero_modificaciones>0){
            $this->save();
        }
    }

    public static function procesarArchivo($archivo){
        $data_comprobante=Factura::extraerInfoFacturaDesdeXML($archivo);

        $factura=Factura::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();

//        $factura=NotaCredito::buscarPorNumeracion($data_comprobante['serie'],$data_comprobante['numero'])->first();
        if($factura instanceof Factura){
//            try{
//                $factura->generarArchivos($archivo);
//            }catch(\Exception $e){
//                throw new \Exception($e->getMessage(),self::EXCEPCION_ACTUALIZAR);
//            }
        }else{
            try{
                Factura::registrarDesdeDatosXml($data_comprobante,$archivo);
            }catch(\Exception $e){
                throw new \Exception($e->getMessage(),self::EXCEPCION_CREAR);
            }

        }
    }

    public static function extraerInfoFacturaDesdeXml($archivo){
        $xml=Util::extraerXmlDeZip($archivo,
            'xmlns:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\xsd\maindoc\UBLPE-Invoice-1.0.xsd"');

        $ns = $xml->getNamespaces(true);
        $serie_numero = explode("-",$xml->children($ns['cbc'])->ID->__toString());
        $fecha_emision = $xml->children($ns['cbc'])->IssueDate->__toString();
//            ." ".$xml->children($ns['cbc'])->IssueTime->__toString();
        $fecha_vencimiento = $fecha_emision;//$xml->children($ns['cbc'])->DueDate->__toString();

        $importe=$xml->children($ns['cac'])->LegalMonetaryTotal->children($ns['cbc'])->PayableAmount->__toString();
        $serie=$serie_numero[0];
        $numero=$serie_numero[1];

        $label = $xml->children($ns['ext'])->UBLExtensions->children($ns['ext'])[0]
                ->ExtensionContent->children($ns['sac'])
                ->AdditionalInformation->children($ns['sac']);

        //
        $gravadas = 0.00; //$label->AdditionalMonetaryTotal[0]->children($ns['cbc'])->PayableAmount->__toString();
        $inafectas = 0.00; //$label->AdditionalMonetaryTotal[1]->children($ns['cbc'])->PayableAmount->__toString();
        $exoneradas = 0.00; //$label->AdditionalMonetaryTotal[2]->children($ns['cbc'])->PayableAmount->__toString();
        $gratuitas = 0.00;
        $subtotal = 0.00;
        $percepciones = 0.00;
        $retenciones = 0.00;
        $detracciones = 0.00;
        $bonificaciones = 0.00;
        $descuentos = 0.00;
        $fise = 0.00;

        foreach($label->AdditionalMonetaryTotal as $id=>$bloque){
            if($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_GRAVADAS) {
                $gravadas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_INAFECTAS){
                $inafectas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_EXONERADAS){
                $exoneradas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_OPERACIONES_GRATUITAS){
                $gratuitas = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_SUBTOTAL_VENTA){
                $subtotal = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_PERCEPCIONES){
                $percepciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_RETENCIONES){
                $retenciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_DETRACCIONES){
                $detracciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_BONIFICACIONES){
                $bonificaciones = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_TOTAL_DESCUENTOS){
                $descuentos = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }elseif($bloque->children($ns['cbc'])->ID->__toString()==self::CONCEPTO_FISE){
                $fise = $bloque->children($ns['cbc'])->PayableAmount->__toString();
            }
        }

        $totales = compact('gravadas','inafectas','exoneradas','gratuitas','subtotal',
                            'percepciones','retenciones','detracciones','bonificaciones','descuentos',
                            'fise');

        $data_empresa=Empresa::extraerInfoEmpresa($xml,$ns);

        $data_emisor=Empresa::extraerInfoEmisor($xml,$ns);

        $data_producto = self::extraerInfoProductos($xml,$ns);

        $codigo_hash = self::extraerCodigoHash($xml,$ns);

        $ruta_zip=$archivo;

        return compact('serie','numero','fecha_emision','fecha_vencimiento','importe','data_empresa','data_emisor','data_producto','codigo_hash','totales','ruta_zip');
    }

    public static function extraerCodigoHash($xml,$ns)
    {
        $codigo = $xml->children($ns['ext'])
            ->UBLExtensions->children($ns['ext'])[1]
            ->ExtensionContent->children($ns['ds'])
            ->Signature->children($ns['ds'])
            ->SignedInfo->children($ns['ds'])
            ->Reference->children($ns['ds'])
            ->DigestValue->__toString();
        return $codigo;
    }


    public static function extraerInfoProductos($xml,$ns){

        $contador = count($xml->children($ns['cac'])->InvoiceLine);
        //dd($contador);
        $listado_productos = array();
        for ($i = 0; $i < $contador; $i++){
            $label = $xml->children($ns['cac'])->InvoiceLine[$i];
            //dd($label);
            $DescripcionProducto=$label->children($ns['cac'])->Item->children($ns['cbc'])->Description->__toString();

            $CantidadUnidad=$label->children($ns['cbc'])->InvoicedQuantity->__toString();

            $TipoCantidad = $label->children($ns['cbc'])->InvoicedQuantity->attributes()->unitCode->__toString();

            $PrecioVentaUnitario = $label->children($ns['cac'])->Price->children($ns['cbc'])->PriceAmount->__toString();

            $PrecioVentaTotal=0.0;

            if(isset($label->children($ns['cac'])->PricingReference)){
           $PrecioVentaTotal=$label->children($ns['cac'])->PricingReference
                ->children($ns['cac'])->AlternativeConditionPrice->children($ns['cbc'])
                ->PriceAmount->__toString();
            }

            $listado_productos[$i] = [ 'descripcion_producto' => trim($DescripcionProducto),
                'cantidadUnidad' => trim($CantidadUnidad),
                'tipoCantidad' => trim($TipoCantidad),
                'PrecioVentaUnitario' => trim($PrecioVentaUnitario),
                'PrecioVentaTotal' => trim($PrecioVentaTotal)
            ];

        }
       // $data=compact('listado_productos');
       // dd($data);
        return $listado_productos;
    }

    public static function registrarDesdeDatosXml($data_comprobante,$archivo){
        $ruc_permitido=env('RUC_EMPRESA','20517869636');
        if ($data_comprobante['data_emisor']['ruc']!=$ruc_permitido) {
            throw new \Exception('El emisor no corresponse, asociado a '.$ruc_permitido.', valor recibido:'.$data_comprobante['data_emisor']['ruc']);
        }

        $empresa=Empresa::buscarPorRuc($data_comprobante['data_empresa']['ruc']);
        if($empresa instanceof Empresa){

        }else{
            $empresa=Empresa::registrarDesdeDatosXml($data_comprobante['data_empresa']);
        }

        $factura=new Factura();
        $factura->id_empresa=$empresa->id;
        $factura->serie=$data_comprobante['serie'];
        $factura->numero=$data_comprobante['numero'];
        $factura->fecha_emision=$data_comprobante['fecha_emision'];
        $factura->importe=$data_comprobante['importe']+$data_comprobante['totales']['percepciones'];
        $factura->estado='AC';
        //Generar pdf
//        $factura->generarPDF($data_comprobante);

        //Generar Excel
//        $factura->generarExcel($data_comprobante);

        //Generar Zip
        $factura->generarZIP($archivo);
        $factura->save();
        return $factura;
    }

    public function generarPDF($data_boleta){
//        dd($data_boleta);
        $sumatoria = 0.00;
        $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];
        $data = [
            'codigo_factura' => $data_boleta['serie'] . "-" . $data_boleta['numero'] ,
            'establecimiento' => $data_boleta['data_emisor']['direccion'],
            'nombre_empresa' => $data_boleta['data_emisor']['razon_social'],
            'ruc_empresa' => $data_boleta['data_emisor']['ruc'],
            'portal_web' => 'www.pordefinir.com',
            'numero_resolucion' => '123456789',
            'fecha_emision' => $data_boleta['fecha_emision'],
            'fecha_vencimiento' => $data_boleta['fecha_vencimiento'],
            'razon_social' => $data_boleta['data_empresa']['razon_social'],
            'ruc' => $data_boleta['data_empresa']['ruc'],
            'direccion_persona' => $data_boleta['data_empresa']['direccion'],
            'tipo_moneda' => 'PEN',
            'sumatoria_descuentos' => $sumatoria,
            'importe_total' =>  $data_boleta['importe'],
            'total_venta_gravada' => $data_boleta['totales']['gravadas'],
            'total_venta_exonerada' => $data_boleta['totales']['exoneradas'],
            'total_venta_inafecta'  => $data_boleta['totales']['inafectas'],
            'percepciones'  => $data_boleta['totales']['percepciones'],
            'total_final'  => $data_boleta['importe']+$data_boleta['totales']['percepciones'],
            'sumatoria_IGV' => $igv,
            'nombre_documento' => 'FACTURA',
            'numero' => $data_boleta['numero'],
            'serie' => $data_boleta['serie'],
            'data_producto'=>$data_boleta['data_producto'],
            'codigo_hash' => $data_boleta['codigo_hash'],
        ];
//        $temp = $data_boleta['data_producto']['listado_productos'];

        //dd($temp[0]['descripcion_producto']);

        $directorio=$this->generarRuta("pdf",$this->fecha_emision);

        if($data != null )
        {
            //Convertir de numeros a letras
            $numero_letras = NumeroALetras::convertir(number_format(floatval($data['importe_total']),2),'','CENTIMOS');
            //Agregar todo los datos al invoice
            $view = \View::make('admin.pdf.invoice', compact('data','numero_letras'))->render();
            $dompdf = \App::make('dompdf.wrapper');
            $dompdf->loadHTML($view);

            $this->ruta_pdf = $directorio . str_replace(".zip",".pdf",basename($data_boleta['ruta_zip']));

            Storage::disk('local')->put($this->ruta_pdf, $dompdf->output());

        }else{
            throw new \Exception("No se pudo generar el pdf");
        }
    }

    public  function  generarExcel($data_boleta){
        $productos = $data_boleta['data_producto'];

        $directorio=$this->generarRuta("excel",$this->fecha_emision);
        $nombre_excel=str_replace(".zip","",basename($data_boleta['ruta_zip']));

        Excel::create($nombre_excel ,function($excel) use ($productos,$data_boleta) {
            $excel->sheet('Factura' ,function($sheet) use ($productos, $data_boleta) {
                $razon_social = $data_boleta['data_empresa']['razon_social'];
                $ruc = $data_boleta['data_empresa']['ruc'];
                $importe_total =  $data_boleta['importe'];
                $total_venta_gravada = $data_boleta['totales']['gravadas'];
                $total_venta_exonerada = $data_boleta['totales']['exoneradas'];
                $total_venta_inafecta  = $data_boleta['totales']['inafectas'];
                $total_igv = $igv = $data_boleta['importe'] - $data_boleta['totales']['gravadas'];

                $percepciones  = $data_boleta['totales']['percepciones'];
                $total_final  = $data_boleta['importe']+$data_boleta['totales']['percepciones'];

                //ESTILOS
                $sheet->cell('A1:J20', function($cells) {
                    $cells->setAlignment('center');
                });
                $contador = count($productos);
                $bloque_percepcion=(isset($percepciones) && $percepciones>0 ? 2:0);

                $sheet->setBorder('A6:E'. ($contador + 6), 'thin');
                $sheet->setBorder('D'.(8 + $contador) .':E'. ($contador + 12 + $bloque_percepcion), 'thin');
                //PARTE IZQUIERDA CABEZERA
                $sheet->cell('A1', function($cell) use ($data_boleta) {
                    $nombre_empresa = $data_boleta['data_emisor']['razon_social'];
                    $cell->setValue($nombre_empresa);
                });
                $sheet->cell('A2', function($cell){
                    $cell->setValue('DNI o RUC: ');
                });
                $sheet->cell('B2', function($cell) use ($ruc) {
                    $cell->setValue($ruc);
                });
                $sheet->cell('A3', function($cell){
                    $cell->setValue('Nombre o Razón Social: ');
                });
                $sheet->cell('B3', function($cell) use ($razon_social) {
                    $cell->setValue($razon_social);
                });
                //PARTE DERECHA CABEZERA
                $sheet->cell('E1', function($cell) {
                    $cell->setValue('FACTURA ELECTRÓNICA');
                });
                $sheet->cell('E2', function($cell) {
                    $cell->setValue(env('RUC_EMPRESA'));
                });
                $sheet->cell('E3', function($cell) use ($data_boleta) {
                    $codigo_factura = $data_boleta['serie'] . "-" . $data_boleta['numero'];
                    $cell->setValue($codigo_factura);
                });
                $sheet->cell('E4', function($cell) use ($data_boleta) {
                    $fecha = $data_boleta['fecha_emision'];
                    $cell->setValue('Fecha: '. $fecha);
                });
                //PARTE TABLA DE PRODUCTOS
                $sheet->fromArray($productos, null, 'A6',true);
                //PARTE TABLA TOTAL

                $sheet->cell('D'.(8 + $contador), function($cell) {
                    $cell->setValue('Total Gravadas:');
                });
                $sheet->cell('D'.(9 + $contador), function($cell) {
                    $cell->setValue('Total Inafectas:');
                });
                $sheet->cell('D'.(10 + $contador), function($cell) {
                    $cell->setValue('Total Exoneradas:');
                });
                $sheet->cell('D'.(11 + $contador), function($cell) {
                    $cell->setValue('Total IGV:');
                });
                $sheet->cell('D'.(12 + $contador), function($cell) {
                    $cell->setValue('Importe Total:');
                });

                if(isset($percepciones) && $percepciones>0){
                    $sheet->cell('D'.(13 + $contador), function($cell) {
                        $cell->setValue('Importe de percepción:');
                    });
                    $sheet->cell('D'.(14 + $contador), function($cell) {
                        $cell->setValue('Total a pagar:');
                    });
                }

                $sheet->cell('E'.(8 + $contador), function($cell) use($total_venta_gravada) {
                    $cell->setValue($total_venta_gravada);
                });
                $sheet->cell('E'.(9 + $contador), function($cell) use ($total_venta_inafecta) {
                    $cell->setValue($total_venta_inafecta);
                });
                $sheet->cell('E'.(10 + $contador), function($cell) use ( $total_venta_exonerada) {
                    $cell->setValue( $total_venta_exonerada);
                });
                $sheet->cell('E'.(11 + $contador), function($cell) use ($total_igv){
                    $cell->setValue($total_igv);
                });
                $sheet->cell('E'.(12 + $contador), function($cell) use ($importe_total){
                    $cell->setValue($importe_total);
                });

                if(isset($percepciones) && $percepciones>0){
                    $sheet->cell('E'.(13 + $contador), function($cell) use ($percepciones){
                        $cell->setValue($percepciones);
                    });
                    $sheet->cell('E'.(14 + $contador), function($cell) use ($total_final){
                        $cell->setValue($total_final);
                    });
                }
            });
        })->store('xls',storage_path("app/".$directorio));

        $this->ruta_excel = $directorio.$nombre_excel.".xls";
    }

    public function generarZip($archivo){
        $directorio=$this->generarRuta("zip",$this->fecha_emision);
        $nueva_ruta=$directorio.basename($archivo);
        if(!Storage::disk("local")->has($nueva_ruta)){
            Storage::disk("local")->move($archivo, $nueva_ruta);
        }
        $this->ruta_zip=$nueva_ruta;
    }


    private function generarRuta($tipo_archivo, $fecha_emision)
    {
        $fecha = $fecha_emision;
        $anio = $fecha->year;
        $mes = Util::mesToString($fecha->month);
        $dia = $fecha->day;
//
        $directorio = "facturas/".$tipo_archivo."/".$anio."/".$mes."/".$dia."/";
        if(!Storage::disk('local')->exists($directorio)){
            Storage::disk('local')->makeDirectory($directorio);
        }

        return $directorio;
    }


}
