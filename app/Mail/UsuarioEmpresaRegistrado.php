<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Modulos\Facturacion\Usuario;

class UsuarioEmpresaRegistrado extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $usuario;
    protected $password;
    
    public function __construct(Usuario $usuario,$password)
    {
        //
        $this->usuario=$usuario;
        $this->password=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.mail.usuario-empresa-registrado')->with(['usuario'=>$this->usuario,'password'=>$this->password]);
    }
}
