function ClassFormulario(form,boton){

    // CONSTANTES DE VALIDACIÓN
    VALIDAR_VACIO=1;
    VALIDAR_CORREO=2;
    VALIDAR_ALFABETICO=3;
    VALIDAR_NUMERO=4;
    VALIDAR_ALFANUMERICO=5;
    VALIDAR_CAPTCHA=6;

    // CONSTANTES DE ESTADO
    NOTIFICAR_EXITO=1;
    NOTIFICAR_ERROR=2;
    NOTIFICAR_PROCESANDO=3;

    this.camposValidados=new Array();
    this.resaltador="";
    this.formulario=form;
    this.notificaciones=boton.siblings('.notificaciones');
    this.validacionCaptcha="";

    this.esCorreo=function(etiqueta){
        //return /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/.test(etiqueta.val());
        return /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(etiqueta.val());;
    }

    this.esVacio=function(etiqueta){
        return etiqueta.val()=="";
    }

    this.esAlfabetico=function(etiqueta){
        return /^([a-z ñáéíóú]{2,60})$/.test(etiqueta.val());
    }

    this.esNumero=function(etiqueta){
        return /^([0-9])*$/.test(etiqueta.val());
    }

    this.setearValidacion=function(nombre_campo,tipo_validacion){
        this.camposValidados.push({campo:nombre_campo, validacion:tipo_validacion});
    }

    this.setearValidacionCaptcha=function(etiqueta_captcha){
        this.validacionCaptcha=etiqueta_captcha.val();
    }

    this.notificar=function(tipo_notificacion){
        this.notificaciones.show();
        etiquetas_notificacion=this.notificaciones.children();
        etiquetas_notificacion.hide();

        if(tipo_notificacion==NOTIFICAR_EXITO){
            etiquetas_notificacion.filter(".done").show().delay(9000).fadeOut();
        }else if(tipo_notificacion==NOTIFICAR_ERROR){
            etiquetas_notificacion.filter(".fail").show().delay(9000).fadeOut();
        }else if(tipo_notificacion==NOTIFICAR_PROCESANDO){
            etiquetas_notificacion.filter(".reload").show();//.delay(7000).fadeOut();
        }
    }

    this.validar = function() {
        error_formulario = 0;

        for (var i = 0; i < this.camposValidados.length; i++ ){
            etiqueta = this.formulario.find('#'+this.camposValidados[i]['campo']);
            tipo = this.camposValidados[i]['validacion'];
            error_campo = false;

            if(tipo == VALIDAR_VACIO && this.esVacio(etiqueta)){
                error_campo = true;
            }else if(tipo == VALIDAR_NUMERO && !this.esNumero(etiqueta)) {
                error_campo = true;
            }else if(tipo == VALIDAR_CORREO && !this.esCorreo(etiqueta)) {
                error_campo = true;
            }else if(tipo == VALIDAR_CAPTCHA && !this.esVacio(etiqueta) && this.validacionCaptcha != "" && this.validacionCaptcha != etiqueta.val()) {
                error_campo = true;
            }else if(tipo == VALIDAR_CAPTCHA && this.esVacio(etiqueta)) {
                error_campo = true;
            }

            if(error_campo){
                error_formulario++;
                etiqueta.addClass(this.resaltador);
            }else {
                etiqueta.removeClass(this.resaltador);
            }
        }

        return error_formulario==0;
    }

    this.enviar = function() {

        if( this.validar() == false ){
            return;
        }

        this.notificar(NOTIFICAR_PROCESANDO);
        contenedorForm = this;

        datos = this.formulario.serialize();

        $.ajax({
            type: "post",
            url: AjaxForms.url,
            data: datos,
            success: function( respuesta ) {

                if( respuesta == "OK" ){
                    contenedorForm.notificar( NOTIFICAR_EXITO );
                }else {
                    contenedorForm.notificar( NOTIFICAR_ERROR );
                }
            }
        })
        .done(function(){
            recargar_captcha();
            contenedorForm.formulario[0].reset();
            // this.formulario[0].reset();
        })
    }
}

// Función que permite recargar un captcha
function  recargar_captcha() {
    var htmlCargando='<div class="animacion text-center">';
        htmlCargando+='<i class="icon-redo"></i>';
        htmlCargando+='</div>';
    var contenido_ajax = $('div#cont-img-captcha');
        contenido_ajax.html(htmlCargando);
    $.ajax({
        type: 'post',
        url: AjaxForms.url,
        data: {
            action: 'recargar_captcha'
        },
        success: function( resultado ) {
            contenido_ajax.html(resultado);
        }
    })
}