$('input#btn-cotizacion').click(function(e){
    e.preventDefault();
    boton = $(this);
    form = boton.parents('form.formulario');
    id_form = form.attr('id');
    
    classForm = new ClassFormulario(form, boton);

    if( id_form == "frmCotizacion" ) {
        classForm.resaltador = 'error';
        // classForm.setearValidacion("txtNombres", 1);   // VALIDAR VACÍO
        // classForm.setearValidacion("txtEmpresa", 1);   // VALIDAR VACÍO
        // classForm.setearValidacion("txtTelefono", 1);   // VALIDAR NUMÉRICO
        classForm.setearValidacion("txtCorreo", 2);     // VALIDAR CORREO
        // classForm.setearValidacion("txtMensaje", 1);    // VALIDAR VACÍO

        if( form.find('input#value_captcha').length ) {
            etiquetaCaptcha = form.find('input#value_captcha');
            classForm.setearValidacion("captcha", VALIDAR_CAPTCHA);
            classForm.setearValidacionCaptcha(etiquetaCaptcha);
        }
    }
    classForm.enviar();
});

$('input#btn-contacto').click(function(e){
    e.preventDefault();
    boton = $(this);
    form = boton.parents('form.formulario');
    id_form = form.attr('id');
    
    classForm = new ClassFormulario(form, boton);

    if( id_form == "frmContacto" ) {
        classForm.resaltador = 'error';
        // classForm.setearValidacion("txtNombres", 1);   // VALIDAR VACÍO
        // classForm.setearValidacion("txtTelefono", 1);   // VALIDAR NUMÉRICO
        classForm.setearValidacion("txtCorreo", 2);     // VALIDAR CORREO
        classForm.setearValidacion("txtMensaje", 1);    // VALIDAR VACÍO

        if( form.find('input#value_captcha').length ) {
            etiquetaCaptcha = form.find('input#value_captcha');
            classForm.setearValidacion("captcha", VALIDAR_CAPTCHA);
            classForm.setearValidacionCaptcha(etiquetaCaptcha);
        }
    }
    classForm.enviar();
})

$('a#btn-recargar-captcha').click(function(e) {
    e.preventDefault();
    recargar_captcha();
})