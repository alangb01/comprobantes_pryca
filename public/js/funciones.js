$('.datepicker').datetimepicker({
	locale: 'es',
	format: 'YYYY-MM-DD'
});

$(document).on('click',"form [data-confirmar]",function(e){
	e.preventDefault();

	$("#myModal").find('#modal_titulo').html('Confirmación');
	$("#myModal").find('#modal_contenido').html($(this).attr("data-confirmar"));

	var form=$(this).parents("form")
	$("#myModal").modal('show').one('click', '#btn_accion', function() {
       form.submit();
    });;
})


$(document).on('click',"a.modal-asociado",function(e){
	e.preventDefault();

	url=$(this).attr('href');
	$.get(url,function(response){
		$("#myModal").find('#modal_titulo').html('Asociar documento');
		$("#myModal").find('#modal_contenido').html(response);

		var form=$("#myModal").find("form#asociar")
		$("#myModal").modal('show').one('click', '#btn_accion', function() {
	       	procesarForm(form);
	    });;
	});
})

function procesarForm(form){

	url=form.attr('action');
	
	data=form.serialize();
	$.post(url,data,function(response){
		if (response=="OK") {
			location.reload();
		}else{
			alert('No se pudo registrar');
		};
	});
}